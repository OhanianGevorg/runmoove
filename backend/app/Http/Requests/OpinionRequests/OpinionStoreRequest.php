<?php

namespace App\Http\Requests\OpinionRequests;

use App\Http\Requests\AbstractRequest;
use App\User;

class OpinionStoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $custom_rules = [];
        if ($this->has('opinion_images')) {
            foreach ($this->get('opinion_images') as $index => $value) {
                $custom_rules['opinion_images.' . ($index) . '.url'] = 'required|file_exists:storage';
            }
        }
        return array_merge([
            'course_date_id' => 'integer|required|exists:course_dates,id',
            'course_opinion_formats' => 'required|max:255',
            'title' => 'required|string|max:255',
            'body' => 'required|string|max:65000',
            'parcour_score' => 'required|numeric|min:1|max:5',
            'ambiance_score' => 'required|numeric|min:1|max:5',
            'dotation_score' => 'required|numeric|min:1|max:5',
            'revitaillement_score' => 'required|numeric|min:1|max:5',
            'organisation_score' => 'required|numeric|min:1|max:5',
            'difficult_score' => 'required|numeric|min:1|max:5',
            'participation_purpose' => 'required|string|max:255',
            'participate_again' => 'required|string|max:255',
            'course_finish_time' => 'string|max:255',
            'best_memories' => 'string|max:65000',
            'place_recommendations' => 'string|max:65000',
            'suggestions' => 'max:65000|string'
        ], $custom_rules);
    }
}
