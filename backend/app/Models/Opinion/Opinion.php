<?php

namespace App\Models\Opinion;

use App\Models\Course\CourseDate;
use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Opinion extends Model
{
    //
    use SoftDeletes, FilterTrait;

    protected $table = 'course_opinion';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'course_date_id',
        'parcour_score',
        'ambiance_score',
        'dotation_score',
        'revitaillement_score',
        'organisation_score',
        'difficult_score',
        'title',
        'participation_purpose',
        'participate_again',
        'course_finish_time',
        'body',
        'best_memories',
        'place_recommendations',
        'suggestions',
    ];

    public static $countableFields = [
        'parcour_score',
        'ambiance_score',
        'dotation_score',
        'revitaillement_score',
        'organisation_score',
        'difficult_score',
    ];

    public function images()
    {
        return $this->hasMany(OpinionImage::class, 'course_opinion_id');
    }

    public function course_opinion_formats()
    {
        return $this->hasMany(CourseOpinionFormat::class, 'course_opinion_id');
    }

    public function get_opinion_formats()
    {
        return $this->course_opinion_formats()->getResults()->map(function (CourseOpinionFormat $relation) {
            return $relation->format;
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function course_date()
    {
        return $this->belongsTo(CourseDate::class, 'course_date_id');
    }

    public function countScoreTotal()
    {
        $total = 0;

        foreach (self::$countableFields as $field) {
            $total += $this->attributes[$field];
        }
        return round($total/count(self::$countableFields));
    }
}
