<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/18/2017
 * Time: 10:58 PM
 */

namespace App\Traits;


use App\Mail\NotificationMessage;
use App\Models\UserRelated\Notifications;
use App\User;
use Illuminate\Support\Facades\Mail;

trait SendEmailTrait
{
    /**
     * @param Notifications $model
     * @param User $user
     */
    public function sendEmail(Notifications $model, User $user)
    {
        if (!$user->email) {
            return;
        }
        try {
            Mail::to($user->email)->send(new NotificationMessage($model));
            return;
        } catch (\Exception $e) {
            return;
        }
    }
}