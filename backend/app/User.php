<?php

namespace App;

use App\Models\Article\Article;
use App\Models\Chat\Message;
use App\Models\Chat\Participant;
use App\Models\Chat\Room;
use App\Models\Course\Course;
use App\Models\Course\CourseDate;
use App\Models\Course\CourseParticipation;
use App\Models\Friendship\BlockedUser;
use App\Models\Friendship\FriendList;
use App\Models\Friendship\FriendRequest;
use App\Models\Opinion\Opinion;
use App\Models\QuestionAndAnswer\Question;
use App\Models\System\SystemCourseFormat;
use App\Models\System\SystemCourseImportance;
use App\Models\System\SystemPracticingType;
use App\Models\SystemConstants\SystemConstants;
use App\Models\SystemLevels;
use App\Models\SystemRelated\UserCourseFormat;
use App\Models\SystemRelated\UserCourseImportance;
use App\Models\SystemRelated\UserPracticingType;
use App\Models\UserLevel;
use App\Models\UserRelated\Notifications;
use App\Models\UserRelated\UserRecord;
use App\Traits\FilterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, FilterTrait;

    protected $fillable = [
        'opt_in',
        'first_name',
        'last_name',
        'email',
        'password',
        'dob',
        'address',
        'role_id',
        'avatar',
        'fb_id',
        'twitter_id',
        'is_completed',
        'completed_percent',
        'user_type',
        'practice_since',
        'running_frequency',
        'transportation_mode',
        'budget',
        'practice_location',
        'living_preferences',
        'voyage_with',
        'presentation',
        'latitude',
        'longitude',
        'api_token',
        'score',
    ];

    private $countableFields = [
        'first_name',
        'last_name',
        'dob',
        'address',
        'avatar',
        'user_type',
        'practice_since',
        'running_frequency',
        'transportation_mode',
        'budget',
        'practice_location',
        'living_preferences',
        'voyage_with',
        'presentation',
        'course_formats',
        'course_importances',
        'user_practicings'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $list = [
        'friendRequest',
        'addFriend'
    ];

    public function getEventsList()
    {
        return $this->list;
    }

    public static function auth()
    {
        if (Session::has('api_token')) {

            $user = self::where('api_token', Session::get('api_token'))->first();

            if ($user) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @desc check if User is admin
     * @return bool
     */
    public function is_admin()
    {
        return (int)$this->role_id === SystemConstants::ADMIN_ROLE;
    }

    /**
     * @desc check if user is orginizer
     * @return bool
     */
    public function is_orginizer()
    {
        return (int)$this->user_type === SystemConstants::ORGANIZER;
    }

    /**
     * @return UserLevel
     */
    public function user_level()
    {
        $user_level = UserLevel::where('user_id', '=', $this->getKey())->first();
        return $user_level ? $user_level : new UserLevel();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_course_formats()
    {
        return $this->hasMany(UserCourseFormat::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_course_importances()
    {
        return $this->hasMany(UserCourseImportance::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_practicing_types()
    {
        return $this->hasMany(UserPracticingType::class, 'user_id');
    }

    /**
     * @return mixed
     */
    public function course_formats()
    {
        return SystemCourseFormat::select('system_course_formats.*')
            ->join('user_course_format', 'system_course_formats.id', '=', 'user_course_format.format_id')
            ->join('users', 'user_course_format.user_id', '=', 'users.id')
            ->where('users.id', '=', $this->getKey())->distinct()->get();
    }

    /**
     * @return mixed
     */
    public function course_importances()
    {
        return SystemCourseImportance::select('system_course_importance.*')
            ->join('user_course_importance', 'system_course_importance.id', '=', 'user_course_importance.importance_id')
            ->join('users', 'user_course_importance.user_id', '=', 'users.id')
            ->where('users.id', '=', $this->getKey())->distinct()->get();
    }

    /**
     * @return mixed
     */
    public function user_practicings()
    {

        return SystemPracticingType::select('system_practicing_table.*')
            ->join('user_practicing_type', 'system_practicing_table.id', '=', 'user_practicing_type.type_id')
            ->join('users', 'user_practicing_type.user_id', '=', 'users.id')
            ->where('users.id', '=', $this->getKey())->distinct()->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ownCourses()
    {
        return $this->hasMany(CourseDate::class, 'course_owner_id');
    }

    /**
     * @return mixed
     */
    public function get_user_level()
    {
        return $this->user_level()->level;
    }

    /**
     * @return array
     */
    public function getCountableFields()
    {
        return $this->countableFields;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_opinions()
    {
        return $this->hasMany(Opinion::class, 'user_id');
    }


    public function setLevel()
    {
        return $this->user_level()->fill([
            'level_id' => SystemLevels::getLevel($this->score),
            'user_id' => $this->getKey()
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_participation()
    {
        return $this->hasMany(CourseParticipation::class, 'user_id');
    }

    public function course_participation_to_many()
    {
        return $this->belongsToMany(CourseDate::class, 'course_participation', 'user_id', 'course_date_id');
    }

    /**
     * @return mixed
     */
    public function get_participation_courses()
    {
        return $this->course_participation()->getResults()->map(function (CourseParticipation $relation) {
            return $relation->course_date;
        });
    }

    /**
     * @return mixed
     */
    public function get_user_course_query()
    {
        return CourseDate::join('course_participation', 'course_participation.course_date_id', '=', 'course_dates.id')
            ->where('course_participation.user_id', '=', $this->getKey());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany(UserRecord::class, 'user_id');
    }

    /**
     * @desc return Friendlist
     * @return mixed
     */
    public function friends()
    {
        return FriendList::where('sender_id', '=', $this->getKey())->orWhere('recipient_id', '=', $this->getKey());
    }

    public function ownFriendRequest()
    {
        return $this->hasMany(FriendRequest::class, 'sender_id');
    }

    public function receivedFriendRequest()
    {
        return $this->hasMany(FriendRequest::class, 'recipient_id');
    }

    public function friendShipRequests()
    {
        return FriendRequest::where('sender_id', '=', $this->getKey())->orWhere('recipient_id', '=', $this->getKey());
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'user_id');
    }

    public function participation()
    {
        return $this->hasMany(Participant::class, 'user_id');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'participants', 'user_id', 'room_id');
    }

    /**
     * @param $recipient_id
     * @return mixed
     */
    public function getFriendRequestByFriend($recipient_id)
    {
        return FriendRequest::where('sender_id', '=', $this->getKey())
            ->where('recipient_id', '=', $recipient_id)
            ->orWhere('recipient_id', '=', $this->getKey())
            ->where('sender_id', '=', $recipient_id);
    }

    /**
     * @param $recipient_id
     * @return mixed
     */
    public function getFriendListByFriend($recipient_id)
    {
        return FriendList::where('sender_id', '=', $this->getKey())
            ->where('recipient_id', '=', $recipient_id)
            ->orWhere('recipient_id', '=', $this->getKey())
            ->where('sender_id', '=', $recipient_id);
    }

    public function hasFriend($user_id)
    {
        return !is_null($this->getFriendListByFriend($user_id)->first());
    }

    /**
     * @param $recipient_id
     * @return bool
     */
    public function canBeFriends($recipient_id)
    {
        if ((int)$recipient_id === (int)$this->getKey()) {
            return false;
        }
        return is_null($this->getFriendRequestByFriend($recipient_id)->first()) && is_null($this->getFriendListByFriend($recipient_id)->first());
    }

    /**
     * @return mixed
     */
    public function getFriendsQueryBuilder()
    {
        $friends = $this->friends()->get(['sender_id', 'recipient_id']);
        $recipients = $friends->pluck('recipient_id')->all();
        $senders = $friends->pluck('sender_id')->all();
        return static::select('users.*')->where('id', '!=', $this->getKey())->whereIn('id', array_merge($recipients, $senders));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notifications::class, 'owner_id');
    }

    /**
     * @desc call sort and filter to model. Use model Filter method
     * @param array $params
     * @return mixed
     */
    public function getSortedNotifications($params = ['isRead' => Notifications::IS_NOT_READ, 'sort' => 'created'])
    {
        return $this->notifications()->getAll($params);
    }

    /**
     * @param $query
     */
    public function scopeAssignMyBlocked($query)
    {
        $query->leftJoin('blocked_users', 'users.id', '=', 'blocked_users.blocked_id')
            ->where('blocker_id', '=', $this->getKey());
    }

    public function blockerUsers()
    {
        return $this->hasMany(BlockedUser::class, 'blocker_id');
    }

    public function blockedUsers()
    {
        return $this->hasMany(BlockedUser::class, 'blocked_id');
    }

    /**
     * @desc if User has blocked
     * @param $user_id
     * @return bool
     */
    public function is_blocked($user_id)
    {
        return !is_null($this->blockedUsers()->where('blocker_id', '=', $user_id)->first());
    }

    /**
     * @desc if User is block
     * @param $user_id
     * @return bool
     */
    public function is_blocker($user_id)
    {
        return !is_null($this->blockerUsers()->where('blocked_id', '=', $user_id)->first());
    }

    public function getBlockedUser($user_id)
    {
        return $this->blockedUsers()->where('blocked_id', '=', $user_id)->first();
    }

    /**
     * @param User $user
     * @return int
     * @desc check if exists room between users and return it's value
     */
    public function checkRoomExists(User $user)
    {
        $first_user_roomIds = $this->rooms()->select('rooms.id')->pluck('id')->toArray();
        $second_user_roomIds = $user->rooms()->select('rooms.id')->pluck('id')->toArray();

        $intersect = array_values(array_intersect($first_user_roomIds, $second_user_roomIds));

        return count($intersect) ? $intersect[0] : 0;
    }

    /**
     * @param User $user
     * @return bool
     * @desc check if user can write
     */
    public function canWrite(User $user)
    {
        return !$user->is_blocked($this->getKey()) && $this->hasFriend($user->getKey());
    }

    /**
     * @return mixed
     * @desc get unread messages count
     */
    public function getUnreadMessagesCount()
    {
        return Message::unreadMessages($this)->count();
    }

    /**
     * @desc article
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'user_id');
    }

    /**
     * @desc check if user can write articles
     * @return bool
     */
    public function canWriteArticle()
    {
        return (int)$this->role_id === SystemConstants::ADMIN_ROLE;
    }

    /**
     * @desc static article write permission check
     * @return bool
     */
    public static function canWriteArticles()
    {
        return static::auth() && static::auth()->is_admin();
    }

    /**
     * @desc the courses where user is official organizer
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function official_courses()
    {
        return $this->hasMany(CourseDate::class, 'official_organizer_id');
    }

    /**
     * @desc check If User is official organizer of course
     * @param CourseDate $courseDate
     * @return bool
     */
    public function isOfficialOrganizer(CourseDate $courseDate)
    {
        return $this->is_orginizer() && (int)$this->getKey() === (int)$courseDate->getOfficialOrganizerId();
    }

    /**
     * @desc check if User can answer the course question
     * @param Question $question
     * @return bool
     */
    public function canAnswer(Question $question)
    {
        if(!$question){
            return false;
        }
        return $this->is_orginizer() && (int)$this->getKey() === $question->course_date->official_organizer_id;
    }

    /**
     * @desc User's official courses questions query
     * @return mixed
     */
    public function getQuestionsQuery()
    {
        return Question::select('questions.*')
            ->join('course_dates', 'course_dates.id', '=', 'questions.course_date_id')
            ->where('course_dates.official_organizer_id', '=', $this->getKey());
    }
}
