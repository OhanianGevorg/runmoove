<?php

namespace App\Events;

use App\Models\UserRelated\Notifications;
use App\Traits\SendEmailTrait;
use App\Transformers\NotificationTransformer\NotificationTransformer;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;

class FriendRequestSendEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels, SendEmailTrait;

    private $model;
    private $event_name;

    /**
     * FriendRequestSendEvent constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notifications.' . $this->model->recipient_id);
    }

    /**
     * @return mixed
     */
    public function broadcastAs()
    {
        $className = get_class($this->model);
        $this->event_name = $className::EVENT_NAME;
        return $this->event_name;
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        /**
         * @var $this ->model Model
         */
        $data = [
            'owner_id' => $this->model->recipient_id,
            'event_name' => $this->event_name,
            'sender_id' => ($this->model->sender_id) ? $this->model->sender_id : null,
            'is_read' => Notifications::IS_NOT_READ,
            'event_id' => $this->model->getKey(),
            'event_type' => get_class($this->model)
        ];
        $event = NotificationTransformer::prepare($data);
        $event->save();

        /**
         * @desc send mail about notification
         */
        try {
            $user = User::findOrFail($event->owner_id);
            $this->sendEmail($event, $user);
        } catch (ModelNotFoundException $e) {
        } catch (\Exception $e) {
        }
        return [
            'event' => NotificationTransformer::transform($event),
            'request_id' => $this->model->getKey(),
            'message' => 'User has sent you friendship request'
        ];
    }
}
