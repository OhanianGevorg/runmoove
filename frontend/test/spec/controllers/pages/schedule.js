'use strict';

describe('Controller: PagesScheduleCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesScheduleCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesScheduleCtrl = $controller('PagesScheduleCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesScheduleCtrl.awesomeThings.length).toBe(3);
  });
});
