<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

//not authenticated
Route::group(['middleware' => ['before', 'jsonify']], function () {

    //auth routes
    Auth::routes();

    //user not auth
    Route::post('user/save', 'UserControllers\UserController@storeMain');
    Route::get('user', 'UserControllers\UserController@index');
    Route::get('user/{id}', 'UserControllers\UserController@show');

    //course not auth
    Route::get('course', 'CourseControllers\CourseController@index');
    Route::get('course/{id}', 'CourseControllers\CourseController@show');

    //login with social buttons
    Route::post('auth/twitter', 'Auth\SocialAuthController@twAuth');
    Route::post('auth/facebook', 'Auth\SocialAuthController@fbAuth');

    //system constants routes
    Route::get('constants/fields', 'Shared\StaticFieldsController@getAllFields');
    Route::get('constants/courseFields', 'Shared\StaticFieldsController@getCourseFields');
    Route::get('constants/opinionFields', 'Shared\StaticFieldsController@getOpinionFields');

    //article not auth routes
    Route::get('article', 'Article\ArticleController@index');
    Route::get('article/{id}', 'Article\ArticleController@show');
    //gpx file
    Route::get('gpx/{fileName}', 'Shared\GPXDownloadController@getFile');

    //questions and answers
    Route::get('question/list', 'QuestionAndAnswer\QuestionController@questionList');

    //get travel api data. Queued job
    Route::get('travel','CourseControllers\CourseController@callAPI');
    Route::get('getTravelData','CourseControllers\CourseController@getApiData');
    Route::get('getAviaBooking','CourseControllers\CourseController@getAviaBookingUrl');
    //authenticated routes
    Route::group(['middleware' => ['ownAuth', 'after']], function () {
        //user routes
        Route::get('eventlist', 'UserControllers\UserController@getEventsList');
        Route::get('user/friends', 'UserControllers\UserController@getFriends');
        Route::post('user/additional', 'UserControllers\UserController@storeAdditional');
        Route::post('user/changeAvatar', 'UserControllers\UserController@changeAvatar');
        Route::post('user/changePassword', 'UserControllers\UserController@changePassword');
        Route::get('participations', 'UserControllers\UserController@ownParticipations');

        Route::post('user', 'UserControllers\UserController@store');
        Route::get('user/{id}/edit', 'UserControllers\UserController@edit');
        Route::put('user/{id}', 'UserControllers\UserController@update');


        //course routes
        Route::get('course/{id}/edit', 'CourseControllers\CourseController@edit');
        Route::put('course/{id}', 'CourseControllers\CourseController@update');
        Route::post('course', 'CourseControllers\CourseController@store');
        Route::delete('course/{id}', 'CourseControllers\CourseController@destroy');

        //upload routes
        Route::post('upload', 'Shared\UploadController@uploadImages');


        //course opinions
        Route::resource('opinion', 'OpinionControllers\OpinionController');

        //course participation
        Route::post('participation', 'CourseControllers\CourseParticipationController@store');
        Route::delete('participation/delete', 'CourseControllers\CourseParticipationController@destroyParticipation');

        //user records
        Route::resource('records', 'UserControllers\UserRecordsController');

        //friendship
        Route::post('friend/request', 'Friendship\FriendshipController@store');
        Route::post('friend/accept', 'Friendship\FriendshipController@acceptFriend');
        Route::delete('friend/delete', 'Friendship\FriendshipController@delete');
        Route::post('friend/decline', 'Friendship\FriendshipController@decline');
        Route::get('friend/friendRequests', 'Friendship\FriendshipController@index');

        //pusher
        Route::post('pusher/auth', 'UserControllers\UserController@pusherAuth');

        // Notifications friend/request
        Route::get('notifications', 'NotificationControllers\NotificationController@index');
        Route::put('notifications', 'NotificationControllers\NotificationController@updateList');
        Route::get('notifications/{id}', 'NotificationControllers\NotificationController@show');

        //friends
        Route::post('block', 'Friendship\FriendsController@block');
        Route::post('unblock', 'Friendship\FriendsController@unblock');
        Route::get('friends', 'Friendship\FriendsController@index');
        Route::get('friends/{id}', 'Friendship\FriendsController@show');
        Route::get('friendlist', 'Friendship\FriendsController@friendlist');
        Route::delete('friends', 'Friendship\FriendsController@deleteFriend');

        //Chat routes
        Route::get('chat', 'Chat\ChatController@index');
        Route::post('chat', 'Chat\ChatController@store');
        Route::delete('chat/{id}', 'Chat\ChatController@delete');
        Route::get('chat/room_chat', 'Chat\ChatController@roomChat');
        Route::get('chat/room', 'Chat\ChatController@getRoom');
        Route::post('chat/read', 'Chat\ChatController@setMessagesRead');

        //GPX Upload
        Route::post('gpx/upload', 'Shared\GPXDownloadController@upload');

        //admin middleware
        Route::group(['middleware' => ['admin']], function () {
            Route::get('article/{id}/edit', 'Article\ArticleController@edit');
            Route::post('article', 'Article\ArticleController@store');
            Route::put('article/{id}', 'Article\ArticleController@update');
            Route::delete('article/{id}', 'Article\ArticleController@destroy');
            Route::get('categories', 'Article\ArticleController@categories');
        });

        //answer and question
        //question
        Route::get('question', 'QuestionAndAnswer\QuestionController@index');
        Route::post('question', 'QuestionAndAnswer\QuestionController@store');
        Route::put('question/{id}', 'QuestionAndAnswer\QuestionController@update');
        Route::delete('question/{id}', 'QuestionAndAnswer\QuestionController@destroy');
        //answer
        Route::post('answer', 'QuestionAndAnswer\AnswerController@store');
        Route::get('answer/{id}/edit', 'QuestionAndAnswer\AnswerController@edit');
        Route::put('answer/{id}', 'QuestionAndAnswer\AnswerController@update');
        Route::delete('answer/{id}', 'QuestionAndAnswer\AnswerController@destroy');

        Route::post("avia",'CourseControllers\CourseController@avia');
    });
});