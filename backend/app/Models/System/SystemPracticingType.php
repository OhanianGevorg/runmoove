<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class SystemPracticingType extends Model
{

    protected $table = 'system_practicing_table';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];

}
