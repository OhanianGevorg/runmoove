<?php

namespace App\Jobs;

use App\Http\Requests\CourseRequests\APICallRequest;
use App\Models\Course\CourseDate;
use App\Models\Course\TravelApi;
use App\Services\TravelAPI\AbstractGetAPIData;
use App\Services\TravelAPI\FlightAPIService;
use App\Services\TravelAPI\TravelAPIService;
use App\Services\TravelAPI\TravelPayoutService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CallTravelsApi implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $course_date;

    private $iterator = 0;

    private $request;

    /**
     * CallTravelsApi constructor.
     * @param CourseDate $courseDate
     * @param array $request
     */
    public function __construct(CourseDate $courseDate, array $request)
    {
        $this->course_date = $courseDate;
        $this->request = $request;
    }

    /**
     * @return bool|void
     */
    public function handle()
    {
//        if (Carbon::now()->diffInDays(Carbon::createFromTimestamp($this->course_date->start_at)->subDay(1)) > 0) {
//            return false;
//        }
        $params = [
            'checkIn' => '2017-09-10',
//            'checkIn' => Carbon::createFromTimestamp($this->course_date->start_at)->subDay(1)->toDateString(),
            'checkOut' => '2017-09-21',
//            'checkOut' => Carbon::createFromTimestamp($this->course_date->end_at)->addDay(1)->toDateString(),
        ];

        $travelAPI = new TravelAPIService([
            'lng' => $this->course_date->course->longitude,
            'lat' => $this->course_date->course->latitude
        ]);

        $flightAPI = new FlightAPIService([
            'host' => 'https://runmoov.com',
            'user_ip' => array_get($this->request, 'user_ip'),
        ], [
            'origin' => array_get($this->request, 'origin'),
//            'origin' => array_get($this->request,'origin_iata'),
            'destination' => array_get($this->request, 'destination')
//            'destination' => array_get($this->request,'destination_iata')
        ], [
//            'departure' => Carbon::createFromTimestamp($this->course_date->start_at)->subDay(1)->toDateString(),
            'departure' => '2017-09-10',
//            'return' => Carbon::createFromTimestamp($this->course_date->end_at)->addDay(1)->toDateString()
            'return' => '2017-09-21'
        ]);

        if (TravelApi::canCallApi($this->course_date, TravelApi::HOTELS)) {
            $this->callApi($travelAPI, TravelApi::HOTELS, $params);
        }
        if (TravelApi::canCallApi($this->course_date, TravelApi::AVIA)) {
            $this->callApi($flightAPI, TravelApi::AVIA);
        }
    }

    private function callApi(AbstractGetAPIData $travelAPI, $type, array $params = [])
    {
        try {
            $data = $travelAPI->getAPIData($params);
            if ($data) {
                $table = new TravelPayoutService($this->course_date, $data, TravelApi::typeIs($type));
                $success = $table->fillData();
                if ($success) {
                    $this->iterator = 0;
                }
            }
        } catch (\InvalidArgumentException $e) {

        } catch (\Exception $e) {
            if ($e->getCode() == 4 && $this->iterator < 2) {
                $this->iterator += 1;
                $this->callApi($travelAPI, $params);
            }
        }
    }
}
