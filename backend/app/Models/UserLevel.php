<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    //
    protected $table = 'user_level';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'level_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(SystemLevels::class, 'level_id');
    }
}
