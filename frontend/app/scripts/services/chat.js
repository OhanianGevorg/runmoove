'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.chat
 * @description
 * # chat
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('Chat', function (ENV,$resource) {
    return $resource(ENV.apiEndpoint + '/chat', {}, {
      get:{
        method:'GET'
      },
      roomChat:{
        method:'GET',
        url:ENV.apiEndpoint + '/chat/room_chat'
      },
      getRoom:{
        method:'GET',
        url:ENV.apiEndpoint + '/chat/room'
      },
      delete:{
        method:'DELETE',
        url:ENV.apiEndpoint + '/chat/:id'
      },
      setMessagesRead: {
        method:'POST',
        url:ENV.apiEndpoint + '/chat/read'
      },

    });
  });
