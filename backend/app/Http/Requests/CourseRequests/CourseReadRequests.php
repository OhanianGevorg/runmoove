<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class CourseReadRequests extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_at'=>'date',
            'end_at'=>'date',
            'name'=>'string|max:255',
            'types'=>'string|max:255',
            'longitude'=>'numeric',
            'latitude'=>'numeric',
            'distance'=>'numeric'
        ];
    }
}
