<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestapsToUserLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('user_level','created_at')&&!Schema::hasColumn('user_level','updated_at')){
            Schema::table('user_level', function (Blueprint $table) {
                $table->timestamps();
            });
        }
        Schema::table('user_level', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('user_level','created_at')&&Schema::hasColumn('user_level','updated_at')){
            Schema::table('user_level', function (Blueprint $table) {
                $table->timestamps();
            });
        }
        Schema::table('user_level', function (Blueprint $table) {
            //
            $table->dropColumn('deleted_at');
        });
    }
}
