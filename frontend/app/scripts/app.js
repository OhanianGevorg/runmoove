'use strict';

/**
 * @ngdoc overview
 * @name runMoovApp
 * @description
 * # runMoovApp
 *
 * Main module of the application.
 */
angular
  .module('runMoovApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'config',
    'ngStorage',
    'ngAria',
    'ngMaterial',
    'duScroll',
    'checklist-model',
    'ngDialog',
    'google.places',
    'twitterApp.services',
    'thatisuday.dropzone',
    'angularMoment',
    'rzModule',
    'ui.router.state.events',
    'ng-fx',
    'md.time.picker',
    'ui.bootstrap',
    'angucomplete-alt',
    'pusher-angular',
    'emoji',
    'sc.twemoji',
    'ngYoutubeEmbed',
    'ui.calendar'
  ])

  .config(function ($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/run/home');

    // $locationProvider.html5Mode(true);

    $stateProvider
      .state('master', {
        url: '',
        abstract: true,
        views: {
          '': {
            template: '<div ui-view/>'
          },
          'header': {
            templateUrl: 'views/layout/header.html'
          },
          'footer': {
            templateUrl: 'views/layout/footer.html'
          }
        }
      })

      /****** Run *****/
      .state('run', {
        url: '',
        abstract: true,
        parent: 'master',
      })
      .state('run.home', {
        url: '/run/home',
        templateUrl: 'views/pages/run/main.html',
        controller: 'RunMainCtrl',
        login: false,
      })

      .state('run.add', {
        url: '/run/add',
        templateUrl: 'views/pages/run/create.html',
        controller: 'RunNewCtrl',
        login: true,
      })

      .state('run.profile', {
        url: '/run/profile/:id',
        templateUrl: 'views/pages/run/profile.html',
        controller: 'RunProfileCtrl',
        login: false
      })
      .state('run.article_create', {
        url: '/run/article/new',
        templateUrl: 'views/pages/articles/create.html',
        controller: 'ArticlesCtrl',
        login: true
      })
      .state('run.article_profile', {
        url: '/run/article/:id',
        templateUrl: 'views/pages/articles/profile.html',
        controller: 'ArticleProfileCtrl',
        login: true
      })
      .state('run.article_edit', {
        url: '/run/article/edit/:id',
        templateUrl: 'views/pages/articles/edit.html',
        controller: 'ArticleEditCtrl',
        login: true
      })

      /*** End Run ***/
      .state('auth', {
        url: '/auth',
        templateUrl: 'views/pages/auth/main.html',
        parent: 'master',
        controller: 'AuthCtrl',
        login: true,
      })

      /* user Profile */
      .state('profile', {
        url: '',
        abstract: true,
        parent: 'master',
      })
      .state('profile.view', {
        url: '/profile/:id',
        templateUrl: 'views/pages/user/profile.html',
        controller: 'ProfileCtrl',
        login: true,
      })
      .state('profile.update', {
        url: '/profile/update/:id',
        templateUrl: 'views/pages/user/update.html',
        controller: 'ProfileUpdateCtrl',
        login: true,
      })
      .state('profile.edit', {
        url: '/profile/edit/:id',
        templateUrl: 'views/pages/user/edit.html',
        controller: 'ProfileEditCtrl',
        login: true,
      })
      .state('profile.records', {
        url: '/profile/records/:id',
        templateUrl: 'views/pages/user/records.html',
        controller: 'UserRecordsCtrl',
        login: true,
      })
      /* End User Profile */
      .state('friends', {
        url: '',
        abstract: true,
        parent: 'master',
      })
      .state('friends.list', {
        url: '/friends',
        templateUrl: 'views/pages/user/friends.html',
        controller: 'FriendsCtrl',
        login: true,
      })
      /* User friends */



      /* End User friends */


      .state('schedule', {
        url: '',
        abstract: true,
        parent: 'master',
      })
      .state('schedule.show', {
        url: '/schedule/:id',
        templateUrl: 'views/pages/schedule.html',
        controller: 'ScheduleCtrl',
        login: true,
      })

      /* Opinions */

    .state('opinion', {
      url: '',
      abstract: true,
      parent: 'master',
    })
      .state('opinion.main', {
        url: '/opinion/runs',
        templateUrl: 'views/pages/run/run_opinions/main.html',
        controller: 'RunOpinionsMainCtrl',
        login: true,
      })
      .state('opinion.new', {
        url: '/opinion/run/new/:id',
        templateUrl: 'views/pages/run/run_opinions/create.html',
        controller: 'RunOpinionsNewCtrl',
        login: true,
      })
      /* End opinions */



    .state('chat', {
      url: '',
      abstract: true,
      parent: 'master',
    })
      .state('chat.messages', {
        url: '/messages',
        templateUrl: 'views/pusher/messages.html',
        controller: 'ChatCtrl',
        login: true,
      })

  })


  .run(function ($rootScope, $state, AuthService,amMoment,$timeout) {
    amMoment.changeLocale('fr');

    $rootScope.$on('$stateChangeStart', function (event, next, toState, toParams, fromState, fromParams) {
      if (next.login !== undefined && next.login && !AuthService.isAuthenticated()) {
        $timeout(function () {  $state.go('auth'); },100);
      }
      var body = angular.element("html, body");
      body.stop().animate({scrollTop: 0}, 500, 'swing', function () {
      });

      $rootScope.searchRunCriteria = {
        'location':'',
        'start_at':'',
        'end_at':'',
        'name':'',
        'distance':'',
        'types':'',
        'longitude':'',
        'latitude':''
      };

      $rootScope.filterField = {
        address: [],
        types: []
      };

    });

  })
