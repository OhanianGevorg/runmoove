<?php

namespace App\Providers;

use App\Services\LevelCount\CountLevel;
use App\Services\LevelCount\LevelCountKernel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class LevelCountProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach (LevelCountKernel::getLevelUpClasses() as $entity=>$score){

            if (class_exists($entity)){
                /**
                 * @var $entity Model
                 */
                $entity::created(function ($model){
                   try{
                       $countLevel = new CountLevel($model);
                       $countLevel->setUserScore();
                   }catch (\Exception $e){
                       return $e->getMessage();
                   }
                });
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
