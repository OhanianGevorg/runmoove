<?php

namespace App\Models\Opinion;

use App\Models\Course\CourseDate;
use App\Models\System\SystemCourseFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseOpinionFormat extends Model
{
    //
    use SoftDeletes;
    protected $table = 'course_opinion_formats';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_opinion_id',
        'format_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function format()
    {
        return $this->belongsTo(SystemCourseFormat::class, 'format_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course_opinion()
    {
        return $this->belongsTo(Opinion::class, 'course_opinion_id');
    }
}
