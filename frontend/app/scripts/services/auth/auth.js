'use strict';

angular.module('runMoovApp')
  .service('AuthService', function ($http, ENV, $rootScope, AuthEvents, $window, $localStorage, $timeout, $state, ngDialog, $injector, userRoles) {

    var setSession = function (response) {
      var user = response.user;
      $localStorage.api_token = response.api_token;
      $localStorage.isAuthenticated = true;
      $localStorage.role = parseInt(user.role_id);
      $localStorage.user_type = parseInt(user.user_type);
      $localStorage.userID = parseInt(user.id);
      $localStorage.avatar = user.avatar;
      $localStorage.avatar_base64 = user.avatar_base64;
      // $localStorage.FBavatar = "https://graph.facebook.com/" + user.fb_user_id + "/picture?type=large";
      $localStorage.siteName = user.site_name;
      $localStorage.name = user.first_name + ' ' + user.last_name;
      $localStorage.first_name = user.first_name;
      $localStorage.last_name = user.last_name;
      $localStorage.address = user.address;
      $localStorage.level = user.user_level;

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          $localStorage.currentLng = position.coords.longitude;
          $localStorage.currentLat = position.coords.latitude;
        });
      }
    };

    var destroySession = function () {
      delete $localStorage.api_token;
      delete $localStorage.isAuthenticated;
      delete $localStorage.role;
      delete $localStorage.userID;
      delete $localStorage.avatar;
      delete $localStorage.siteName;
      delete $localStorage.FBavatar;
      delete $localStorage.FBfriends;
      delete $localStorage.first_name;
      delete $localStorage.last_name;
      delete $localStorage.address;
      delete $localStorage.level;
      delete $localStorage.currentLng;
      delete $localStorage.currentLat;
    };

    var login = function (credentials) {
      return $http.post(ENV.apiEndpoint + '/login', credentials).then(function (res) {
        console.log(res);

        if (!res.data.hasOwnProperty('errors')) {
          // $rootScope.openPopup('views/auth/signup/popups/logging_in.html');

          setSession(res.data);
          $rootScope.$broadcast(AuthEvents.loginSuccess);
          // $rootScope.closeAllPopups();

        }
        return res.data;
      });
    };

    var facebookLogin = function (credentials) {
      return $http.post(ENV.apiEndpoint + '/auth/facebook', credentials).then(function (res) {
        $timeout(function () {
          if (!res.data.hasOwnProperty('errors')) {
            // setSession(res.data.user);
            $rootScope.$broadcast(AuthEvents.loginSuccess);
          }
        }, 100);
        return res.data;

      });
    };


    var twitterLogin = function (credentials, authUserData) {
      $rootScope.authUserData = authUserData;
      return $http.post(ENV.apiEndpoint + '/auth/twitter', credentials).then(function (res) {


        if (res.data.auth_type == 'sign_up') {

          return res.data;

        } else {
          if (!res.data.hasOwnProperty('errors')) {

            $timeout(function () {
              // setSession(res.data.user);
              $rootScope.$broadcast(AuthEvents.loginSuccess);
              $rootScope.closeAllPopups();
            }, 100);
          }
          return res.data;
        }
      });
    };



    var logout = function () {

      return $http.post(ENV.apiEndpoint + '/logout').then(function (res) {

        if (res.data.hasOwnProperty('success')) {
          $injector.get('PusherService').disconnect();
          destroySession();
          $rootScope.openLogin = true;
          $timeout(function () {
            $state.go('auth');
          }, 200);
        }

      });
      // delete $localStorage.api_token;
      // delete $localStorage.isAuthenticated;
      // delete $localStorage.role;
      // delete $localStorage.userID;
      // delete $localStorage.avatar;
      // delete $localStorage.siteName;
      // delete $localStorage.FBavatar

    };

    var setAvatar = function (avatar) {
      $localStorage.avatar = avatar;
    };

    var isAuthenticated = function () {
      return $localStorage.isAuthenticated;
    };

    var getRole = function () {
      return $localStorage.role || false;
    };

    var getUserID = function () {
      return $localStorage.userID || false;
    };

    var getAvatar = function () {
      return $localStorage.avatar || false;
    };

    var getBase64Avatar = function () {
      return $localStorage.avatar_base64 || false;
    };

    var getSiteName = function () {
      return $localStorage.siteName || false;
    };

    var getName = function () {
      return $localStorage.name || false;
    };

    var getFirstName = function () {
      return $localStorage.first_name || false;
    };

    var getType = function () {
      return $localStorage.user_type || false;
    };

    var getAddress = function () {
      return $localStorage.address || false;
    };

    var getLevelName = function () {
      return $localStorage.level.name || false;
    };

    var isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }

      return (isAuthenticated() && authorizedRoles.indexOf(getRole()) !== -1);
    };

    var isAdmin = function () {
      return getRole() === userRoles.admin;
    };

    var getLng = function () {
      return $localStorage.currentLng || false;
    };

    var getLat = function () {
      return $localStorage.currentLat || false;
    };


    return {
      login: login,
      facebookLogin: facebookLogin,
      twitterLogin: twitterLogin,
      logout: logout,
      isAuthorized: isAuthorized,
      setAvatar: setAvatar,
      isAuthenticated: function () {
        return isAuthenticated();
      },
      userID: function () {
        return getUserID();
      },
      avatar: function () {
        return getAvatar();
      },
      base64Avatar: function () {
        return getBase64Avatar();
      },
      siteName: function () {
        return getSiteName();
      },
      role: function () {
        return getRole();
      },
      name: function () {
        return getName();
      },
      firstName: function () {
        return getFirstName();
      },
      type: function () {
        return getType();
      },
      address: function () {
        return getAddress();
      },
      isAdmin: function () {
        return isAdmin();
      },
      levelName: function () {
        return getLevelName();
      },
      currentLng: function () {
        return getLng();
      },

      currentLat: function () {
        return getLat();
      },

      destroySession: destroySession
    };
  });
