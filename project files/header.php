<!doctype html>
<html>
<head>
	<!-- meta -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- title -->
	<title></title>

	<!-- css -->
	<link rel="stylesheet" href="css/fullcalendar.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="css/slick.css"/>
	<link rel="stylesheet" href="css/slick-theme.css"/>
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/lity.css">
	<link rel="stylesheet" href="css/jssor.css">
	<link rel="stylesheet" href="css/style.css">

	<!-- scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="">
	<div class="wrapper">
		<!-- HEADER -->
		<header>
			<div class="container-fluid mobile-hide">
				<div class="row">
					<div class="col-lg-5 header-left-part">
						<a href="#"><img src="img/logo-h.png" alt="logo header" style="width:300px"></a>
					</div>
					<div class="col-lg-7 header-right-part">
						<nav>
							<div class="row">
								<div class="col-sm-3  col-sm-offset-1 menu-item">
									<a href="#"><img src="img/edit-icon.png" alt="edit icon">Laisser un avis</a>
								</div>
								<div class="col-sm-3 menu-item">
									<a href="#"><img src="img/envelope-with-msg-icon.png" alt="envelope with msg icon"><span class="messages-qty">7</span>Messagerie</a>
								</div>
								<div class="col-sm-3 menu-item">
									<a href="#"><img src="img/notifications-with-msg-icon.png" alt="bell with msg icon"><span class="notifications-qty">5</span>Notifications</a>
																	</div>
								<div class="col-sm-2 header-item-fix menu-item">
									<p>Manon</p>
									<a class="asd" href="#"><img class="profile-img" src="img/bonjor-icon.png" alt="bonjor icon"></a>
									<div class="profile-menu">
										<ul>
											<li><a href="#"><img src="img/settings-icon.png" alt="settings icon">Mon compte</a></li>
											<li><a href="#"><img src="img/calendar-icon.png" alt="calendar icon">Calendrier</a></li>
											<li><a href="#"><img src="img/big-trophy-icon.png" alt="big trophy icon">Mes records</a></li>
											<li><a href="#"><img src="img/friends-icon.png" alt="friend icon">Mes amis</a></li>
											<li><a href="#"><img src="img/disconnect-icon.png" alt="disconnect icon">Me déconnecter</a></li>
										</ul>
										<span class="tr"></span>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>

			<!-- mobile header -->
			<div class="logo middle-hide-and-more">
				<a href="#" rel="home"><img class="img-responsive" src="img/logo-header.png" alt="logo header"></a>
			</div>
			<div class="mobile-nav middle-hide-and-more">
				<div class="menu-btn">
					<div></div>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="responsive-menu">
					<ul id="menu-mobile-menu">
						<li><a href="#"><img src="img/bonjor-icon.png" alt="bonjor icon">Bonjour Manon!</a></li>
						<li><a href="#"><img src="img/settings-icon.png" alt="settings icon">Mon compte</a></li>
						<li><a href="#"><img src="img/notifications-with-msg-icon.png" alt="bell with msg icon"><span class="notifications-qty">5</span>Notifications</a></li>
						<li><a href="#"><img src="img/friends-icon.png" alt="friend icon">Mes amis</a></li>
						<li><a href="#"><img src="img/envelope-with-msg-icon.png" alt="envelope with msg icon"><span class="messages-qty">7</span>Messagerie</a></li>
						<li><a href="#"><img src="img/big-trophy-icon.png" alt="big trophy icon">Mes records</a></li>
						<li><a href="#"><img src="img/edit-icon.png" alt="edit icon">Laisser un avis</a></li>
						<li><a href="#"><img src="img/disconnect-icon.png" alt="disconnect icon">Me déconnecter</a></li>
					</ul>
				</div>
			</div>
			<!-- mobile header end -->

		</header>
		<!-- HEADER end -->
