<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/1/2017
 * Time: 12:47 AM
 */

namespace App\Transformers\Article;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Article\Categories;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class CategoryTransformer implements TransformerInterface, PrepareInterface
{

    use TransformCollection, PrepareCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id'=>$model->getKey(),
            'name' => $model->name
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new Categories([
            'name' => array_get($raw, 'name')
        ]);
    }
}