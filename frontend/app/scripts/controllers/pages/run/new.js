'use strict';

angular.module('runMoovApp')

  .controller('RunNewCtrl', function ($scope, systemConstants, AuthService, Runs, ENV, $state, $localStorage, moment,$window,$timeout) {

    $scope.data = {
      'user_id': AuthService.userID(),
      'latitude': '',
      'longitude': '',
      'distance': 10,
      'start_at': '',
      'end_at': '',
      'name': '',
      'location': '',
      'organizer_email': '',
      'description': '',
      'organizer_telephone': '',
      'site': '',
      'registration_fee': '',
      'participants_number': '',
      'course_images': [],
      'course_formats': [],
      'practicing_types': [],
      'course_markers': [],
      'license_agreement': 'false',
    };


    $scope.testVar = false;





    $scope.geo = {
      address: []
    };

    $scope.location = {
      'latitude':'',
      'longitude':''
    };
    $scope.start = {};

    $scope.chekedDatas = {
      course_formats: [],
      practicing_types: []
    };

    $scope.activeStep = 'first';

    $scope.UserType = AuthService.type();


    $scope.nextStep = function (step) {
      $scope.activeStep = step;

      if (step == 'second') {
        if(!Object.keys($scope.geo.address).length){
          $scope.location['latitude'] = 0;
          $scope.location['longitude'] = 0;
        }else {
          $scope.location['latitude'] = $scope.geo.address.geometry.location.lat();
          $scope.location['longitude'] =$scope.geo.address.geometry.location.lng();
        }
        $timeout(function () {
          $scope.start.init($scope.location);
        },500);
      }
      if (step == 'first') {
        $scope.start.clearMap();
      }
      var body = angular.element("html, body");
      body.stop().animate({scrollTop: 0}, 500, 'swing', function () {
      });

    };

    $scope.previewMode = function (bool) {
      if (bool == true) {
        $scope.activeStep = '';
      } else {
        $scope.activeStep = 'third';
      }
      $scope.isPreviewMode = bool;
    };


    /**
     * get System Constants
     */
    systemConstants.get(function (res) {
      $scope.systemTypes = res.system_types;
    });



    $scope.distanceSlider = {
      value:0,
      options: {
        floor: 0,
        ceil: 100,
        step: 1,
        onEnd: function(id) {
          $scope.data.distance = $scope.distanceSlider.value;
        }
      }
    };


    // $scope.uploadGpxToggle = function () {
    //   $timeout(function () {
    //     angular.element('#gpxFileUpload').click();
    //   },100);
    // };

    $scope.getMyGpxFile = function (e) {
      console.log(angular.element(e.target));
    };


    // $('body').on('change','#gpxFileUpload',function(e){
    //   $scope.fileObj = 45;
    //   $scope.fileObj = e.target.files[0];
    //   console.log($scope.fileObj);
    //
    // });

    $scope.saveRun = function () {

      $scope.data.course_formats = $scope.chekedDatas.course_formats;
      $scope.data.practicing_types = $scope.chekedDatas.practicing_types;
      for (var n in $scope.data) {
        if (n == "course_images" || n == "course_markers") {
          continue;
        }
        if (Array.isArray($scope.data[n])) {
          $scope.data[n] = $scope.data[n].join();
        }
      }


      if (Object.keys($scope.geo.address).length > 0) {
        $scope.data.location = $scope.geo.address.formatted_address;
        $scope.data.latitude = $scope.geo.address.geometry.location.lat();
        $scope.data.longitude = $scope.geo.address.geometry.location.lng();
      }

      Runs.save($scope.data, function (res) {
        if (res.hasOwnProperty('success')) {
          $state.go('run.home');
        }
      }, function (error) {
        $scope.errors = error.data;
      });

    };


  });
