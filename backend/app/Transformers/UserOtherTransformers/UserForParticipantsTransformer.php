<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/19/2017
 * Time: 11:29 PM
 */

namespace App\Transformers\UserOtherTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use Illuminate\Support\Facades\Storage;

class UserForParticipantsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [

            ];
        }
        return [
            'id' => $model->getKey(),
            'avatar' => (!filter_var($model->avatar, FILTER_VALIDATE_URL) && Storage::disk('public')->exists($model->avatar)) ? asset(Storage::url($model->avatar)) : $model->avatar
        ];
    }
}