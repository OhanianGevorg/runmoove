<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Broadcast::routes([
//            'before', 'jsonify', 'ownAuth', 'after'
//        ]);

        /*
         * Authenticate the user's personal channel...
         */
//        Broadcast::channel('private-notifications.*', function ($user, $userId) {
//            return (int)User::auth()->getKey() === (int)$userId;
//        });
//        Broadcast::channel('private-chat.*', function ($user, $userId) {
//            return (int)User::auth()->getKey() === (int)$userId;
//        });
    }
}
