<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/27/17
 * Time: 10:10 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Transformers\UserTransformer;
use Carbon\Carbon;

class CourseSmallTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'official_organizer_id'=>$model->official_organizer_id,
            'start_at' => Carbon::createFromTimestamp((int)$model->start_at)->toRfc822String(),
            'end_at' => Carbon::createFromTimestamp((int)$model->end_at)->toRfc822String(),
            'totals' => $model->getStarsCount(),
            'reviews_count' => $model->getReviewCount(),
            'participants_count' => $model->participants_count(),
            'course' =>CourseTransformer::courseXsTransformer($model->course),
            'user'=>UserTransformer::smallTransformer($model->user),
            'created_at' => $model->created_at
        ];
    }
}