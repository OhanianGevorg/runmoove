<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/18/2017
 * Time: 5:04 PM
 */

namespace App\Services\LevelCount;


use App\Models\SystemLevels;
use App\Models\UserLevel;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CountLevel
{

    private $model;
    private $score;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @desc method for count user score set it level
     * @throws \Exception
     */
    public function setUserScore()
    {
        $user = User::find($this->model->user_id);

        if (!$user) {
            throw new NotFoundHttpException('This user doesn\'t exists');
        }

        $this->score = array_get(LevelCountKernel::getLevelUpClasses(), get_class($this->model));

        if (!$this->score) {
            throw new NotFoundHttpException('Class does not support this action');
        }

        /**
         * @var $user User
         */

        $score = $user->score + $this->score;

        $user->score = $score;
        $is_saved = $user->save();

        if (!$is_saved) {
            throw new \Exception(('User data is not saved' . "\n" . $user->name . "\n" . "\n" . "$user->score"));
        }
        /**
         * @desc call UserLevel method for change user level
         */
        $user_level = $user->setLevel();
        $isLevelSet = $user_level->save();
        if (!$isLevelSet) {
            throw new \Exception('User level data is not saved' . "\n" . $user->name . "\n" . "\n" . "$user->score");
        }
    }
}