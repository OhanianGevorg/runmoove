<?php

namespace App\Http\Controllers\Article;

use App\Http\Requests\Article\ArticleReadRequest;
use App\Http\Requests\Article\ArticleStoreRequest;
use App\Models\Article\Article;
use App\Models\Article\Categories;
use App\Transformers\Article\ArticleImageTransformer;
use App\Transformers\Article\ArticleTransformer;
use App\Transformers\Article\ArticleVideoTransformer;
use App\Transformers\Article\CategoryTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * @desc set middleware permissions
     * ArticleController constructor.
     */
    public function __construct()
    {
//        $this->middleware('ownAuth', ['except' => ['index', 'show']]);
//        $this->middleware('admin', ['except' => ['index', 'show']]);

    }

    /**
     * @desc show all articles
     * @param ArticleReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ArticleReadRequest $request)
    {
        try {
            $articles = Article::getAll($request->all());
            return response()->json([
                'data' => ArticleTransformer::transformCollection($articles)
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }


    }

    public function store(ArticleStoreRequest $request)
    {
        if (!User::auth() || !User::auth()->canWriteArticle()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 401);
        }
        DB::beginTransaction();
        try {
            $article = ArticleTransformer::prepare($request->all(), ['user_id' => User::auth()->getKey()]);
            $article->save();
            if ($request->has('images')) {
                $article->images()->saveMany(ArticleImageTransformer::prepareCollection($request->get('images'), ['article_id' => $article->getKey()]));
            }
            if ($request->has('videos')) {
                $article->videos()->saveMany(ArticleVideoTransformer::prepareCollection($request->get('videos'), ['article_id' => $article->getKey()]));
            }
            DB::commit();
            return response()->json([
                'success' => 'Article has successfully created'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function edit($id)
    {
        if (!User::auth() || !User::auth()->canWriteArticle()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 401);
        }

        try {
            /**
             * @var $article Article
             */
            $article = User::auth()->articles()->where('id', '=', $id)->firstOrFail();
            return response()->json([
                'data' => ArticleTransformer::transform($article)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Article not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function update(ArticleStoreRequest $request, $id)
    {
        if (!User::auth() || !User::auth()->canWriteArticle()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 401);
        }

        try {
            /**
             * @var $article Article
             */
            $article = User::auth()->articles()->where('id', '=', $id)->firstOrFail();
            DB::beginTransaction();

            $article->update(ArticleTransformer::prepareUpdate($request->all()));
            if ($request->has('images')) {
                $article->images()->delete();
                $article->images()->saveMany(ArticleImageTransformer::prepareCollection($request->get('images'), ['article_id' => $article->getKey()]));
            }
            if ($request->has('videos')) {
                $article->videos()->delete();
                $article->videos()->saveMany(ArticleVideoTransformer::prepareCollection($request->get('videos'), ['article_id' => $article->getKey()]));
            }
            DB::commit();
            return response()->json([
                'success' => 'Article has successfully updated',
                'data' => ArticleTransformer::transform($article)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Article not found'
            ], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @desc show single article
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            /**
             * @var $article Article
             */
            $article = Article::findOrFail($id);
            return response()->json([
                'data' => ArticleTransformer::transform($article)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Article not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @desc delete article and own related records
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (!User::auth() || !User::auth()->canWriteArticle()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 401);
        }
        try {
            /**
             * @var $article Article
             */
            $article = User::auth()->articles()->where('id', '=', $id)->firstOrFail();
            DB::beginTransaction();
            $article->images()->delete();
            $article->videos()->delete();
            $article->delete();
            DB::commit();
            return response()->json([
                'success' => 'Article has deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Article not found'
            ], 404);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }

    }

    /**
     * @desc return article categories
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories()
    {
        if (!User::auth() || !User::auth()->canWriteArticle()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 401);
        }
        return response()->json([
            'categories' => CategoryTransformer::transformCollection(Categories::all())
        ], 200);
    }

}
