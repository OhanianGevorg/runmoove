<?php

namespace App\Http\Requests\QuestionAndAnswer;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class QuestionStoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_date_id' => 'required|integer|exists:course_dates,id',
            'body' => 'required|max:65000'
        ];
    }
}
