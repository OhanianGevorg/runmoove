<?php $title ='Contact'; $bodyclass ='fifth-page contact-page'; require_once 'header.php'; ?>

		<!-- MAIN CONTENT -->
		<main>
			<div class="container  contact-text">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 m-b-20">
						<h1 class="text-center">Nous contacter</h1>
						<p>
							Si vous souhaitez nous faire part de vos suggestions, nous poser des questions ou bien tout
							simplement bavarder, n’hésitez pas, toute notre équipe est à votre service !
							Envoyez nous un email à l’adresse contact@runmoov.com ou pro@runmoov.com pour les
							professionnels.
						</p>
						<p>
							Passez nous un coup de fil : +33 6 66 39 22 33
						</p>
					</div>

				</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2 left-part">
						<p>Ou, utilisez notre formulaire de contact ci-dessous  </p>
						<form action="#">
							<div class="input-box choice">
								<select>
									<option value="">A propos de mon compte</option>
									<option value="">Mon statut Organisateur</option>
									<option value="">Question sur une course</option>
									<option value="">Signaler un problème</option>
									<option value="">Questions générales sur le site</option>
									<option value="">Nous contacter</option>
								</select>
							</div>
							<div class="input-box last-name">
								<input type="text" placeholder="Nom">
							</div>
							<div class="input-box email">
								<input type="email" placeholder="Email">
							</div>
							<textarea rows="4" cols="50" placeholder="Message"></textarea>
							<input type="checkbox" id="checkbox1" class="css-checkbox">
							<label for="checkbox1" name="checkbox" class="css-label checkbox">
								L’actualité des courses, dans votre boite mail
							</label>
							<input type="checkbox" id="checkbox2" class="css-checkbox">
							<label for="checkbox2" name="checkbox" class="css-label checkbox">
								Les courses près de chez vous, dans votre boite mail
							</label>
							<input type="checkbox" id="checkbox3" class="css-checkbox">
							<label for="checkbox3" name="checkbox" class="css-label checkbox">
								Notre sélection de course pour vous, dans votre boite mail
							</label>
							<button>ENVOYER</button>
						</form>
					</div>
				</div>
			</div>
		</main>
		<!-- MAIN CONTENT end -->

	<?php require_once 'footer.php'; ?>
