<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/18/2017
 * Time: 12:22 AM
 */

namespace App\Transformers\Chat;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Chat\Message;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;

class MessageTransformer implements PrepareInterface, TransformerInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Message([
            'user_id' => array_get($raw, 'user_id'),
            'recipient_id' => array_get($raw, 'recipient_id'),
            'body' => array_get($raw, 'body'),
            'room_id' => array_get($raw, 'room_id'),
            'is_read' => array_get($raw, 'is_read')
        ]);
    }

    public static function transform($model)
    {
        /**
         * @var $model Message
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'room_id' => $model->room_id,
            'user_id' => (User::auth() && User::auth()->getKey() === $model->user_id) ? $model->user_id : UserForFriendsTransformer::transform($model->user),
            'recipient_id' => (User::auth() && User::auth()->getKey() === $model->recipient_id) ? $model->recipient_id : UserForFriendsTransformer::transform($model->recipient),
            'body' => html_entity_decode(html_entity_decode(json_decode($model->body))),
            'is_read' => $model->is_read,
            'created_at' => $model->created_at
        ];
    }
}