'use strict';

describe('Controller: PagesRunRunOpinionsNewCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesRunRunOpinionsNewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunRunOpinionsNewCtrl = $controller('PagesRunRunOpinionsNewCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunRunOpinionsNewCtrl.awesomeThings.length).toBe(3);
  });
});
