'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesRunArticlesCtrl
 * @description
 * # PagesRunArticlesCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('ArticlesCtrl', function ($scope,ENV,$localStorage,Articles,$state) {

    $scope.data = {
      title: '',
      body: '',
      date: '',
      videos: [],
      images:[],
      category:1
    };

    $scope.activeType = 'Article';

    Articles.categories(function (res) {
      $scope.categories = res.categories;
    });

    $scope.addMoreVideo = function () {
      $scope.data.videos.push({url: ''});
    };


    $scope.removeVideo = function (idx) {
      $scope.data.videos.splice(idx,1);
    };

    //Set options for dropzone
    //Visit http://www.dropzonejs.com/#configuration-options for more options
    $scope.dzOptions = {
      url: ENV.apiEndpoint + '/upload',
      paramName: 'photo',
      maxFilesize: '30',
      acceptedFiles: 'image/jpeg, images/jpg, image/png',
      addRemoveLinks: true,
      dictRemoveFile: "",
      dictCancelUpload: "",
      clickable: '#addPhotoBtn',
      parallelUploads: 1,
      autoProcessQueue: true
    };


    $scope.InProgress = false;
    //Handle events for dropzone
    //Visit http://www.dropzonejs.com/#events for more events
    $scope.dzCallbacks = {
      addedfile: function (file) {
        if (!$scope.InProgress) {
          $scope.newFile = file;
        }
        $scope.InProgress = true;

      },
      success: function (file, xhr) {
        if (xhr.hasOwnProperty('image')) {

          $scope.data.images.push({file_name: xhr['image']});
          $scope.InProgress = false;
        }

        angular.element('.dz-remove').addClass('fa fa-remove');
      },
      error: function (file) {
        $scope.newFile.length = 0;
      },

      removedfile: function (file) {
        var image = JSON.parse(file.xhr.response);
        var index;
        for (var i = 0; i < $scope.data.images.length; i++) {
          if ($scope.data.images[i].url === image.image) {
            index = i;
            break;
          }
        }
        $scope.data.images.splice(index, 1);
      },
      uploadprogress: function (file, progress) {
        file.progress = progress;
        if (progress === 100) {
          $scope.InProgress = true;
        }
      },
      sending: function (file, xhr) {
        return xhr.setRequestHeader('X-TOKEN', $localStorage.api_token);
      }
    };


    $scope.removeNewFile = function () {
      $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
    };


    $scope.getActiveTypeName = function (event) {
      $scope.activeType =  angular.element(event.target).closest('label').text();
      console.log($scope.activeType);
    };


    $scope.saveArticle = function () {

      Articles.save($scope.data,function (res) {
        toastr.success('Article is added', 'Success message');
        $state.go('run.home');
      },function (err) {
        $scope.errors = err.data;
        toastr.error('Something goes wrong', 'Error message');
      });
    };

  });
