<?php

namespace App\Models\SystemRelated;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCourseFormat extends Model
{
    use SoftDeletes;

    protected $table = 'user_course_format';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'format_id'
    ];
}
