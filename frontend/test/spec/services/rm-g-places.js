'use strict';

describe('Service: rmGPlaces', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var rmGPlaces;
  beforeEach(inject(function (_rmGPlaces_) {
    rmGPlaces = _rmGPlaces_;
  }));

  it('should do something', function () {
    expect(!!rmGPlaces).toBe(true);
  });

});
