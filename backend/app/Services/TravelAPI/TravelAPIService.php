<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/21/2017
 * Time: 12:18 AM
 */

namespace App\Services\TravelAPI;


class TravelAPIService extends AbstractGetAPIData
{
    protected $token = '7d4bddfe4ce17c56ed7f1205a8117f3f';
    protected $marker = '145897';

    private $cityId;
    private $longLat;

    private $searchParams = [];
    /**
     * @var array
     * @desc map array for hotels and cityID queries
     */
    private $uriMap = [
        'hotels' => [
            'search' => [
                'url' => "https://engine.hotellook.com/api/v2/search/start.json",
                'query' => [
                    'marker' => '',
                    'cityId' => '',
                    'checkIn' => '',
                    'checkOut' => '',
                    'adultsCount' => 2,
                    'currency' => 'USD',
                    'waitForResult' => 0,
                    'lang' => 'en'
                ]
            ],
            'result' => [
                'url' => "https://engine.hotellook.com/api/v2/search/getResult.json",
                'query' => [
                    'searchId' => '',
                    'limit' => 4
                ]
            ],
        ],
        'lookup' => [
            'search' => [
                'url' => 'http://engine.hotellook.com/api/v2/lookup.json',
                'query' => [
                    'marker' => '',
                    'lang' => 'us',
                    'lookFor' => 'city',
                    'limit' => 3,
                    'query' => '40.415363,-3.707398'
                ]
            ]
        ]
    ];

    /**
     * TravelAPIService constructor.
     * @param $longLat
     */
    public function __construct(array $longLat)
    {
        $this->longLat = $longLat['lat'] . ',' . $longLat['lng'];
        $this->uriMap['lookup']['search']['query']['query'] = $this->longLat;
    }

    /**
     * @param array $searchData
     * @return array|bool
     * @throws \Exception
     */
    public function getAPIData(array $searchData)
    {
        $this->searchParams = $searchData;
        $cityID = $this->getCityId();
        if (!$cityID) {
            throw new \Exception("No such cityID", 404);
        }
        $searchData['cityId'] = $cityID;
        try {
            $searchID = $this->getSearchID($searchData);
            $hotels = $this->getHotelsQueryData($searchID);
            if (!isset($hotels['status']) || $hotels['status'] !== 'ok') {
                throw new \Exception("Data Not Found",404);
            }
            $result = [
                'content' => $hotels['result'],
                'city_id' => $cityID,
            ];
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $searchId
     * @return mixed
     */
    private function getHotelsQueryData($searchId)
    {
        $this->uriMap['hotels']['result']['query']['searchId'] = $searchId;
        $query = $this->queryBuilder($this->uriMap['hotels']['result']['url'], $this->uriMap['hotels']['result']['query']);
        return $this->callCURL($query);
    }

    /**
     * @return mixed
     */
    private function getCityId()
    {
        try {
            $query = $this->queryBuilder($this->uriMap['lookup']['search']['url'], $this->uriMap['lookup']['search']['query']);
            $response = $this->callCityIdCURL($query)['content'];
            $response = json_decode($response, true);
            $this->cityId = null;
            if (count($response['results']['locations'])) {
                $this->cityId = $response['results']['locations'][0]['id'];
                return $this->cityId;
            }
            return $this->cityId;
        } catch (\Exception $e) {
            if ($e->getCode() == 4) {
                $this->getCityId();
            } else {
                $this->cityId = null;
                return $this->cityId;
            }

        }
    }

    /**
     * @param $url
     * @param array $queryArray
     * @return string
     */
    public function queryBuilder($url, array $queryArray)
    {
        return $url . $this->buildSearchQuery($queryArray);
    }

    /**
     * @param array $data
     * @return null|string
     */
    protected function makeSignature(array $data)
    {
        try {
            return md5($this->token . ":" . $this->marker . $this->kSortAndGet($data));
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param array $searchData
     * @param $type
     * @return mixed
     */
    protected function fillSearchData(array $searchData, $type)
    {
        $requestParams = [];
        if ($type == "hotels") {
            $requestParams = $this->uriMap['hotels']['search']['query'];
        }

        foreach ($searchData as $key => $data) {
            if (isset($requestParams[$key])) {
                $requestParams[$key] = $data;
            }
        }

        return $requestParams;
    }

    /**
     * @param $query
     * @return mixed
     * @throws \Exception
     */
    private function callCURL($query)
    {
        $result = $this->initCurl($query);
        $content = json_decode($result['content'], true);
        if (isset($content['errorCode'])) {
            throw new \Exception($content['message'], (int)$content['errorCode']);
        }
        return $content;
    }

    private function callCityIdCURL($query)
    {
        $result = $this->initCurl($query);
        if (strpos($result['content'], "Bad") !== false) {
            throw new \Exception("Bad Request", 500);
        }
        if (isset($result['errorCode'])) {
            throw new \Exception($result['message'], (int)$result['errorCode']);
        }
        return $result;
    }

    /**
     * @param array $searchData
     * @return mixed
     * @throws \Exception
     */
    protected function getSearchID(array $searchData)
    {
        $this->uriMap['hotels']['search']['query'] = $this->fillSearchData($searchData, "hotels");
        $query = $this->queryBuilder($this->uriMap['hotels']['search']['url'], $this->uriMap['hotels']['search']['query']);
        $content = $this->callCURL($query);
        return $content['searchId'];
    }

    /**
     * @param array $queryParam
     * @return string
     * @desc build query string
     */
    protected function buildSearchQuery(array $queryParam)
    {
        $queryStr = parent::buildSearchQuery($queryParam);
        return $queryStr = '?' . $queryStr . "marker=" . $this->marker . "&signature=" . $this->makeSignature($queryParam);
    }

    /**
     * @param $query
     * @return array
     * @throws \Exception
     */
    protected function initCurl($query)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error) {
            throw new \Exception($error, 500);
        }
        return [
            'content' => $content,
            'response' => $response
        ];
    }
}