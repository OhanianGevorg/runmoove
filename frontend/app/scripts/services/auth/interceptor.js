'use strict';

angular.module('runMoovApp')
  .factory('auth/interceptor', function ($rootScope, $q, AuthEvents, $localStorage, ErrorEvents) {

    return {
      // optional method
      'request': function (config) {
        // do something on success
        config.headers['X-TOKEN'] = $localStorage.api_token;
        return config;
      },

      // optional method
      'requestError': function (rejection) {
        // do something on error
        //if (canRecover(rejection)) {
        //  return responseOrNewPromise
        //}
        return $q.reject(rejection);
      },

      // optional method
      'response': function (response) {
        return response;
      },

      // optional method
      'responseError': function (rejection) {
        console.log('Response Error', rejection);
        $rootScope.$broadcast({
          401: AuthEvents.notAuthenticated,
          403: AuthEvents.notAuthorized,
          404: ErrorEvents.notFound,
          419: AuthEvents.sessionTimeout,
          440: AuthEvents.sessionTimeout
        }[rejection.status], rejection);

        return $q.reject(rejection);
      }
    };
  }).config(function ($httpProvider) {
  $httpProvider.interceptors.push('auth/interceptor');
});
