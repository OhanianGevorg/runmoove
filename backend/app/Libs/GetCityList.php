<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/25/2017
 * Time: 12:26 AM
 */

namespace App\Libs;


use Symfony\Component\HttpFoundation\File\Exception\FileException;

class GetCityList
{
    private $filePath;

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return mixed|null
     */
    public function getCityListArray()
    {
        try{
            $list = file_get_contents($this->filePath);
            if(!$list){
                return null;
            }
            return json_decode($list,true);
        }catch (FileException $e){
            dd($e->getMessage().$e->getLine(),$e->getFile());
        }catch (\Exception $e){
            dd($e->getMessage().$e->getLine(),$e->getFile());
        }
    }
}