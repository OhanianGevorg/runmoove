<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TravelApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_api', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_date_id')->unsigned();
            $table->tinyInteger('type');
            $table->longText('body');
            $table->timestamps();
            $table->foreign('course_date_id')
                ->references('id')
                ->on('course_dates')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_api');
    }
}
