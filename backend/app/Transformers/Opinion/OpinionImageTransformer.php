<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/20/17
 * Time: 10:13 AM
 */

namespace App\Transformers\Opinion;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Opinion\OpinionImage;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use Illuminate\Support\Facades\Storage;

class OpinionImageTransformer implements TransformerInterface,PrepareInterface
{
    use TransformCollection,PrepareCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'url' => Storage::disk('public')->exists($model->img_url) ? asset(Storage::url($model->img_url)) : '',
            'course_opinion_id' => $model->course_id
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new OpinionImage([
            'course_opinion_id' => $injector['course_opinion_id'],
            'img_url' => array_get($raw, 'url')
        ]);
    }
}