<?php

namespace App\Models\Course;

use App\Models\System\SystemCourseFormat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseFormats extends Model
{
    //
    use SoftDeletes;

    protected $table = 'course_formats';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_id',
        'format_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function format()
    {
        return $this->belongsTo(SystemCourseFormat::class,'format_id');
    }
}
