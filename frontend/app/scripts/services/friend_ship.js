'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.friendShip
 * @description
 * # friendShip
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('friendShip', function (ENV, $resource) {

    return $resource(ENV.apiEndpoint + '/friend', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      store:{
        method:'POST',
        url:ENV.apiEndpoint + '/friend/request'
      },
      acceptFriend: {
        method: 'POST',
        url:ENV.apiEndpoint + '/friend/accept'
      },
      decline: {
        method: 'POST',
        url:ENV.apiEndpoint + '/friend/decline'
      }
    });
  });
