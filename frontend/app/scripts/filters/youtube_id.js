'use strict';

/**
 * @ngdoc filter
 * @name runMoovApp.filter:youtubeId
 * @function
 * @description
 * # youtubeId
 * Filter in the runMoovApp.
 */
angular.module('runMoovApp')
  .filter('youtubeId', function () {
    return function (text) {
      console.log(text);
      var video_id = text.split('v=')[1].split('&')[0];
      return video_id;
    }
  });
