// Here we attach this controller to our testApp module
angular.module('runMoovApp')

// The controller function let's us give our controller a name: MainCtrl
// We'll then pass an anonymous function to serve as the controller itself.
    .controller('MainCtrl', function ($scope, $rootScope, AuthService, $state, $timeout, ngDialog, moment, friendShip,PusherService,AuthEvents,$localStorage,Chat,Users) {

        //notification properties
        $rootScope.usersNotification = null;
        $rootScope.eventList = null;
        $rootScope.events = null;
        $rootScope.menu = {
          nt: false,
          message: false
        };

        $rootScope.AUTHUSER = AuthService;


        // Users.isAuth(function (res) {
        //   if (res.hasOwnProperty('error')) {
        //     $rootScope.logout();
        //   }
        // });


        if (!AuthService.isAuthenticated()) {
            $rootScope.openLogin = true;
            $state.go('auth');
        }
        if (AuthService.isAuthenticated()) {
            PusherService.initPusher(AuthService.userID(), AuthService.isAuthenticated(), $localStorage.api_token);
        }
        $rootScope.currentDate = moment().format('DD MMMM YYYY');





        $rootScope.formatDate = function (date) {
            if (date)
                return moment(date).format('DD MMMM YYYY');
        };

      $rootScope.formatDateWidthTime = function (date) {
        if (date)
          return moment(date).format('[Le] DD MMMM YYYY [à] hh[h]mm');
      };

        $rootScope.formatRangeDate = function (start, end) {
            if (start && end) {
                if (moment(start).format('MMMM') === moment(end).format('MMMM')) {
                    return 'Du ' + moment(start).format('DD') + ' au ' + moment(end).format('DD MMMM YYYY');
                } else {
                    return 'Du ' + moment(start).format('DD MMMM YYYY') + ' au ' + moment(end).format('DD MMMM YYYY');
                }
            }
        };


        /**
         * Call Logout function
         */
        $rootScope.logout = function () {
            // $rootScope.signOutTwitter();
            AuthService.logout();
        };


        /**
         * @desc function close all opened pop ups
         */
        $rootScope.closeAllPopups = function () {
            ngDialog.closeAll();
        };

        /**
         *
         * @param template (string)
         * @desc global pop up opening function
         */
        $rootScope.openPopup = function (template) {
            $rootScope.closeAllPopups();
            ngDialog.open({
                template: template,
                className: 'ngdialog-theme-default',
                width: '100%',
                overlay: false,
                showClose: true,
                closeByDocument: true
            });
        };


        //Global ngDialog Events
        $rootScope.$on('ngDialog.closing', function () {
            angular.element('#root').removeClass('blured');
        });

        $rootScope.$on('ngDialog.opened', function () {
            angular.element('#root').addClass('blured');
        });


        $rootScope.getRepeatNumber = function (num) {
            if (num === undefined && !num) {
                return;
            }
            return new Array(parseInt(num));
        };


        $rootScope.getRunTotalScore = function (run_scores) {
            if (!run_scores) {
                return;
            }
            var score = 0;
            for (n in run_scores) {
                score = score + parseInt(run_scores[n])
            }
            return Math.round(score / Object.keys(run_scores).length);
        };




        /*PUSHER*/
        $rootScope.$on(AuthEvents.loginSuccess, function () {
            if (AuthService.isAuthenticated()) {
                PusherService.initPusher(AuthService.userID(), AuthService.isAuthenticated(), $localStorage.api_token);
            }
        });

        $rootScope.toggleMenu = function (item) {
           $rootScope.menu[item] = !$rootScope.menu[item];
        };

      $rootScope.openMenu = function($mdMenu, ev) {
        originatorEv = ev;
        $mdMenu.open(ev);
      };


      $scope.msgData = {
        recipient_id:'',
        body:'',
      };

      $rootScope.openMsgPopup = function (user_id) {
        $scope.msgUserId = user_id;
        $rootScope.closeAllPopups();
        ngDialog.open({
          template: 'views/popups/chat.html',
          className: 'ngdialog-theme-default',
          width: '100%',
          overlay: false,
          showClose: true,
          scope:$scope,
          closeByDocument: true
        });
      };

      $rootScope.sendMessage = function (recipient_id) {


        if ($scope.msgData.body === '' ) {
          angular.element('.emojionearea').addClass('shake-error');
          setTimeout(function () {
            angular.element('.emojionearea').removeClass('shake-error');
          },800);
          return;
        }
        $scope.msgData.recipient_id = recipient_id;

        Chat.save($scope.msgData,function (res) {
          $rootScope.closeAllPopups();
          $scope.msgData = {
            recipient_id:'',
            body:'',
          };
          angular.element('.emojionearea-editor').html('');
        },function (err) {
          if (err.data.hasOwnProperty('error')) {
            toastr.error(err.data.error, 'Chat Error');
          }
        });
      };


      $rootScope.getMessageRoom = function (id) {
        Chat.getRoom({recipient_id:id},function (res) {
        });
      };


      $rootScope.msgTimeFilter = function (date) {

        var localDate = moment.utc(date).local();
        var hours = moment(moment()).diff(localDate, 'hours');

        localDate.add(hours, 'hours');

        var minutes = moment(moment()).diff(localDate, 'minutes');

        if (hours === 0) {
          return 'Il y à ' + minutes + ' minutes';
        } else if (hours >= 1 && hours <= 3) {
          return 'Il y à ' + minutes + ' minutes';
          // return 'Il y à ' + hours + ' heure ' + minutes + ' minutes';
        } else {
          return localDate.format('DD MMM YY  HH:MM');
        }
      };


      $rootScope.removeFromFriends = function (user_id) {
        Friends.delete({user_id:user_id},function (res) {

        })
      };


      $rootScope.getYouTubeThumb = function (video_id) {
        return 'https://img.youtube.com/vi/'+ video_id +'/mqdefault.jpg';
      };

      $rootScope.getUrlFromTag = function (str) {
       return angular.element(str).attr('href');
      };

        $rootScope.createCORSRequest = function(method, url) {
            var xhr = new XMLHttpRequest();
            if ("withCredentials" in xhr) {

                // Check if the XMLHttpRequest object has a "withCredentials" property.
                // "withCredentials" only exists on XMLHTTPRequest2 objects.
                xhr.open(method, url, true);

            } else if (typeof XDomainRequest != "undefined") {

                // Otherwise, check if XDomainRequest.
                // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
                xhr = new XDomainRequest();
                xhr.open(method, url);

            } else {

                // Otherwise, CORS is not supported by the browser.
                xhr = null;

            }
            return xhr;
        };


        $rootScope.getTravelHotelAvatar = function (id) {
          return 'https://photo.hotellook.com/image_v2/limit/h'+ id +'_1/600/400.jpg';
        };


        $rootScope.openNgDialog = function (template,data) {
          $rootScope.closeAllPopups();
          ngDialog.open({
            template: template,
            className: 'ngdialog-theme-default',
            width: '100%',
            overlay: false,
            showClose: true,
            closeByDocument: true,
            resolve:{
              data: function(){
                return data;
              }
            },
            controller: function($scope,data){
              $scope.hotel = data;
            }
          });
        };

    });
