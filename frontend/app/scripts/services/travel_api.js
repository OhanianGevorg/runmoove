'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.travelApi
 * @description
 * # travelApi
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('travelApi', function (ENV, $resource) {


    return $resource(ENV.apiEndpoint, {}, {
      call: {
        url: ENV.apiEndpoint + '/travel',
        method: 'GET',
      },
      get: {
        url: ENV.apiEndpoint + '/getTravelData',
        method: 'GET',
      },
      getAvia: {
        url: ENV.apiEndpoint + '/getAviaBooking',
        method: 'GET',
      },
    });
  });
