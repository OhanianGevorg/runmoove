<?php

namespace App\Console\Commands;

use App\Models\System\SystemCourseFormat;
use App\Models\System\SystemCourseImportance;
use App\Models\System\SystemPracticingType;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InsertSystemData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $course_formats = [
            '5-10km',
            'Semi-Marathon',
            'Marathon',
            'Trail court',
            'Trail Moyenne distance',
            'Trail Long Distance',
            'Ultra Trail'
        ];
        $course_importance = [
            'L\'Aambiance',
            'Le parcours',
            'La solidarite',
            'La proximite',
            'Le prestige',
            'L\'organisation',
            'La dotation'
        ];
        $practicing_type = [
            'Route',
            'Trail',
            'Triathlon',
            'Marche sportive'
        ];

        DB::beginTransaction();
        try {
            SystemCourseFormat::insert(array_map(function ($format) {
                return [
                    'name' => $format,
                    'created_at' => Carbon::now()
                ];
            }, $course_formats));
            SystemCourseImportance::insert(array_map(function ($importance) {
                return [
                    'name' => $importance,
                    'created_at' => Carbon::now()
                ];
            }, $course_importance));
            SystemPracticingType::insert(array_map(function ($type) {
                return [
                    'name' => $type,
                    'created_at' => Carbon::now()
                ];
            }, $practicing_type));
            DB::commit();
            $this->info('successfully created!');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

}
