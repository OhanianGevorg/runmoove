<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/16/2017
 * Time: 2:46 PM
 */

namespace App\Services\LevelCount;

class LevelCountKernel
{
    private static $levelUpClasses = [
        \App\Models\Opinion\Opinion::class =>1
    ];

    public static function getLevelUpClasses()
    {
        return static::$levelUpClasses;
    }
}