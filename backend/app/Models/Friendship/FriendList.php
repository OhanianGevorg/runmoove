<?php

namespace App\Models\Friendship;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FriendList extends Model
{
    use SoftDeletes;
    //
    const IS_BLOCKED = 1;
    const IS_NOT_BLOCKED = 0;

    const EVENT_NAME = 'addFriend';

    protected $table = 'friendlist';

    protected $primaryKey = 'id';

    protected $fillable = [
        'recipient_id',
        'sender_id',
        'is_blocked',
        'blocker_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    /**
     * @return bool
     */
    public function isBlocked()
    {
        return (int)$this->is_blocked === self::IS_BLOCKED;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blocker()
    {
        return $this->belongsTo(User::class, 'blocker_id');
    }

    /**
     * @param $query
     * @param array $attributes
     * @return mixed
     */
    public function scopeGetOnlyFields($query, $attributes = [])
    {
        if (!count($attributes)) {
            return $query->select($this->table . '.*');
        }
        return $query->select($this->table . '.' . implode(',', $attributes));
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeGetNotBlocked($query)
    {
        return $query->where('is_blocked', '=', self::IS_NOT_BLOCKED);
    }

    public static function friendNotRecordExists($model)
    {
        return is_null(static::where('recipient_id', '=', $model->recipient_id)
            ->where('sender_id', '=', $model->sender_id)->first()) && is_null(static::where('recipient_id', '=', $model->sender_id)
            ->where('sender_id', '=', $model->recipient_id)->first());
    }
}
