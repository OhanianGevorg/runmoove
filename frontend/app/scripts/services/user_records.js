'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.userRecords
 * @description
 * # userRecords
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('userRecords', function (ENV, $resource) {


    return $resource(ENV.apiEndpoint + '/records/:id', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      save:{
        method:'POST'
      },
    });

  });
