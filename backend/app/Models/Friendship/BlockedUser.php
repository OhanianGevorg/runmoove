<?php

namespace App\Models\Friendship;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BlockedUser extends Model
{
    protected $table = 'blocked_users';

    protected $primaryKey = 'id';

    protected $fillable = [
        'blocker_id',
        'blocked_id',

    ];

    public function blocker()
    {
        return $this->belongsTo(User::class, 'blocker_id');
    }

    public function blocked()
    {
        return $this->belongsTo(User::class, 'blocker_id');
    }
}
