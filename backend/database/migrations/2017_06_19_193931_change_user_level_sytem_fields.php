<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserLevelSytemFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('system_levels','img_name')&&Schema::hasColumn('system_levels','below_level')&&Schema::hasColumn('system_levels','top_level')){
            Schema::table('system_levels', function (Blueprint $table) {
                $table->dropColumn('img_name');
                $table->dropColumn('below_level');
                $table->dropColumn('top_level');
            });
        }
        if(!Schema::hasColumn('system_levels','real_name')&&!Schema::hasColumn('system_levels','img_name')&&!Schema::hasColumn('system_levels','up_score')&&!Schema::hasColumn('system_levels','down_score')){
            Schema::table('system_levels',function (Blueprint $table){
                $table->string('real_name');
                $table->string('img_name')->nullable();
                $table->integer('up_score')->nullable();
                $table->integer('down_score');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
