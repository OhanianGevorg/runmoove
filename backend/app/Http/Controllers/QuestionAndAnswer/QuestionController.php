<?php

namespace App\Http\Controllers\QuestionAndAnswer;

use App\Http\Requests\QuestionAndAnswer\QuestionListRequest;
use App\Http\Requests\QuestionAndAnswer\QuestionReadRequest;
use App\Http\Requests\QuestionAndAnswer\QuestionStoreRequest;
use App\Models\Course\CourseDate;
use App\Models\QuestionAndAnswer\Question;
use App\Transformers\QuestionAndAnswer\QuestionTransformer;
use App\User;
use Doctrine\Common\Collections\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * @desc not auth method
     * @param QuestionListRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function questionList(QuestionListRequest $request)
    {
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'Course date is required'
            ], 422);
        }
        /**
         * @var $course_date CourseDate
         * @var $questions Collection
         */
        try {
            $course_date = CourseDate::findOrFail($request->get('course_date_id'));
            $questions = $course_date->questions()->getAll($request->all());
            return response()->json([
                'data' => QuestionTransformer::transformCollection($questions)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param QuestionReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(QuestionReadRequest $request)
    {
        if (!User::auth() || !User::auth()->is_orginizer()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        try {
            return response()->json([
                'data' => QuestionTransformer::transformCollection(User::auth()->getQuestionsQuery()->getAll($request->all()))
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param QuestionStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        try {
            CourseDate::findOrFail($request->get('course_date_id'));
            $question = QuestionTransformer::prepare($request->all(), ['user_id' => User::auth()->getKey()]);
            $question->save();
            return response()->json([
                'success' => 'Your question has posted',
                'data' => QuestionTransformer::transform($question)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @param QuestionStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, QuestionStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        try {
            /**
             * @var $question Question
             */
            $question = Question::where('id', '=', $id)
                ->where('user_id', '=', User::auth()->getKey())
                ->firstOrFail();
            $question->update(QuestionTransformer::prepareUpdate($request->all()));
            return response()->json([
                'success' => 'Data has successfully updated',
                'data' => QuestionTransformer::transform($question)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $question Question
         */
        try {
            $question = Question::where('id', '=', $id)
                ->where('user_id', '=', User::auth()->getKey())
                ->firstOrFail();
            DB::beginTransaction();
            $question->answer()->delete();
            $question->delete();
            DB::commit();
            return response()->json([
                'success' => 'Data has successfully deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
