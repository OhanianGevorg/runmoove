<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/18/2017
 * Time: 12:10 AM
 */

namespace App\Transformers\Chat;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Chat\Participant;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;

class ParticipantsTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Participant([
            'room_id' => array_get($raw, 'room_id'),
            'user_id' => array_get($raw, 'user_id')
        ]);
    }

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'room_id'=>$model->room_id,
            'user_id' => (User::auth() && User::auth()->getKey() === $model->user_id) ? $model->user_id : UserForFriendsTransformer::transform($model->user)
        ];
    }
}