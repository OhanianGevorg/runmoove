'use strict';

/**
 * @ngdoc service
 * @name frontendApp.Runs
 * @description
 * # Runs
 * Service in the frontendApp.
 */
angular.module('runMoovApp')
    .service('Runs', function (ENV, $resource) {
        return $resource(ENV.apiEndpoint + '/course', {}, {
            query: {
                method: 'GET',
                isArray: false
            },
            update: {
                method: 'PUT'
            },
            edit: {
                url: ENV.apiEndpoint + '/course/:id/edit',
                method: 'GET'
            },
            save: {
                method: 'POST'
            },
            show: {
                method: 'GET',
                url: ENV.apiEndpoint + '/course/:id'
            },
            userRuns: {
                method: 'GET',
                url: ENV.apiEndpoint + '/participations'
            },
            travelAPI: {
                method: 'GET',
                url: ENV.apiEndpoint + '/travel'
            },
            avia: {
                method: "POST",
                url: ENV.apiEndpoint + '/avia'
            }
        });
    });
