<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCourseMarkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_markers', function (Blueprint $table) {
            //
            $table->dropColumn('marker_type');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
        Schema::table('course_markers', function (Blueprint $table) {
            $table->string('marker_type')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->mediumText('coords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_markers', function (Blueprint $table) {
            //
            $table->dropColumn('coords');
        });
    }
}
