<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 5/29/2017
 * Time: 11:24 PM
 */

namespace App\Transformers\Shared;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class StaticTransformer implements TransformerInterface
{
    use TransformCollection;
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'name' => $model->name
        ];
    }
}