<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/11/2017
 * Time: 12:02 AM
 */

namespace App\Transformers\UserOtherTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\User;
use Illuminate\Support\Facades\Storage;

class UserForFriendsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        /**
         * @var $model User
         */
        if (!$model) {
            return [];
        }
        $user_array = [
            'id' => $model->getKey(),
            'name' => $model->first_name . " " . $model->last_name,
            'avatar' => (!filter_var($model->avatar, FILTER_VALIDATE_URL) && Storage::disk('public')->exists($model->avatar)) ? asset(Storage::url($model->avatar)) : $model->avatar
        ];
        if (User::auth()) {
            $user_array['is_blocked'] = User::auth()->is_blocked($model->getKey());
            $user_array['is_friend'] = User::auth()->hasFriend($model->getKey());
            $user['own_block'] = User::auth()->is_blocker($model->getKey());
        }
        return $user_array;
    }
}