<?php

namespace App\Http\Controllers\QuestionAndAnswer;

use App\Http\Requests\QuestionAndAnswer\AnswerStoreRequest;
use App\Models\QuestionAndAnswer\Answer;
use App\Models\QuestionAndAnswer\Question;
use App\Transformers\QuestionAndAnswer\AnswerTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnswerController extends Controller
{
    /**
     * @param AnswerStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AnswerStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $question Question
         * @var  $answer Question
         */
        try {
            $question = Question::findOrFail($request->get('question_id'));
            if (!User::auth()->canAnswer($question)) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 403);
            }
            $answer = AnswerTransformer::prepare($request->all(), ['user_id' => User::auth()->getKey()]);
            $answer->save();
            return response()->json([
                'success' => 'Answer has successfully posted',
                'data' => AnswerTransformer::transform($answer)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $answer Answer
         */
        try {
            $answer = Answer::findOrFail($id);
            if (!User::auth()->canAnswer($answer->question)) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 403);
            }
            return response()->json([
                'data' => AnswerTransformer::transform($answer)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @param AnswerStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, AnswerStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $question Question
         * @var $answer Answer
         */
        try {
            $question = Question::findOrFail($request->get('question_id'));
            if (!User::auth()->canAnswer($question)) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 403);
            }
            $answer = Answer::where('id', '=', $id)
                ->where('user_id', '=', User::auth()->getKey())
                ->firstOrFail();
            $answer->update(AnswerTransformer::update($request->all()));
            return response()->json([
                'success' => 'Data has successfully updated',
                'data' => AnswerTransformer::transform($answer)
            ], 200);

        } catch (ModelNotFoundException $e) {

            return response()->json([
                'error' => 'Data has not found'
            ], 404);
        } catch (\Exception $e) {

            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $answer Answer
         */
        try {
            $answer = Answer::where('id', '=', $id)
                ->where('user_id', '=', User::auth()->getKey())->firstOrFail();
            if (!User::auth()->canAnswer($answer->question)) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 403);
            }
            $answer->delete();
            return response()->json([
                'success' => 'Data has successfully deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Model not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
