'use strict';

describe('Filter: filters/youtubeId', function () {

  // load the filter's module
  beforeEach(module('runMoovApp'));

  // initialize a new instance of the filter before each test
  var filters/youtubeId;
  beforeEach(inject(function ($filter) {
    filters/youtubeId = $filter('filters/youtubeId');
  }));

  it('should return the input prefixed with "filters/youtubeId filter:"', function () {
    var text = 'angularjs';
    expect(filters/youtubeId(text)).toBe('filters/youtubeId filter: ' + text);
  });

});
