'use strict';

describe('Service: friendShip', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var friendShip;
  beforeEach(inject(function (_friendShip_) {
    friendShip = _friendShip_;
  }));

  it('should do something', function () {
    expect(!!friendShip).toBe(true);
  });

});
