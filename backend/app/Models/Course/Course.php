<?php

namespace App\Models\Course;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

    use SoftDeletes,FilterTrait;

    protected $table = 'courses';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'latitude',
        'longitude',
        'distance',
        'registration_fee',
        'participants_number',
        'name',
        'location',
        'organizer_telephone',
        'organizer_email',
        'site',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_dates()
    {
        return $this->hasMany(CourseDate::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_formats()
    {
        return $this->hasMany(CourseFormats::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_types()
    {
        return $this->hasMany(CourseTypes::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_images()
    {
        return $this->hasMany(CourseImages::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_markers()
    {
        return $this->hasMany(CourseMarkers::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @desc get course formats
     * @return mixed
     */
    public function get_course_formats()
    {
        return $this->course_formats()->getResults()->map(function (CourseFormats $relation){
           return $relation->format;
        });
    }

    /**
     * @desc get course types
     * @return mixed
     */
    public function get_course_types()
    {
        return $this->course_types()->getResults()->map(function (CourseTypes $relation){
           return $relation->type;
        });
    }
}
