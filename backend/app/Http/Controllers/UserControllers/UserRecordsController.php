<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Requests\UserStoreRequests\UserDeleteRequest;
use App\Http\Requests\UserStoreRequests\UserRecordsStoreRequest;
use App\Models\Course\CourseDate;
use App\Models\UserRelated\UserRecord;
use App\Transformers\Shared\UserRecordTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRecordsController extends Controller
{
    //
    public function index(Request $request)
    {
        if (!$request->get('user_id')) {
            return response()->json([
                'error' => 'User is required'
            ], 422);
        }
        /**
         * @var $user User
         */
        $user = User::find($request->get('user_id'));
        if (!$user) {
            return response()->json([
                'error' => 'User has not found'
            ], 404);
        }
        return response()->json([
            'records' => UserRecordTransformer::transformCollection($user->records)
        ]);
    }

    public function store(UserRecordsStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'course_date_id is required'
            ], 422);
        }

        try {
            $cours_date = CourseDate::findOrFail($request->get('course_date_id'));
            $user_record = UserRecordTransformer::prepare($request->all(), ['user_id' => User::auth()->getKey()]);
            $user_record->save();
            return response()->json([
                'success' => UserRecordTransformer::transform($user_record)
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function destroy($id, UserDeleteRequest $request)
    {
        /**
         * @var $user_record UserRecord
         */
        if (is_null(User::auth())) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        $user_record = User::auth()->records()->where('id', $id)->first();
        if (!$user_record) {
            return response()->json([
                'error' => 'Record not found'
            ], 404);
        }

        $user_record->delete();
        return response()->json([
            'success' => 'Record has successfully deleted'
        ], 200);

    }
}
