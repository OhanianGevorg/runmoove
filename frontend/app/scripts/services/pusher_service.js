'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.pusherService
 * @description
 * # pusherService
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
    .service('PusherService', function ($rootScope, AuthService, Notifications, $pusher, ENV) {

        $rootScope.getActiveNotification = [];
        $rootScope.getActiveMessages = [];
        this.data = null;
        var isNotConnected = true;
        var _self = this;
        var pusherAPI = null;
        var channelName = '';
        var chatChannel = '';
        var client = function (apiToken) {
            return new Pusher(ENV.pusher_key, {
                // encrypted: true,
                cluster: 'us2',
                // authEndpoint: ENV.apiEndpoint + '/broadcasting/auth',
                authEndpoint: ENV.apiEndpoint + '/pusher/auth',
                auth: {
                    headers: {
                        'X-TOKEN': apiToken
                        // 'X-Socket-ID':null
                    }
                }
            });
        };


        this.pusher = function (apiToken) {
            var push = client(apiToken);
            // push.config.auth.headers['X-Socket-ID'] =push.connection.socket_id;
            return $pusher(push);
        };

        this.connect = function (id, notificationsList) {
            try {
                channelName = 'private-notifications.' + id;
                var connect = pusherAPI.subscribe(channelName);
                var events;
                connect.bind('pusher:subscription_succeeded', function () {
                    /**
                     * @desc this is chat connection
                     */

                    chatChannel = 'private-private-chat.' + id;
                    var chatConnect = pusherAPI.subscribe(chatChannel);
                    chatConnect.bind('pusher:subscription_succeeded', function () {
                        chatConnect.bind('message.sent', function (response) {
                          console.log(response);
                          var audioMsg = new Audio('sound/message.mp3');
                          audioMsg.play();
                          $rootScope.getActiveMessages.unshift(response.event);
                            $rootScope.messages.push(response.mail);
                            $('.message-list-box[rm-scroll]').mCustomScrollbar("update");
                            $('.message-list-box[rm-scroll]').mCustomScrollbar("scrollTo", ".message:last-child");
                            $rootScope.unreadMessageCount = response.message.length;
                        });
                    });
                    chatConnect.bind('pusher:subscription_error', function (status) {
                        console.log(status);
                    });
                    angular.forEach(notificationsList, function (notification) {
                        console.log(notification);
                        connect.bind(notification, function (response) {
                          var audioMsg = new Audio('sound/message.mp3');
                          audioMsg.play();
                            events = response.event;
                            _self.data = {
                                id: events.id,
                                status: 'yes'
                            };

                              if (events.event_name === 'message.sent') {
                                $rootScope.getActiveMessages.unshift(events);
                              } else {
                                $rootScope.getActiveNotification.unshift(events);
                              }


                        });
                    });
                });

                connect.bind('pusher:subscription_error', function (status) {
                    console.log(status);
                });

            } catch (err) {
                console.log(err);
            }
            Pusher.log = function (message) {
                if (window.console && window.console.log) {
                    window.console.log(message);
                }
            };
        };

        this.initPusher = function (id, isAuthenticated, token) {

            if (!id || !isAuthenticated || !token) {
                console.log('not auth');
                return false;
            }
            if (isNotConnected) {
                pusherAPI = this.pusher(token);
                // pusherAPI.client.config.auth.headers['X-Socket-ID'] = pusherAPI.client.connection.socket_id;
                // console.log(pusherAPI.client.connection,pusherAPI);
                Notifications.get(function (response) {
                    if (response.hasOwnProperty('error') || !response.hasOwnProperty('events')) {
                        console.log('error', response);
                        return false;
                    }
                    _self.connect(id, response['events']);
                });
            }
            isNotConnected = false;
        };

        this.disconnect = function () {
            isNotConnected = true;
            pusherAPI.unsubscribe(channelName);
            pusherAPI.unsubscribe(chatChannel);
            pusherAPI = null;
        }
    });
