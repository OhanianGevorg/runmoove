<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/27/2017
 * Time: 12:43 AM
 */

namespace App\Transformers\Shared;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\UserRelated\UserRecord;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class UserRecordTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new UserRecord([
            'course_date_id' => array_get($raw, 'course_date_id'),
            'user_id' => array_get($injector, 'user_id'),
            'time' => array_get($raw, 'time')
        ]);
    }

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id'=>$model->getKey(),
            'user_id' => $model->user_id,
            'course_date_id' => ($model->course_date)?$model
                ->course_date
                ->get_course_field('name'):$model->course_date_id,
            'time' => $model->time
        ];
    }
}