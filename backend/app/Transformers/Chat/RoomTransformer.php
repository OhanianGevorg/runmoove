<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/18/2017
 * Time: 12:06 AM
 */

namespace App\Transformers\Chat;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Chat\Room;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class RoomTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Room([
            'creator_id' => array_get($raw, 'user_id')
        ]);
    }

    public static function transform($model)
    {
        /**
         * $model Room
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'participants' => ParticipantsTransformer::transformCollection($model->participants)
        ];
    }
}