<?php

namespace App\Models\Chat;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{

    use SoftDeletes;

    protected $table = 'participants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'room_id',
        'user_id'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class,'room_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
