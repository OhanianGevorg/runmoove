'use strict';

describe('Controller: LayoutFooterCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var LayoutFooterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LayoutFooterCtrl = $controller('LayoutFooterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LayoutFooterCtrl.awesomeThings.length).toBe(3);
  });
});
