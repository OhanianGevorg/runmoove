'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:centerCrop
 * @description
 * # centerCrop
 */
angular.module('runMoovApp')
  .directive('centerCrop', function () {
    return {
      scope: {
        ratioWidth:'=',
        ratioHeight:'=',
      },
      restrict: 'AEC',
      link: function link(scope, element, attrs) {

        scope.ratioWidth = scope.ratioWidth !== undefined ? scope.ratioWidth : 1;
        scope.ratioHeight = scope.ratioHeight  !== undefined ? scope.ratioHeight : 1;
        var ratioValue = scope.ratioWidth / scope.ratioHeight;
        var img = $(element).find('img');

        $(element).css({
          'display':'flex',
          'justify-content':'center',
          'align-items':'center',
          'overflow':'hidden',
        });




        function  generateSize() {
          $(element).height($(element).width() / ratioValue);
          img.each(function (index,item) {
            $(this).load();
            $(item).on("load",function () {
              if (this.width > this.height && this.width / this.height > ratioValue) {
                img.css({'height':'100%','width':'auto'});
              } else {
                img.css({'width':'100%','height':'auto'});
              }

            });
          });
        };

        generateSize();

        $(window).resize(function () {
          generateSize();
        });






      }
    };
  });
