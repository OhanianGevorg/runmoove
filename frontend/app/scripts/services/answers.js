/**
 * @ngdoc service
 * @name runMoovApp.articles
 * @description
 * # articles
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('Answers', function (ENV,$resource) {
    return $resource(ENV.apiEndpoint + '/answer', {}, {
      get: {
        method: 'GET',
      },
      query: {
        method: 'GET',
        isArray: false
      },
      delete: {
        method: 'DELETE',
      },
      list: {
        url: ENV.apiEndpoint + '/answer/list',
        method: 'GET',
      },

    });
  });

