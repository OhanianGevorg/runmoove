'use strict';

describe('Service: Runs', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var Runs;
  beforeEach(inject(function (_Runs_) {
    Runs = _Runs_;
  }));

  it('should do something', function () {
    expect(!!Runs).toBe(true);
  });

});
