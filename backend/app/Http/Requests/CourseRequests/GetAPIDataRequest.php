<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class GetAPIDataRequest extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_date_id'=>'required|integer|exists:course_dates,id'
        ];
    }
}
