<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleImage extends Model
{
    //
    use SoftDeletes;
    protected $table = 'article_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_name',
        'article_id'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class,'article_id');
    }
}
