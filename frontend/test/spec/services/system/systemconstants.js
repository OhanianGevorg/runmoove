'use strict';

describe('Service: system/systemConstants', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var system/systemConstants;
  beforeEach(inject(function (_system/systemConstants_) {
    system/systemConstants = _system/systemConstants_;
  }));

  it('should do something', function () {
    expect(!!system/systemConstants).toBe(true);
  });

});
