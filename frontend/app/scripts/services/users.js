'use strict';

angular.module('runMoovApp')
  .factory('Users', function ($resource, ENV) {

    return $resource(ENV.apiEndpoint + '/user/:id', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      show:{
        method:'GET',
        url:ENV.apiEndpoint + '/user/:id/'
      },
      update: {
        method: 'PUT',
      },
      edit:{
        url:ENV.apiEndpoint + '/user/:id/edit',
        method:'GET'
      },
      save:{
        url:ENV.apiEndpoint + '/user/save',
        method:'POST'
      },
      saveAdditional:{
        url:ENV.apiEndpoint + '/user/additional',
        method:'POST'
      },
      changeAvatar:{
        url:ENV.apiEndpoint + '/user/changeAvatar',
        method:'POST',
      },
      changePassword:{
        url:ENV.apiEndpoint + '/user/changePassword',
        method:'POST',
      },
    isAuth:{
      url:ENV.apiEndpoint + '/user/isauth',
        method:'GET'
    },
    });
  });
