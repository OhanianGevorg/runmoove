<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/1/2017
 * Time: 1:00 AM
 */

namespace App\Transformers\Article;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Article\ArticleImage;
use App\Models\Article\ArticleVideo;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use Illuminate\Support\Facades\Storage;

class ArticleImageTransformer implements TransformerInterface, PrepareInterface
{

    use TransformCollection, PrepareCollection;

    public static function transform($model)
    {
        /**
         * @var $model ArticleImage
         */
        if (!$model) {
            return [

            ];
        }

        return [
            'id' => $model->getKey(),
            'file_name' => asset(Storage::url($model->file_name)),
            'article_id' => $model->article_id
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new ArticleImage([
            'file_name' => array_get($raw, 'file_name'),
            'article_id' => array_get($injector, 'article_id')
        ]);
    }
}