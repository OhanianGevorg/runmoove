'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesUserFriendsCtrl
 * @description
 * # PagesUserFriendsCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('FriendsCtrl', function (Friends,$scope) {

    $scope.data = {
      name:'',
      longitude:'',
      latitude:'',
    };

    $scope.geo = {
      location: []
    };

    $scope.searchUsers = function () {


      if (Object.keys($scope.geo.location).length > 0) {
        $scope.data.longitude = $scope.geo.location.geometry.location.lng();
        $scope.data.latitude = $scope.geo.location.geometry.location.lat();
      } else if ($scope.geo.location = []) {
        $scope.data.longitude = '';
        $scope.data.latitude = '';
      }

      Friends.query($scope.data,function (res) {
        $scope.friends = res.friends;

        $scope.removeFriend = function (id,index) {
          Friends.delete({user_id: id}, function (res) {
            if (res.hasOwnProperty('success')) {
              $scope.friends.splice(index,1);
            }
          });
        };

      });

     };

    $scope.searchFriends = function () {
      Friends.friendlist(function (res) {
          $scope.friends = res.friends;
      });
    };
    $scope.searchFriends();

  });
