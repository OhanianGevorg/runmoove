<?php

namespace App\Services;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TransformerMapper
{
    /**
     * @var array
     * @desc describe model and associated transformer
     * @desc key is model base name, value is transformer class full name
     */
    private static $transformerMap = [

        'FriendRequest' => \App\Transformers\Friendship\FriendRequestTransformer::class,
        'FriendList' => \App\Transformers\Friendship\FriendListTransformer::class
    ];

    /**
     * @param $className
     * @return mixed
     * @desc get value from map
     */
    public static function getTransformer($className)
    {
        return array_get(static::$transformerMap, $className, null);
    }

    /**
     * @return array
     * @desc get all data from map
     */
    public static function getAllTransformers()
    {
        return static::$transformerMap;
    }

    /**
     * @param $model
     * @return array
     * @desc get data from map and call related formatted data or model fillable array
     */
    public static function mapTransformer($model)
    {
        /**
         * @var $model Model
         */
        $transformer = static::getTransformer(class_basename($model));
        if ($transformer) {
            return ($model instanceof Collection) ? $transformer::transformCollection($model) : $transformer::transform($model);
        }

        return $model->getFillable();
    }
}