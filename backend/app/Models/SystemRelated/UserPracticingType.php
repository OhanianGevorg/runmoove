<?php

namespace App\Models\SystemRelated;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPracticingType extends Model
{
    //
    use SoftDeletes;

    protected $table = 'user_practicing_type';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'type_id'
    ];
}
