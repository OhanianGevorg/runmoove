'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:courseMapDirective
 * @description
 * # courseMapDirective
 */
angular.module('runMoovApp')
    .directive('courseMapDirective', function ($http, ENV, $localStorage) {
        return {
            restrict: 'A',
            scope: {
                geoData: "=geodata",
                isEditable: "@edit",
                start: "=start",
                data: "=data",
                toggleBtn: "=",
                gpxUrl: "=gpxUrl"
            },
            link: function (scope, element, attrs) {

                var elementID = attrs.id;
                var drawnItems = null;
                var control = null;
                if (!elementID || typeof elementID !== 'string') {
                    return false;
                }
                var map = null;
                var localGeodata = null;
                var drawnGpx = null;
                var gpxLayer = null;
                var startCoords = [];
                var uploadUrl = ENV.apiEndpoint + '/gpx/upload';
                //style object
                var style = {
                    color: '#4392ff',
                    opacity: 0.9
                };

                var Icon = L.icon({
                    iconUrl: 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png',
                    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',

                    iconSize: [30, 45], // size of the icon
                    shadowSize: [50, 64], // size of the shadow
                    iconAnchor: [13, 45], // point of the icon which will correspond to marker's location
                    shadowAnchor: [4, 62],  // the same for the shadow
                    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                    // map object
                    // var map = L.map(elementID, {drawControl: true}).setView([33.81, -84.4], 17);
                })

                if (scope.isEditable == 'true') {
                    $('#' + scope.toggleBtn).click(function (e) {
                        $('#gpxFileUpload').click();
                    });

                    $(element).append(' <input type="file" class="hidden"  id="gpxFileUpload" accept=".gpx">');
                }


                scope.startMap = scope.start;
                scope.startMap.init = function (geodata, coords) {
                    localGeodata = geodata;

                    if (!geodata.hasOwnProperty('latitude') || !geodata.hasOwnProperty('longitude')) {
                        startCoords = [
                            0, 0
                        ];
                    } else {
                        startCoords.push(localGeodata.latitude);
                        startCoords.push(localGeodata.longitude);
                    }

                    map = new L.map(elementID, {
                        center: startCoords,
                        zoom: 15
                    });

                    if (coords) {
                        var hikeLayer = buildTrail(coords, style);

                        hikeLayer.addTo(map);
                    }

                    draw(scope.isEditable);

                    setTimeout(function () {
                        if (scope.gpxUrl) {
                            var gpxFullUrl = ENV.apiEndpoint + '/gpx/' + scope.gpxUrl;
                            rendGpx(gpxFullUrl);
                        }
                    }, 500);


                    //events

                    map.on('draw:created', function (e) {
                        var type = e.layerType,
                            layer = e.layer;
                        if (drawnItems) {
                            drawnItems.addLayer(layer);
                        }
                    });

                    // map.on('click', putMarker);
                    // function putMarker(e) {
                    //   L.marker(e.latlng, { icon: Icon }).addTo(map);
                    // }

                    map.on('draw:edited', function (e) {
                        scope.data.course_coords = JSON.stringify(drawnItems.toGeoJSON());
                    });

                    map.on('draw:drawstop', function (e) {
                        scope.data.course_coords = JSON.stringify(drawnItems.toGeoJSON());
                    });
                    //end events
                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
                };

                /**
                 * @desc initialize map
                 */
                scope.startMap.clearMap = function () {
                    localGeodata = [];
                    startCoords = [];
                    map.remove();
                    $(element).find('#gpxFileUpload').val('');
                };


                $(element).find('#gpxFileUpload').bind("change", function (changeEvent) {
                    scope.$apply(function () {
                        readURL(changeEvent.target);
                    });
                });


                function readURL(input) {

                    var file = input.files[0];

                    if (input.files && file) {

                        var fd = new FormData();
                        fd.append('gpx', file);
                        $http.post(uploadUrl, fd, {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined, 'X-TOKEN': $localStorage.api_token}
                        })
                            .then(function (res) {
                                scope.data.course_coords_url = res.data.gpx;
                                scope.data.course_coords = '';
                            }, function (err) {

                            });


                        var reader = new FileReader();

                        reader.onload = function (event) {
                            rendGpx(event.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                /**
                 * @desc draw map
                 * @returns {boolean}
                 */
                function draw(isEditable) {
                    if (!map) {
                        return false;
                    }
                    // if user can edit map
                    if (isEditable === 'true') {
                        drawnItems = new L.FeatureGroup();
                        map.addLayer(drawnItems);
                        var marker = Marker('https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png');

                        control = drawControl(drawnItems, marker);
                        map.addControl(control);

                    }
                }


                /**
                 * render GPX file obj or file url
                 * @param gpx_file
                 */
                function rendGpx(gpx_file) {

                    gpxLayer = new L.GPX(gpx_file,
                        {
                            async: true,
                            marker_options: {
                                wptIconUrls: {
                                    '': 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png',
                                    'Geocache Found': 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png',
                                    'Park': 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png'
                                },
                                startIconUrl: 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png',
                                endIconUrl: 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png',
                                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png'
                            },
                        });
                    gpxLayer.on('loaded', function (e) {
                        map.fitBounds(e.target.getBounds());
                    }).addTo(map);
                }

                /**
                 * @desc build trail
                 * @param coords
                 * @param stylesObject
                 * @returns {boolean}
                 */
                function buildTrail(coords, stylesObject) {
                    if (!coords) {
                        return false;
                    }
                    // console.log(coords);
                    var trail = JSON.parse(coords);
                    return L.geoJson(trail, {
                        style: stylesObject,
                        pointToLayer: function (feature, latlng) {

                            return L.marker(latlng, {icon: Icon});
                        },
                    });
                }

                /**
                 * @desc return custom marker class
                 * @param iconUrl
                 * @returns {*}
                 */
                function Marker(iconUrl) {
                    return L.Icon.extend({
                        options: {
                            shadowUrl: null,
                            iconSize: new L.Point(24, 24),
                            iconAnchor: new L.Point(24, 24),
                            iconUrl: iconUrl
                        }
                    });
                }

                var customMarker = L.Icon.extend({
                    options: {
                        shadowUrl: null,
                        iconAnchor: new L.Point(12, 12),
                        iconSize: new L.Point(30, 45),
                        iconUrl: 'https://fysio.no/bundles/nullsjuinteraktivfysio/images/google-map/marker-map.png'
                    }
                });

                /**
                 * @desc set draw control options
                 * @param drawnItems
                 * @param marker
                 * @returns {*}
                 */
                function drawControl(drawnItems, marker) {
                    console.log(marker);
                    if (!drawnItems) {
                        return false;
                    }
                    var isMarker = (function () {
                        return (marker !== undefined || marker !== null) && marker instanceof Object;
                    })();

                    return new L.Control.Draw({
                        draw: {
                            position: 'topright',

                            polygon: false,
                            marker: {
                                icon: (isMarker) ? new customMarker() : false
                            },
                            rectangle: false,
                            circle: false,
                            polyline: {
                                metric: false,
                                shapeOptions: {
                                    opacity: 1,
                                    color: '#f07300',
                                    fillColor: '#f07300'
                                }
                            }
                        },
                        edit: {
                            featureGroup: drawnItems
                        }
                    });
                }

                function drawOptions() {
                    L.DrawToolbar.include({
                        getModeHandlers: function (map) {
                            return [
                                {
                                    enabled: true,
                                    handler: new L.Draw.MarkerA(map, { icon: new L.Icon.Default() }),
                                    title: 'Place restaurant marker'
                                },
                                {
                                    enabled: true,
                                    handler: new L.Draw.Marker(map, { icon: new L.Icon.Default() }),
                                    title: 'Place gas station marker'
                                },
                                {
                                    enabled: true,
                                    handler: new L.Draw.Marker(map, { icon: new L.Icon.Default() }),
                                    title: 'Place hospital marker'
                                }
                            ];
                        }
                    });
                }
            }
        };

    });

