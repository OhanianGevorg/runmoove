<?php

namespace App\Http\Controllers\CourseControllers;

use App\Http\Requests\CourseRequests\CourseParticipationDeleteRequest;
use App\Http\Requests\CourseRequests\CourseParticipationStoreRequest;
use App\Http\Requests\UserStoreRequests\UserUpdateRequest;
use App\Models\Course\CourseDate;
use App\Models\Course\CourseParticipation;
use App\Transformers\CourseTransformers\CourseParticipationTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseParticipationController extends Controller
{

    public function index()
    {

    }

    /**
     * @param CourseParticipationStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseParticipationStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'Course has not found'
            ], 404);
        }
        /**
         * @var $course_date CourseDate
         */
        try {
            $course_date = CourseDate::findOrFail($request->get('course_date_id'));
            if ($course_date->checkParticipation(User::auth())) {
                return response()->json([
                    'error' => 'You have already participated this course'
                ], 403);
            }
            $data['course_date_id'] = $course_date->getKey();
            $data['user_id'] = User::auth()->getKey();
            $course_participation = CourseParticipationTransformer::prepare($data);
            $course_participation->save();
            return response()->json([
                'success' => 'You have successfully registered to course'
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ]);
        }
    }

    /**
     * @param CourseParticipationDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyParticipation(CourseParticipationDeleteRequest $request)
    {
        if (is_null(User::auth())) {
            return response()->json([
                'error' => 'You have not permission to this action'
            ], 403);
        }
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'Wrong data'
            ], 422);
        }
        /**
         * @var $course_date CourseDate
         */
        try {
            $course_date = CourseDate::findOrFail($request->get('course_date_id'));
            if (!$course_date->checkParticipation(User::auth())) {
                return response()->json([
                    'error' => "You have not participated this course"
                ], 403);
            }
            $course_date->getParticipant(User::auth())->delete();
            return response()->json([
                'success' => 'Your participation has successfully deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
