<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private static $count = 20;

    public function run()
    {
        $userCount = \App\User::all()->count();
        $users = [];
        for ($i = 0; $i < static::$count; $i++) {
            $users[] = [
                'first_name' => 'user_first_name' . $userCount . $i,
                'last_name' => 'user_last_name' . $userCount . $i,
                'address' => 'Yerevan, Armenia',
                'dob' => '2017-07-09T20:00:00.000Z',
                'longitude' => '44.503490',
                'latitude' =>'40.177200',
                'email'=>'user_first_name' . $userCount . $i.'@gmail.com',
                'role_id'=>\App\Models\SystemConstants\SystemConstants::NOT_ADMIN,
                'user_type'=>\App\Models\SystemConstants\SystemConstants::SIMPLE_USER,
                'is_completed'=>0,
                'completed_percent'=>45,
                'opt_in'=>'yes',
                'password'=>bcrypt('asdasd'),
                'score'=>0
            ];
        }
        \App\User::insert($users);
    }
}
