<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemLevels extends Model
{
    //
    protected static $waterline = 50;

    protected $table = 'system_levels';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'real_name' .
        'img_name',
        'up_score',
        'down_score'
    ];

    public static function getLevel($score)
    {
        if ($score > static::$waterline) {
            return static::where('down_score', '=', static::$waterline)->first()->getKey();
        }
        if ($score == 0) {
            return static::where('down_score', '=', $score)->first()->getKey();
        }
        return static::where('down_score', '<', $score)->where('up_score', '>=', $score)->first()->getKey();

    }
}
