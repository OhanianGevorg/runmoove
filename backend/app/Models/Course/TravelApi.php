<?php

namespace App\Models\Course;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TravelApi extends Model
{
    const HOTELS = 1;
    const AVIA = 2;
    protected $table = 'travel_api';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_date_id',
        'type',
        'body',
        'city_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course_date()
    {
        return $this->belongsTo(CourseDate::class, 'course_date_id');
    }

    public static function canCallApi(CourseDate $courseDate, $type)
    {
        try {
            $travelData = static::where('type', '=', $type)
                ->where('course_date_id', '=', $courseDate->getKey())->firstOrFail();
            return Carbon::now()->diffInHours($travelData->updated_at) > 2;
        } catch (ModelNotFoundException $e) {
            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

    }

    public static function typeIs($type)
    {
        switch ($type) {
            case (self::HOTELS):
                return self::HOTELS;
            case (self::AVIA):
                return self::AVIA;
            default:
                return null;
        }
    }
}
