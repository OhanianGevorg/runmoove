<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBlockerIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('friendlist', function (Blueprint $table) {
            $table->dropForeign('friendlist_blocker_id_foreign');
            $table->dropColumn('blocker_id');
        });
        Schema::table('friendlist', function (Blueprint $table) {
            $table->integer('blocker_id')->unsigned()->nullable();
            $table->foreign('blocker_id')
                ->references('id')
                ->on('users')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('friendlist', function (Blueprint $table) {
            $table->dropForeign('friendlist_blocker_id_foreign');
            $table->dropColumn('blocker_id');
        });
        Schema::table('friendlist', function (Blueprint $table) {
            $table->integer('blocker_id')->unsigned();
            $table->foreign('blocker_id')
                ->references('id')
                ->on('users')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }
}
