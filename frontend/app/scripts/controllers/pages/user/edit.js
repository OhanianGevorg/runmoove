'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PagesUserEditCtrl
 * @description
 * # PagesUserEditCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
  .controller('ProfileEditCtrl', function ($scope, $stateParams,Users,AuthService,$state,ENV,$localStorage,ngDialog,$mdToast,$timeout) {

    $scope.userId = $stateParams.id;

    if ( $scope.userId  != AuthService.userID()) {
      $state.go('run.home');
    }

    Users.show({id:$scope.userId,user_id:$scope.userId},function (res) {
      $scope.user = res.user;

      $scope.slider.value = $scope.user.completed_percent;

      if ($scope.user.completed_percent >= 90) {
        $scope.profileIsComplated = true;
      } else {
        $scope.profileIsComplated = false;
      }

      $scope.data = {
        first_name:$scope.user.first_name,
        last_name:$scope.user.last_name,
        email:$scope.user.email,
        dob:$scope.user.dob,
        address:$scope.user.address,
        avatar:$scope.user.avatar,
      };


      $scope.pass_data = {
        old_password: '',
        password: '',
        confirm_password: '',
      };



      var myDropzone = angular.element('#UserDropZone')[0].dropzone;

      var mockFile = {
        accepted:true,

        name: 'Avatar 1',
        size: 123456,
        type: 'image/jpeg',
        status: Dropzone.SUCCESS,
        url: $scope.user.avatar,
      };
      // Call the default addedfile event handler

      myDropzone.emit("addedfile", mockFile);

      // And optionally show the thumbnail of the file:
      myDropzone.emit("thumbnail", mockFile, $scope.user.avatar);

      myDropzone.files.push(mockFile);




      $scope.InProgress = false;

    });

    //  main slider
    $scope.slider = {
      value:0,
      options: {
        floor: 0,
        ceil: 100,
        step: 1,
        disabled:true
      }
    };


    //Set options for dropzone
    //Visit http://www.dropzonejs.com/#configuration-options for more options
    $scope.dzOptions = {
      url: ENV.apiEndpoint + '/upload',
      paramName: 'photo',
      maxFilesize: '30',
      acceptedFiles: 'image/jpeg, images/jpg, image/png',
      addRemoveLinks: true,
      dictRemoveFile: "",
      dictCancelUpload: "",
      clickable: '#editPhotoBtn',
      parallelUploads: 1,
      autoProcessQueue: true,
      maxFiles:1,
      uploadMultiple:false,

    };

    //Handle events for dropzone
    //Visit http://www.dropzonejs.com/#events for more events
    $scope.dzCallbacks = {

      addedfile: function (file) {
        angular.element('.dz-remove').addClass('fa fa-remove');
        if (!$scope.InProgress) {
          $scope.newFile = file;
        }
        $scope.InProgress = true;

      },
      success: function (file, xhr) {
        if (xhr.hasOwnProperty('image')) {
          $scope.avatarFile = xhr.image;
          $scope.InProgress = false;
        }

        angular.element('.dz-remove').addClass('fa fa-remove');
      },
      error: function (file) {
        $scope.newFile.length = 0;
      },

      removedfile: function (file) {
        var image = JSON.parse(file.xhr.response);
        var index;
        for (var i = 0;i<$scope.data.course_images.length;i++){
          if($scope.data.course_images[i].url==image.image){
            index = i;
            break;
          }
        }
        $scope.data.course_images.splice(index, 1);
      },
      uploadprogress: function (file, progress) {
        file.progress = progress;
        if(progress===100){
          $scope.InProgress = true;
        }
      },
      sending: function(file,xhr) {
        return xhr.setRequestHeader('X-TOKEN', $localStorage.api_token);
      }
    };


    //Apply methods for dropzone
    //Visit http://www.dropzonejs.com/#dropzone-methods for more methods
    $scope.dzMethods = {};
    $scope.removeNewFile = function () {
      $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
    };

    $scope.editUserProfile = function (event) {



      Users.update({id:$scope.userId,user_id:$scope.userId},$scope.data,function (res) {
        $scope.data.avatar = $scope.avatarFile;
        if ($scope.data.avatar) {
          Users.changeAvatar($scope.data,function (response) {
            AuthService.setAvatar(response.file);
          });
        }
        $state.go('profile.view',{id:AuthService.userID()},{reload:true});
        $timeout(function () {
        $mdToast.show(
          $mdToast.simple()
            .textContent('Votre profil a été changé!')
            .position('top right')
            .hideDelay(3000)
        );
      },1500);
      });
    };


    /*** CHANGE PASSWORD ***/


    $scope.openChangePassPopup = function () {
      ngDialog.open({
        template: '/views/popups/password_change.html',
        className: 'ngdialog-theme-default',
        width: '100%',
        scope:$scope,
        overlay: false,
        showClose: true,
        closeByDocument:true
      });
    };


    $scope.changeUserPassword = function () {
      Users.changePassword($scope.pass_data,function (res) {
      },function (err) {
        $scope.errors = err.data;
      });
    };


  });
