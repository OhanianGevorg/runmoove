<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/1/17
 * Time: 10:15 AM
 */

namespace App\Services\TravelAPI;


abstract class AbstractGetAPIData
{
    protected $token = '';
    protected $marker = '';
    public abstract function getAPIData(array $params);

    /**
     * @param array $queryParam
     * @return string
     */
    protected function buildSearchQuery(array $queryParam)
    {
        $queryStr = '';
        foreach ($queryParam as $key => $value) {
            if ($value !== null && $value !== "") {
                $queryStr .= $key . '=' . $value . '&';
            }
        }
        return $queryStr;
    }

    protected abstract function fillSearchData(array $searchData, $type);

    protected abstract function initCurl($query);

    protected abstract function getSearchID(array  $searchData);

    /**
     * @param array $data
     * @return string
     */
    protected function kSortAndGet(array $data)
    {
        $dataStr = '';
        ksort($data);
        foreach ($data as $key => $datum) {
            if (is_array($datum)) {
                $dataStr .= $this->kSortAndGet($datum);
            } else {
                if ($datum !== null && $datum !== "") {
                    $dataStr .= ":" . $datum;
                }
            }

        }
        return $dataStr;
    }

    /**
     * @param array $data
     * @return null|string
     */
    protected function makeSignature(array $data)
    {
        try {
            return md5($this->token . $this->kSortAndGet($data));
        } catch (\Exception $e) {
            return null;
        }
    }
}