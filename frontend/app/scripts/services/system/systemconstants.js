'use strict';

/**
 * @ngdoc service
 * @name frontendApp.system/systemConstants
 * @description
 * # system/systemConstants
 * Service in the frontendApp.
 */
angular.module('runMoovApp')
  .service('systemConstants', function ($resource, ENV) {

    return $resource(ENV.apiEndpoint + '/constants', {}, {
      get: {
        url:ENV.apiEndpoint + '/constants/fields',
        method: 'GET',
        isArray: false
      },
      opinionFields: {
        url:ENV.apiEndpoint + '/constants/opinionFields',
        method:'GET',
      }
    });

  });
