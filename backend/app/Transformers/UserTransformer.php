<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 5/28/2017
 * Time: 7:36 PM
 */

namespace App\Transformers;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Friendship\FriendRequest;
use App\Models\SystemConstants\SystemConstants;
use App\Traits\TransformCollection;
use App\Transformers\CourseTransformers\CourseDateTransformer;
use App\Transformers\Friendship\FriendListTransformer;
use App\Transformers\Friendship\FriendRequestTransformer;
use App\Transformers\NotificationTransformer\NotificationTransformer;
use App\Transformers\Opinion\CourseOpinionTransformer;
use App\Transformers\Shared\LevelTransformer;
use App\Transformers\Shared\StaticTransformer;
use App\Transformers\Shared\UserRecordTransformer;
use App\User;
use Illuminate\Support\Facades\Storage;

class UserTransformer implements PrepareInterface, TransformerInterface
{

    use TransformCollection;

    public static function smallTransformer($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'email' => $model->email,
            'user_type' => $model->user_type,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'avatar' => (!filter_var($model->avatar, FILTER_VALIDATE_URL) && Storage::disk('public')->exists($model->avatar)) ? asset(Storage::url($model->avatar)) : $model->avatar,
            'user_level' => LevelTransformer::transform($model->get_user_level())
        ];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    public static function transform($model)
    {
        /**
         * @var $model User
         */
        if (!$model) {
            return [];
        }
        $user = [
            'id' => $model->getKey(),
            'email' => $model->email,
            'user_type' => $model->user_type,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'address' => $model->address,
            'dob' => $model->dob,
            'avatar' => (!filter_var($model->avatar, FILTER_VALIDATE_URL) && Storage::disk('public')->exists($model->avatar)) ? asset(Storage::url($model->avatar)) : $model->avatar,
            'role_id' => $model->role_id,
            'is_completed' => $model->is_completed,
            'completed_percent' => $model->completed_percent,
            'fb_id' => $model->fb_id,
            'twitter_id' => $model->twitter_id,
            'practice_since' => $model->practice_since,
            'running_frequency' => $model->running_frequency,
            'transportation_mode' => $model->transportation_mode,
            'budget' => $model->budget,
            'practice_location' => $model->practice_location,
            'living_preferences' => $model->living_preferences,
            'voyage_with' => $model->voyage_with,
            'presentation' => $model->presentation,
            'course_formats' => StaticTransformer::transformCollection($model->course_formats()),
            'course_importance' => StaticTransformer::transformCollection($model->course_importances()),
            'user_practicing' => StaticTransformer::transformCollection($model->user_practicings()),
            'user_level' => LevelTransformer::transform($model->get_user_level()),
            'score' => $model->score,
            'records' => UserRecordTransformer::transformCollection($model->records),
            'participation' => CourseDateTransformer::transformCollection($model->get_participation_courses()),
            'opinions' => CourseOpinionTransformer::transformCollection($model->course_opinions),
            'created_courses' => CourseDateTransformer::transformCollection($model->ownCourses),
        ];

        if (User::auth() && (int)User::auth()->getKey() !== $model->getKey()) {
            $user['is_blocked'] = User::auth()->is_blocked($model->getKey());
            $user['own_block'] = User::auth()->is_blocker($model->getKey());
            $user['is_friend'] = User::auth()->hasFriend($model->getKey());
        }
        if (User::auth() && (int)User::auth()->getKey() === (int)$model->getKey()) {
            $user = array_merge($user, [
                'friend_requests' => FriendRequestTransformer::transformCollection($model->receivedFriendRequest()->friendRequestByStatus(FriendRequest::PENDING)->get()),
                'notifications' => NotificationTransformer::transformCollection($model->getSortedNotifications()),
                'messages_count' => $model->getUnreadMessagesCount()
            ]);
        }

        return $user;
    }

    /**
     * @param array $raw
     * @param array $injector
     * @return \App\User
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new User([
            'first_name' => array_get($raw, 'first_name'),
            'last_name' => array_get($raw, 'last_name'),
            'email' => array_get($raw, 'email'),
            'password' => bcrypt(array_get($raw, 'password')),
            'dob' => array_get($raw, 'dob'),
            'address' => array_get($raw, 'address'),
            'longitude' => array_get($raw, 'longitude'),
            'latitude' => array_get($raw, 'latitude'),
            'twitter_id' => array_get($raw, 'twitter_id'),
            'fb_id' => array_get($raw, 'fb_id'),
            'avatar' => array_get($raw, 'avatar'),
            'opt_in' => array_key_exists('is_subscribed', $raw) && $raw['is_subscribed'] == 'true' ? 'yes' : 'no',
            'role_id' => SystemConstants::NOT_ADMIN,
            'user_type' => array_key_exists('is_organizer', $raw) ? SystemConstants::ORGANIZER : SystemConstants::SIMPLE_USER,
            'is_completed' => 0,
            'completed_percent' => 0,
            'score' => 0
        ]);
    }

    public static function prepareAdditional(array $raw, array $injector = [])
    {

        return [
            'practice_since' => array_key_exists('practice_since', $raw) ? $raw['practice_since'] : '',
            'running_frequency' => array_key_exists('running_frequency', $raw) ? $raw['running_frequency'] : '',
            'transportation_mode' => array_key_exists('transportation_mode', $raw) ? $raw['transportation_mode'] : '',
            'budget' => array_key_exists('budget', $raw) ? $raw['budget'] : '',
            'practice_location' => array_key_exists('practice_location', $raw) ? $raw['practice_location'] : '',
            'living_preferences' => array_key_exists('living_preferences', $raw) ? $raw['living_preferences'] : '',
            'voyage_with' => array_key_exists('voyage_with', $raw) ? $raw['voyage_with'] : '',
            'presentation' => array_key_exists('presentation', $raw) ? $raw['presentation'] : ''
        ];
    }

    public static function updatePrepare(array $raw, array $injector = [])
    {
        return [
            'first_name' => array_get($raw, 'first_name'),
            'last_name' => array_get($raw, 'last_name'),
            'email' => array_get($raw, 'email'),
            'dob' => array_get($raw, 'dob'),
            'address' => array_get($raw, 'address'),
            'opt_in' => array_key_exists('is_subscribed', $raw) && $raw['is_subscribed'] == 'true' ? 'yes' : 'no',
        ];
    }
}