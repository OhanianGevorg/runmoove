<?php

namespace App\Providers;

use App\Libs\EventClassesDescriber;
use App\Libs\TGValidator;
use App\Observers\OwnObservers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new TGValidator($translator, $data, $rules, $messages);
        });

        foreach (EventClassesDescriber::getEventClasses() as $class=>$value){
            if(class_exists($class)){
                $class::observe(OwnObservers::class);
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
