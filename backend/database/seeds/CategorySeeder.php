<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    private static $categories = [
        ['name'=>'Dans les baskets de ...'],
        ['name'=>'Une course avec ...'],
        ['name'=>'Interview'],
        ['name'=>'Caméra embarquée'],
        ['name'=>'Bons plans'],
        ['name'=>'Conseils'],
        ['name'=>'Test'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Article\Categories::insert(self::$categories);
    }
}
