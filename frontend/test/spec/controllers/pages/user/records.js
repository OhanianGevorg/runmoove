'use strict';

describe('Controller: PagesUserRecordsCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesUserRecordsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesUserRecordsCtrl = $controller('PagesUserRecordsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesUserRecordsCtrl.awesomeThings.length).toBe(3);
  });
});
