<?php

namespace App\Events;

use App\Models\UserRelated\Notifications;
use App\Traits\SendEmailTrait;
use App\Transformers\Chat\MessageTransformer;
use App\Transformers\NotificationTransformer\NotificationTransformer;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSendEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels, SendEmailTrait;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $model;
    private $event_name = 'message.sent';

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('private-chat.' . $this->model->recipient_id);
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return $this->event_name;
    }

    public function broadcastWith()
    {

        $data = [
            'owner_id' => ($this->model->recipient_id) ? $this->model->recipient_id : null,
            'event_name' => $this->event_name,
            'sender_id' => ($this->model->user_id) ? $this->model->user_id : null,
            'is_read' => Notifications::IS_NOT_READ,
            'event_id' => $this->model->getKey(),
            'event_type' => get_class($this->model)
        ];
        $event = NotificationTransformer::prepare($data);
        $event->save();
        /**
         * @desc send mail about notification
         */
        try {
            $user = User::findOrFail($event->owner_id);
            $this->sendEmail($event, $user);
        } catch (ModelNotFoundException $e) {
        } catch (\Exception $e) {
        }
        return [
            'event' => NotificationTransformer::transform($event),
            'mail' => MessageTransformer::transform($this->model),
            'message' => 'You have a new message'
        ];
    }
}
