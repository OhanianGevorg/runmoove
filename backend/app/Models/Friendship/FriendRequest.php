<?php

namespace App\Models\Friendship;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FriendRequest extends Model
{
    //
    use SoftDeletes, FilterTrait;

    const PENDING = 0;
    const ACCEPTED = 1;
    const DECLINED = 3;
    const EVENT_NAME = 'friendRequest';
    protected $table = 'friend_requests';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sender_id',
        'recipient_id',
        'status'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function isAccepted()
    {
        return (int)$this->status === self::ACCEPTED;
    }
    public function scopeFriendRequestByStatus($query, $status)
    {
        return $query->where('status', '=', $status);
    }

    public function notification()
    {
        return $this->morphMany('App\Models\UserRelated\Notifications','event');
    }
}
