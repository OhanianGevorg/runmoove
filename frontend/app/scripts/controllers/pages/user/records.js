'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesUserRecordsCtrl
 * @description
 * # PagesUserRecordsCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('UserRecordsCtrl', function (userRecords,Runs,$scope,$stateParams, ENV) {
    $scope.userID = $stateParams.id;

    $scope.data = {
      'user_id':$scope.userID,
      'course_date_id':'',
      'time':'',
    };

    $scope.selectedRun = [];

    $scope.runsURL = ENV.apiEndpoint + '/course';



    userRecords.query({user_id:$scope.userID},function (res) {

      $scope.records = res.records;

      $scope.addRecord = function () {
        $scope.data.course_date_id = $scope.selectedRun.originalObject.course_id;
        userRecords.save($scope.data,function (res) {
          $scope.addedRun = [{name:$scope.selectedRun.originalObject.course.name}];
          $scope.records.push({ course_date_id:$scope.addedRun, time:$scope.data.time, id:res.success.id, user_id:$scope.userID });
        });
      };

      $scope.removeRecord = function (id, index) {
        userRecords.delete({id:id},function (res) {
          $scope.records.splice(index,1);
          return;
        });
      };

    });

  });
