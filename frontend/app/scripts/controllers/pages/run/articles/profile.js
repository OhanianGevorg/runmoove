'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesRunArticlesProfileCtrl
 * @description
 * # PagesRunArticlesProfileCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('ArticleProfileCtrl', function (Articles,$stateParams,$scope) {
    Articles.show({id:$stateParams.id},function (res) {
      $scope.article = res.data;
    });
  });
