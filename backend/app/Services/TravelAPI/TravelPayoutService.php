<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/29/2017
 * Time: 12:36 AM
 */

namespace App\Services\TravelAPI;


use App\Models\Course\CourseDate;
use App\Models\Course\TravelApi;

class TravelPayoutService
{
    private $courseDate;
    private $data;
    private $type;

    public function __construct(CourseDate $courseDate, array $data, $type)
    {
        $this->courseDate = $courseDate;
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * @desc fill TravelAPI table
     */
    public function fillData()
    {
        if(!$this->type){
            throw new \InvalidArgumentException("Type is required",422);
        }
        try {
            TravelApi::updateOrCreate([
                'course_date_id' => $this->courseDate->getKey(),
                'type' => $this->type,
            ], [
                'body' => json_encode($this->data['content']),
                'city_id' => $this->data['city_id']
            ]);
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }
}