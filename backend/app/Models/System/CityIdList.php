<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class CityIdList extends Model
{
    protected $table = "city_id_list";
    protected $primaryKey = "id";
    protected $fillable = [
        "city_id",
        "lng",
        "lat",
        "country_short",
        "city"
    ];
}
