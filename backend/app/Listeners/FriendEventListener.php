<?php

namespace App\Listeners;

use App\Events\FriendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FriendEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Friends  $event
     * @return void
     */
    public function handle(FriendEvent $event)
    {
        //
    }
}
