'use strict';

describe('Controller: PagesUserUpdateCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesUserUpdateCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesUserUpdateCtrl = $controller('PagesUserUpdateCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesUserUpdateCtrl.awesomeThings.length).toBe(3);
  });
});
