<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/11/2017
 * Time: 1:16 AM
 */

namespace App\Libs;


use App\User;

class Calculations
{
    /**
     * @param User $user
     * @return User
     */
    public static function calculateFillPercent(User $user)
    {
        $countableList = $user->getCountableFields();
        $filledCount = 0;
        foreach ($user->getAttributes() as $key => $value) {
            if (in_array($key, $countableList) && $value) {
                $filledCount++;
            }
        }
        if ($user->user_course_formats) {
            $filledCount++;
        }
        if ($user->user_course_importances) {
            $filledCount++;
        }
        if ($user->user_practicing_types) {
            $filledCount++;
        }
        $percent = (int)ceil($filledCount * 100 / count($countableList));
        $user->completed_percent = $percent;

        if ($percent >= 90) {
            $user->is_completed = true;
        }
        return $user;
    }
}