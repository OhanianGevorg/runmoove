<?php

namespace App\Models\SystemRelated;

use Illuminate\Database\Eloquent\Model;

class UserCourseImportance extends Model
{
    //
    protected $table = 'user_course_importance';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'importance_id'
    ];
}
