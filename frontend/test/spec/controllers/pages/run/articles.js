'use strict';

describe('Controller: PagesRunArticlesCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesRunArticlesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunArticlesCtrl = $controller('PagesRunArticlesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunArticlesCtrl.awesomeThings.length).toBe(3);
  });
});
