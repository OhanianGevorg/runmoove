'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.courseParticipation
 * @description
 * # courseParticipation
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('courseParticipation', function (ENV,$resource) {

    return $resource(ENV.apiEndpoint + '/participation/:id', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      save:{
        method:'POST'
      },
      delete:{
        method:'DELETE'
      },
      destroy:{
        url:ENV.apiEndpoint + '/participation/delete',
        method:'DELETE'
      },
    });
  });
