'use strict';

describe('Directive: rmScroll', function () {

  // load the directive's module
  beforeEach(module('runMoovApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<rm-scroll></rm-scroll>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the rmScroll directive');
  }));
});
