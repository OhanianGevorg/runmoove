<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 5/27/2017
 * Time: 1:59 AM
 */
namespace App\Models\SystemConstants;

use App\Models\System\SystemCourseFormat;
use App\Models\System\SystemCourseImportance;
use App\Models\System\SystemPracticingType;
use App\Models\SystemRelated\UserCourseFormat;
use App\Transformers\Shared\StaticTransformer;

class SystemConstants
{
    const ORGANIZER = 1;
    const SIMPLE_USER = 2;
    const ADMIN_ROLE = 3;
    const NOT_ADMIN = 4;

    public static $practice_location = [
        'my_city' => 'Dans ma ville',
        'my_department' => 'Dans mon département',
        'france' => 'En France',
        'europe' => 'En Europe',
        'world' => 'Partout dans le Monde'
    ];

    public static $running_frequency = [
        'more_than_three' => '+de 3 fois par semaine',
        'equal_three' => '3 fois par semaine',
        'less_than_three' => '1 fois par semaine',
        'less_than_one' => 'd\'une fois par semaine'
    ];

    public static $budget = [
        'economic' => 'Economique',
        'comfort' => 'Confortable',
        'lux' => 'Lux'
    ];

    public static $transportation_mode = [
        'carpooling' => 'Covoiturage',
        'train' => 'Train',
        'air' => 'Avion',
        'car' => 'Voiture personelle'
    ];
    public static $living_preference = [
        'flat' => 'Gites & Chambres d\'hôtes',
        'hotel' => 'Hôtel',
        'family' => 'Famille & Amis',
        'rental' => 'Location chez un particulier',
        'camping' => 'Camping'
    ];

    public static $voyage_with = [
        'alone' => 'Seul(e)',
        'two' => 'A deux',
        'small_group' => 'En petit groupe ( > 5 )',
        'group' => 'En groupe ( + de 5 )'
    ];

    public static $marker_type = [
        'Départ' => '',
        'Arrivée ' => '',
        'Balise' => '',
        'Ravitaillement' => ''
    ];

    public static $participation_purposes = [
        'atmosphere' => 'L\'ambiance',
        'course' => 'Le parcours',
        'prestige' => 'Le prestige',
        'solidarity' => 'La solidarité',
        'chrono' => 'Faire un chrono'
    ];

    public static $participate_again = [
        'sure' => 'C’est sur !',
        'surely' => 'Surement',
        'maybe' => 'Peut-être',
        'never' => 'Jamais'
    ];

    public static function getAll()
    {
        return [
            'budget' => self::$budget,
            'practice_location' => self::$practice_location,
            'running_frequency' => self::$running_frequency,
            'transportation_mode' => self::$transportation_mode,
            'living_preferences' => self::$living_preference,
            'voyage_with' => self::$voyage_with,
            'marker_type' => self::$marker_type,
            'course_formats' => StaticTransformer::transformCollection(SystemCourseFormat::all()),
            'course_importance' => StaticTransformer::transformCollection(SystemCourseImportance::all()),
            'practicing_types' => StaticTransformer::transformCollection(SystemPracticingType::all()),
        ];
    }

    public static function getForCourse()
    {
        return [
            'marker_type' => self::$marker_type,
            'course_formats' => StaticTransformer::transformCollection(SystemCourseFormat::all()),
            'practicing_types' => StaticTransformer::transformCollection(SystemPracticingType::all()),
        ];
    }

    public static function getForOpinion()
    {
        return [
            'participate_again' => self::$participate_again,
            'participation_purposes' => self::$participation_purposes
        ];
    }
}