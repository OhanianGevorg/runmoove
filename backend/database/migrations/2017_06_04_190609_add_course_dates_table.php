<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCourseDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('course_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_owner_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->dateTime('start_at');
            $table->dateTime('end_at');
            $table->nullableTimestamps();
            $table->softDeletes();

            $table->foreign('course_owner_id')
                ->references('id')
                ->on('users')
                ->onUpdate('no action')
                ->onDelete('no action');
            $table->foreign('course_id')
                ->references('id')
                ->on('courses')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_dates');
    }
}
