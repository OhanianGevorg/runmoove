<?php

use Illuminate\Database\Seeder;

class CityIdListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = new \App\Libs\GetCityList("C:\\phpscript\\city.list.json");
        $city_list = $list->getCityListArray();
        if ($city_list) {
            $query = [];
            $iterator = 0;
            foreach ($city_list as $key => $city_data) {
//                dd($city_data);
                $query[] = [
                    'city_id' => $city_data['id'],
                    'country_short' => $city_data['country'],
                    'city' => $city_data['name'],
                    'lat' => $city_data['coord']['lat'],
                    'lng' => $city_data['coord']['lon']
                ];
                $iterator += 1;
                if ($iterator === 99) {
                    try {
                        \App\Models\System\CityIdList::insert($query);
                        $query = [];
                        $iterator = 0;
                        echo "success";
                    } catch (Exception $e) {
                        dd($e->getMessage().$e->getLine(),$e->getFile());
                    }

                }
            }
        }
    }
}
