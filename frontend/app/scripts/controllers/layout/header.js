'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:LayoutHeaderCtrl
 * @description
 * # LayoutHeaderCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
  .controller('HeaderCtrl', function ($scope, AuthService,$localStorage,$rootScope,systemConstants,$state,friendShip,Notification) {

    $scope.currentUser = AuthService;
    $scope.menuIsOpen = false;

    $rootScope.searchRunCriteria = {
      'location':'',
      'start_at':'',
      'end_at':'',
      'name':'',
      'distance':'',
      'types':'',
      'longitude':'',
      'latitude':''
    };


    $rootScope.filterField = {
      address: [],
      types: []
    };

    /**
     * get System Constants
     */
    systemConstants.get(function (res) {
      $scope.systemTypes = res.system_types;
    });

    $rootScope.filterSlider = {
      value:0,
      options: {
        floor: 0,
        ceil: 100,
        step: 10,
        onEnd: function(id) {
          $rootScope.searchRunCriteria.distance = $rootScope.filterSlider.value;
        }
      }
    };


    $rootScope.filterTypesObj = [];

    $scope.menuToggle = function () {
      $scope.menuIsOpen = !$scope.menuIsOpen;
    };

    $scope.callRunSearch = function (page) {

      if ($state.current.name === 'opinion.main') {
        $rootScope.searchRuns();
        return;
      }

      if ($state.current.name !== 'run.home') {
        $state.go('run.home');
      } else {
        $rootScope.searchRuns();
      }
    };

    // angular.element('.menu-toggle-btn').click(function(){
    //  angular.element('.profile-menu').toggleClass('open');
    // });

    angular.element('.menu-toggle-btn-mobile').click(function() {
      $('.responsive-menu').toggleClass('expand');
    });


    angular.element('.profile-menu').click(function(){
      $(this).removeClass('open');
    });

    angular.element('.responsive-menu').click(function(){
      $(this).removeClass('expand');
    });


    /* *************** Search filters ******************** */
    $('.advance-filter').hide();
    $('.simple').click(function(){
      $(this).parent().hide();
      $('.advance-filter').slideDown('fast');
    });

    $('.advance').click(function(){
      $(this).parent().hide();
      $('.simple-filter').show();
    });

    var initialValue = 25;

    var sliderTooltip = function(event, ui) {
      var curValue = ui.value || initialValue;
      var tooltip = '<div class="tooltips"><div class="tooltips-inner">' + curValue + ' KM</div></div>';

      angular.element('.ui-slider-handle').html(tooltip);

    };

    // angular.element("#slider6").slider({
    //   value: initialValue,
    //   min: 1,
    //   max: 100,
    //   step: 1,
    //   create: sliderTooltip,
    //   slide: sliderTooltip
    // });

    $rootScope.acceptFriend = function (id,index) {
      friendShip.acceptFriend({event_id: id}, function (res) {
        if (res.hasOwnProperty('success')) {
          $scope.frRequest = true;
          Notification.setStatus({notification_ids:[id]},function (res) {
            if (res.hasOwnProperty('success')) {
              $rootScope.getActiveNotification.splice(index,1);
            }
          });
        }
      });
    };

    $rootScope.declineFriend = function (id,index) {
      friendShip.decline({event_id: id}, function (res) {
        if (res.hasOwnProperty('success')) {
          $scope.frRequest = true;
          Notification.setStatus({notification_ids:[id]},function (res) {
            if (res.hasOwnProperty('success')) {
              $rootScope.getActiveNotification.splice(index,1);
            }
          });
        }
      });
    };



    Notification.query({sort:'created'},function (res) {

      angular.forEach(res.notifications,function (item) {
        if (item.is_read === 2) {
          if (item.event_name === 'message.sent') {
            $rootScope.getActiveMessages.push(item);
            // var audioMsg = new Audio('https://notificationsounds.com/message-tones/jingle-bells-sms-523/download/mp3');
            // audioMsg.play();
          } else {

            audioNtf.play();
          }
        }
      });


      $scope.closeNotificationItem = function (id,index) {

        Notification.setStatus({notification_ids:[id]},function (res) {
          if (res.hasOwnProperty('success')) {
            $rootScope.getActiveNotification.splice(index,1);
          }
        });
      };

    });

    $scope.menuItemCount = angular.element('header md-menu-item').length;


  });
