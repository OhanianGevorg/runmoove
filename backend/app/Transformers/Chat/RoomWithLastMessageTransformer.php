<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/18/17
 * Time: 11:37 AM
 */

namespace App\Transformers\Chat;


use App\Interfaces\TransformerInterface;
use App\Models\Chat\Room;
use App\Traits\TransformCollection;
use App\User;

class RoomWithLastMessageTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        /**
         * @var $model Room
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'participants' => ParticipantsTransformer::transformCollection($model->participants),
            'messages' => MessageTransformer::transform($model->messages()->last()->first()),
            'user_unread_messages' => $model->messages()->unreadMessages(User::auth())->count(),
            'unread_messages' => $model->messages()->unreadMessages()->count(),
        ];
    }
}