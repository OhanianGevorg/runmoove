'use strict';

describe('Service: userRecords', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var userRecords;
  beforeEach(inject(function (_userRecords_) {
    userRecords = _userRecords_;
  }));

  it('should do something', function () {
    expect(!!userRecords).toBe(true);
  });

});
