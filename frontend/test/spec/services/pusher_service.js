'use strict';

describe('Service: pusherService', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var pusherService;
  beforeEach(inject(function (_pusherService_) {
    pusherService = _pusherService_;
  }));

  it('should do something', function () {
    expect(!!pusherService).toBe(true);
  });

});
