<?php

namespace App\Mail;

use App\Models\UserRelated\Notifications;
use App\Transformers\NotificationTransformer\NotificationTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;

;
use Illuminate\Queue\SerializesModels;

class NotificationMessage extends Mailable
{
    use Queueable, SerializesModels;

    private $model;

    /**
     * NotificationMessage constructor.
     * @param Notifications $model
     */
    public function __construct(Notifications $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $notification = NotificationTransformer::transform($this->model);
        return $this->view('emails.notification_image', compact('notification'));
    }
}
