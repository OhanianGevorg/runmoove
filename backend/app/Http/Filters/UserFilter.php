<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/11/2017
 * Time: 12:52 AM
 */

namespace App\Http\Filters;


class UserFilter extends AbstractFilter
{
    /**
     * CourseFilter constructor.
     * @param $query
     */
    public function __construct($query)
    {
        parent::__construct($query);
    }

    /**
     *  Allowed filter fields
     */
    protected $filter = [
        'email',
        'first_name',
        'last_name',
        'dob',
        'latitude',
        'longitude',
        'name'
    ];

    /**
     *  Allowed sorting fields
     */
    protected $order = [
        'first_name',
        'created'
    ];

    /**
     * Filter by "name" field
     *
     * @param string $value
     */
    protected function filterByFirst_name($value = '')
    {
        $this->query->where('users.first_name', 'like', '%' . $value . '%');
    }

    /**
     * Filter by "tag" field
     *
     * @param string $value
     */
    protected function filterByLast_name($value = '')
    {
        $this->query->where('users.last_name', 'like', '%' . $value . '%');
    }

    /**
     * Filter by "domain_id" field
     *
     * @param string $value
     */
    protected function filterByEmail($value = '')
    {
        $this->query->where('users.email', $value);
    }

    protected function filterByLatitude($value = '')
    {
        $this->query->where('users.latitude', $value);
    }

    protected function filterByLongitude($value = '')
    {
        $this->query->where('users.longitude', $value);
    }

    protected function filterByName($value = '')
    {
        $val = $value ? explode(' ', $value)[0] :$value;
        $this->query->where('users.first_name', 'like', '%' . $val . '%')
            ->orWhere('users.last_name', 'like', '%' . $val . '%');
    }

    /**
     * Sort by name
     *
     * @param string $dir , ASC or DESC
     */
    protected function sortByName($dir = '')
    {
        $this->query->orderBy('name', $dir);
    }

    /**
     * Sort by created_at
     *
     * @param string $dir , ASC or DESC
     */
    protected function sortByCreated($dir = '')
    {
        $this->query->orderBy('created_at', $dir);
    }
}