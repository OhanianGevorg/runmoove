'use strict';

describe('Controller: AuthAuthCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var AuthAuthCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthAuthCtrl = $controller('AuthAuthCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AuthAuthCtrl.awesomeThings.length).toBe(3);
  });
});
