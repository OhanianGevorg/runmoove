'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.notifications
 * @description
 * # notifications
 * Factory in the runMoovApp.
 */
angular.module('runMoovApp')
  .factory('Notifications', function (ENV,$resource) {
      return $resource(ENV.apiEndpoint + '/eventlist', {}, {
          get:{
              method:'GET'
          }
      });
  });
