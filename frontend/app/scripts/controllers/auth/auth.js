angular.module('runMoovApp')
    .controller('AuthCtrl', function ($scope, $rootScope, Users, AuthService, $state, $mdToast, twitterService, ngDialog, $timeout, moment,PusherService) {

        /**
         * @desc object for keep all sign up data
         */

        if (AuthService.isAuthenticated()) {
            $state.go('profile.view', {id: AuthService.userID()});
        }

        $scope.data = {
            first_name: '',
            last_name: '',
            email: '',
            dob: '',
            password: '',
            address: '',
            is_organizer: 2,
            is_subscribed: 'no',
            twitter_id: '',
        };

        $scope.authData = [];


        $scope.twLogin = function () {
            twttr.anywhere(function (T) {
                T.signIn();
            });
        };


        $scope.isValid = '';
        $scope.notSaveSearch = true;
        $scope.errors = {};

        $scope.address = {};

        $scope.credentials = {
            email: '',
            password: ''
        };


        $scope.login = function (credentials) {

            AuthService.login(credentials).then(function (response) {

                $rootScope.openLogin = false;
                $rootScope.AuthUser = AuthService;
                $rootScope.closeAllPopups();
                // PusherService.initPusher(response['user']['id'],true,response['api_token']);
                $state.go('profile.view', {id: AuthService.userID()});

            }, function (errors) {
                $scope.login_errors = errors.data;
                $scope.isValid = ' error-login';
            });
        };
        /**
         * @desc Send Post request to server
         */
        $scope.saveUser = function (type) {


            $scope.data.password_confirmation = $scope.data.password;

            if (type !== 'social') {

                $scope.data.address = $scope.address.formatted_address;
                // $scope.data.latitude = $scope.address.geo.geometry.location.lat();
                // $scope.data.longitude = $scope.address.geo.geometry.location.lng();

            }

            Users.save($scope.data, function (data) {
                $scope.errors = {};

                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Inscription est réussie!')
                        .position('top right')
                        .hideDelay(3000)
                );
                $rootScope.openPopup('views/popups/sign-up-success.html');

                if (type === 'social') {

                    $scope.credentials = {
                        email: $scope.data.email,
                        password: $scope.data.password
                    };

                    $timeout(function () {
                        $scope.login($scope.credentials);
                    }, 1000);
                }

            }, function (response) {
                $scope.errors = response.data;

            });
        };

        /**
         * SOCIAL AUTH
         */


        //    Facebook Login
        $rootScope.fblogin = function (type) {

            //Example with Facebook
            OAuth.initialize('q1Moul-B6vvRwIigX-0nU7dWK4U');
            OAuth.popup('facebook').done(function (result) {
                result.me().done(function (data) {
                    var fbData = data;
                    fbData.file = "https://graph.facebook.com/" + fbData.id + "/picture?width=400&height=400";
                    fbData.type = type;
                    AuthService.facebookLogin(fbData).then(function (res) {

                        if (res.auth_type === 'sign_up') {
                            $scope.data = {
                                first_name: fbData.firstname,
                                last_name: fbData.lastname,
                                email: (fbData.id + '@fb.com').toLowerCase(),
                                dob: moment().format('YYYY:MM:DD HH:MM:SS'),
                                password: fbData.id,
                                fb_id: fbData.id,
                                address: geoplugin_countryName() ? geoplugin_city() + ', ' + geoplugin_countryName() : 'no selected',
                                avatar: fbData.file,
                                is_organizer: 2,
                                is_subscribed: 'no',
                            };
                            $scope.saveUser('social');
                        } else if (res.auth_type === 'log_in') {
                            $scope.credentials = {
                                email: (fbData.id + '@fb.com').toLowerCase(),
                                password: fbData.id
                            };

                            $scope.login($scope.credentials);
                        }

                        if (res.hasOwnProperty('errors')) {
                            $scope.errors = res.errors;
                            $scope.isValid = ' error-login';
                        }
                    });

                });
            }).fail(function (err) {
                //todo when the OAuth flow failed
            });

        };

        $rootScope.fblogout = function () {
            FB.getLoginStatus(function (response) {
                if (response && response.status === 'connected') {
                    FB.logout(function (response) {
                        console.log("Vous n'est pas connectés dans facebook");
                    });
                }
            });
        };


        $scope.tweets; //array of tweets

        twitterService.initialize();

        //using the OAuth authorization result get the latest 20 tweets from twitter for the user
        $scope.refreshTimeline = function () {
            twitterService.getLatestTweets().then(function (data) {
                $scope.tweets = data;
            });
        };

        //when the user clicks the connect twitter button, the popup authorization window opens
        $scope.twLogin = function (type) {
            twitterService.connectTwitter().then(function () {
                if (twitterService.isReady()) {
                    //if the authorization is successful, hide the connect button and display the tweets
                    twitterService.getTwUser().then(function (data) {

                        $scope.authData = data;

                        var twData =
                        {
                            id: data.id,
                            first_name: data.name,
                            file: data.profile_image_url_https,
                            type: type,
                        };


                        AuthService.twitterLogin(twData, $scope.authData).then(function (res) {

                            if (res.auth_type === 'sign_up') {
                                $scope.data = {
                                    first_name: $scope.authData.name.split(" ").map(String)[0],
                                    last_name: $scope.authData.name.split(" ").map(String)[1],
                                    email: ($scope.authData.alias + '@twitter.com').toLowerCase(),
                                    dob: moment().format('YYYY:MM:DD HH:MM:SS'),
                                    password: $scope.authData.id,
                                    twitter_id: $scope.authData.id,
                                    address: $scope.authData.location,
                                    avatar: $scope.authData.raw.profile_image_url_https.replace('_normal', ''),
                                    is_organizer: 2,
                                    is_subscribed: 'no',
                                };
                                $scope.saveUser('social');
                            } else {
                                $scope.credentials = {
                                    email: ($scope.authData.alias + '@twitter.com').toLowerCase(),
                                    password: $scope.authData.id
                                };

                                $scope.login($scope.credentials);
                            }
                        });
                    });
                }
            });
        };

        //sign out clears the OAuth cache, the user will have to reauthenticate when returning
        $rootScope.signOutTwitter = function () {
            twitterService.clearCache();
            $scope.tweets.length = 0;
        };

        //if the user is a returning user, hide the sign in button and display the tweets
        if (twitterService.isReady()) {
            $('#connectButton').hide();
            $('#getTimelineButton, #signOut').show();
            $scope.refreshTimeline();
        }
    });
