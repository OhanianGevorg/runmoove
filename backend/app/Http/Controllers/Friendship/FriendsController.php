<?php

namespace App\Http\Controllers\Friendship;

use App\Http\Requests\Friendship\BlockFriendRequest;
use App\Http\Requests\Friendship\BlockUserRequest;
use App\Http\Requests\Friendship\FriendsReadRequest;
use App\Models\Friendship\BlockedUser;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FriendsController extends Controller
{
    /**
     * @param FriendsReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(FriendsReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $friends Collection
         */
//        $friends = User::auth()->getFriendsQueryBuilder()->getAll($request->all());
        $friends = User::where('id', '<>', User::auth()->getKey())->getAll($request->all());
        return response()->json([
            'friends' => UserForFriendsTransformer::transformCollection($friends)
        ], 200);
    }

    public function show($id)
    {

    }

    public function block(BlockUserRequest $request)
    {
        if (!User::auth() || !$request->has('user_id')) {
            return response()->json([
                'error' => "you have not permission"
            ], 403);
        }
        if (User::auth()->is_blocked($request->get('user_id'))) {
            return response()->json([
                'error' => 'You have already blocked this user'
            ], 403);
        }
        try {
            $blocked_user = new BlockedUser([
                'blocker_id' => User::auth()->getKey(),
                'blocked_id' => $request->get('user_id')
            ]);
            $blocked_user->save();
            return response()->json([
                'blocked_user' => UserForFriendsTransformer::transform(User::find($request->get('user_id')))
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }

    }

    public function unblock(BlockUserRequest $request)
    {
        if (!User::auth() || !$request->has('user_id')) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        if (!User::auth()->is_blocked($request->get('user_id'))) {
            return response()->json([
                'error' => 'You have not yet block this user'
            ], 403);
        }
        try {
            $block = User::auth()->getBlockedUser($request->get('user_id'));
            $block->delete();
            return response()->json([
                'success' => 'You have unblock'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }

    }

    public function deleteFriend(BlockUserRequest $request)
    {
        if (!User::auth() || !$request->has('user_id')) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        if (!User::auth()->hasFriend($request->get('user_id'))) {
            return response()->json([
                'error' => 'You are not friends'
            ], 403);
        }

        try {
            User::auth()->getFriendListByFriend($request->get('user_id'))->first()->delete();
            return response()->json([
                'success' => 'Successfully deleted'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function friendlist()
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission!'
            ], 403);
        }
        $friends = User::auth()->getFriendsQueryBuilder()->getAll();
        return response()->json([

            'friends' => UserForFriendsTransformer::transformCollection($friends)
        ], 200);
    }
}
