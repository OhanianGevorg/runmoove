'use strict';

describe('Controller: PagesRunNewCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesRunNewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunNewCtrl = $controller('PagesRunNewCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunNewCtrl.awesomeThings.length).toBe(3);
  });
});
