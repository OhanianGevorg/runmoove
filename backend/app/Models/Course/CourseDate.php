<?php

namespace App\Models\Course;

use App\Models\Opinion\Opinion;
use App\Models\QuestionAndAnswer\Question;
use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseDate extends Model
{

    use SoftDeletes, FilterTrait;

    protected $table = 'course_dates';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_owner_id',
        'course_id',
        'start_at',
        'end_at',
        'official_organizer_id',
        'created_at',
        'updated_at'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'course_owner_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_opinion()
    {
        return $this->hasMany(Opinion::class, 'course_date_id');
    }

    /**
     * @return array
     */
    public function getStarsCount()
    {
        $countableFields = Opinion::$countableFields;

        $totals = [];

        foreach ($countableFields as $field) {
            $totals[$field] = 0;
        }

        $reviewScoresFields = $this->course_opinion()->select($countableFields)->get();

        if (!count($reviewScoresFields)) {
            return $totals;
        }

        foreach ($reviewScoresFields as $key => $item) {
            foreach ($countableFields as $field) {
                $totals[$field] = $totals[$field] + $item->attributes[$field];
            }
        }
        return array_map(function ($field) use ($reviewScoresFields) {
            return round($field / count($reviewScoresFields));
        }, $totals);
    }

    /**
     * @return mixed
     */
    public function getReviewCount()
    {
        return $this->course_opinion->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course_participants()
    {
        return $this->hasMany(CourseParticipation::class, 'course_date_id');
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getParticipant(User $user)
    {
        return $this->course_participants()->where('user_id', '=', $user->getKey())->firstOrFail();
    }

    /**
     * @return mixed
     */
    public function get_participants()
    {
        return $this->course_participants()->getResults()->map(function (CourseParticipation $relation) {
            return $relation->user;
        });
    }

    /**
     * @desc check if participation exists
     * @param User $user
     * @return bool
     */
    public function checkParticipation(User $user)
    {
        return !is_null($this->course_participants()->where('user_id', '=', $user->getKey())->first());
    }

    /**
     * @return int
     */
    public function participants_count()
    {
        return (int)count($this->get_participants());
    }

    /**
     * @return mixed
     */
    public function get_participants_query()
    {
        return User::select('users.*')
            ->join('course_participation', 'users.id', '=', 'course_participation.user_id')
            ->where('course_participation.course_date_id', '=', $this->getKey());
    }

    /**
     * @param $value
     * @return mixed
     */
    public function get_course_field($value)
    {
        return $this->course()->select($value)->get();
    }

    /**
     * @desc return to official organizer relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function official_organizer()
    {
        return $this->belongsTo(User::class, 'official_organizer_id');
    }

    /**
     * @return int|mixed
     */
    public function getOfficialOrganizerId()
    {
        return is_null($this->official_organizer_id) ? 0 : $this->official_organizer_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(Question::class, 'course_date_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function apiResponses()
    {
        return $this->hasMany(TravelApi::class, 'course_date_id');
    }

    public function getAPIResponses()
    {
        $apiDataCollection = new Collection();
        /**
         * @var $data Collection
         */
        $data = $this->apiResponses;
        if (count($data) === 0) {
            return $data;
        }
        $data->contains(function ($value) use ($apiDataCollection) {
            $nullCount = 0;
            /**
             * @var $value Model
             */
            foreach ($value->getAttributes() as $attribute) {
                if ($attribute === null || $attribute === 0) {
                    $nullCount += 1;
                }
            }
            if ($nullCount === 0) {
                $apiDataCollection->add($value);
            }
        });
        return $apiDataCollection;
    }
}
