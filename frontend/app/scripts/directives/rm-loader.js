'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:rmLoader
 * @description
 * # rmLoader
 */
angular.module('runMoovApp')
  .directive('rmLoader', function ($window) {
    return {
      templateUrl:'views/tools/rm-loader.html',
      restrict: 'E',
      scope: {
        data:'=',
        type:'='
      },
      link: function link(scope, element, attrs) {

        $(element).css({
          'z-index':999999,
        });

        if (scope.type !== 'inner') {
          $(element).find('.loader-item').css({
            top:$window.innerHeight/2,
            left:$window.innerWidth/2,
            position:'fixed'
          });
        }  else {
          $(element).find('.loader-item').css({

            position:'relative'
          });
        }

      }
    };
  });
