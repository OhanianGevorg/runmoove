<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('course_opinion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('course_date_id')->unsigned();
            $table->tinyInteger('parcours_score');
            $table->tinyInteger('ambiance_score');
            $table->tinyInteger('dotation_score');
            $table->tinyInteger('ravitaillement_score');
            $table->tinyInteger('organisation_score');
            $table->tinyInteger('difficult_score');
            $table->string('title');
            $table->string('participation_purpose');
            $table->string('participate_again');
            $table->time('course_finish_time');
            $table->mediumText('body');
            $table->mediumText('best_memories');
            $table->mediumText('place_recommendations');
            $table->mediumText('suggestions');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('course_date_id')
                ->references('id')
                ->on('course_dates')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('course_opinion');
    }
}
