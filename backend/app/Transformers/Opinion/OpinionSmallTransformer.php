<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/28/17
 * Time: 10:39 AM
 */

namespace App\Transformers\Opinion;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Transformers\CourseTransformers\CourseSmallTransformer;

class OpinionSmallTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'user_id' => $model->user_id,
            'images' => OpinionImageTransformer::transformCollection($model->images),
            'parcour_score' => $model->parcour_score,
            'ambiance_score' => $model->ambiance_score,
            'dotation_score' => $model->dotation_score,
            'revitaillement_score' => $model->revitaillement_score,
            'organisation_score' => $model->organisation_score,
            'difficult_score' => $model->difficult_score,
            'total' => $model->countScoreTotal(),
            'course_date'=>CourseSmallTransformer::transform($model->course_date),
            'created_at'=>$model->created_at
        ];
    }
}