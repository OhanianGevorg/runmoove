'use strict';

describe('Service: courseParticipation', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var courseParticipation;
  beforeEach(inject(function (_courseParticipation_) {
    courseParticipation = _courseParticipation_;
  }));

  it('should do something', function () {
    expect(!!courseParticipation).toBe(true);
  });

});
