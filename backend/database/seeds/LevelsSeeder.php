<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private static $levels = [
        [
            'name' => 'baby_runner',
            'real_name' => 'Baby Runner',
            'img_name' => null,
            'up_score' => 3,
            'down_score' => 0
        ],
        [
            'name' => 'runner',
            'real_name' => 'Runner',
            'img_name' => null,
            'up_score' => 5,
            'down_score' => 3
        ],
        [
            'name' => 'serial_runner',
            'real_name' => 'Serial Runner',
            'img_name' => null,
            'up_score' => 10,
            'down_score' => 5
        ],
        [
            'name' => 'runner_killer',
            'real_name' => 'Runner Killer',
            'img_name' => null,
            'up_score' => 20,
            'down_score' => 10
        ],
        [
            'name' => 'run_expert',
            'real_name' => 'Run Expert',
            'img_name' => null,
            'up_score' => 50,
            'down_score' => 20
        ],
        [
            'name' => 'run_master',
            'real_name' => 'Run Master',
            'img_name' => null,
            'up_score' => null,
            'down_score' => 50
        ],
    ];

    public function run()
    {
        //
        App\Models\SystemLevels::insert(self::$levels);
    }
}
