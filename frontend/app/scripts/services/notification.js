'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.notifications
 * @description
 * # notifications
 * Factory in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('Notification', function (ENV,$resource) {
    return $resource(ENV.apiEndpoint + '/notifications', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      get:{
        method:'GET'
      },
      setStatus:{
        method:'PUT'
      }
    });
  });
