'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PagesUserProfileCtrl
 * @description
 * # PagesUserProfileCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
  .controller('ProfileCtrl', function ($scope,userRecords,$rootScope,$stateParams,Runs, $state, AuthService, Users,friendShip,Friends) {

      $scope.authUserID = AuthService.userID();
      $scope.userId = $stateParams.id;
      $scope.frRequest = false;
      $scope.profileIsActive = false;

      Users.show({id:$scope.userId,user_id:$scope.userId},function (res) {
        $scope.user = res.user;
        $scope.profileIsActive = true;
        angular.forEach($scope.user.friend_requests,function (usr) {

          if (usr.sender === $scope.authUserID) {
            $scope.frRequest = true;
          };
        });

        $scope.album = [];

        /**
         * get user all opinions images and push in album
         */
        angular.forEach(res.user.opinions,function (item) {
          angular.forEach(item.images,function (op_img) {
            $scope.album.push(op_img);
          });
        });

        /**
         * get  user all runs images and push in album
         */
        angular.forEach(res.user.created_courses,function (item) {
          angular.forEach(item.course.course_images,function (run_img) {
              $scope.album.push(run_img);
          });
        });

        $scope.user_runs = res.user.created_courses;
        // Runs.query({user_id:$scope.userId}, function (res) {
        //   $scope.runs = res.data;
        //   console.log($scope.runs);
        // });

        userRecords.query({user_id:$scope.userId},function (res) {
          $scope.records = res.records;
        });
      });

      $scope.user = AuthService;

      // Users.get({id: $scope.userID}, function (res) {
      //   console.log(res);
      // })

    $rootScope.friendRequest = function (id) {
      friendShip.store({recipient_id: id}, function (res) {
        if (res.hasOwnProperty('success')) {
          $scope.frRequest = true;
        }
      });
    };


    $rootScope.removeFromFriends = function (user_id) {
      Friends.delete({user_id:user_id},function (res) {
        $scope.user.is_friend = false;
        $scope.frRequest = false;
      });
    };

  });
