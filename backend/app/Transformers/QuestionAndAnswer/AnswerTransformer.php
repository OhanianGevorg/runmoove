<?php
namespace App\Transformers\QuestionAndAnswer;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\QuestionAndAnswer\Answer;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserXsTransformer;

class AnswerTransformer implements TransformerInterface, PrepareInterface
{
    use PrepareCollection, TransformCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Answer([
            'user_id' => array_get($injector, 'user_id'),
            'question_id' => array_get($raw, 'question_id'),
            'body' => array_get($raw, 'body')
        ]);
    }

    public static function update(array $raw, array $injector = [])
    {
        return [
            'question_id' => array_get($raw, 'question_id'),
            'body' => array_get($raw, 'body')
        ];
    }

    public static function transform($model)
    {
        /**
         * @var $model Answer
         */
        if (!$model) {
            return [];
        }
        return [
            'user_id' => $model->user_id,
            'user' => UserXsTransformer::transform($model->answering),
            'question_id' => $model->question_id,
            'body' => $model->body,
            'created_at' => $model->created_at
        ];
    }
}