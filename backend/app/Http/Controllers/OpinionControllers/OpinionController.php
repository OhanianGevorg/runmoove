<?php

namespace App\Http\Controllers\OpinionControllers;

use App\Http\Requests\OpinionRequests\OpinionReadRequest;
use App\Http\Requests\OpinionRequests\OpinionStoreRequest;
use App\Models\Course\CourseDate;
use App\Models\Opinion\CourseOpinionFormat;
use App\Models\Opinion\Opinion;
use App\Transformers\Opinion\CourseOpinionTransformer;
use App\Transformers\Opinion\OpinionImageTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OpinionController extends Controller
{
    //
    public function index(OpinionReadRequest $request)
    {
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'data is incorrect'
            ], 422);
        }

        $course_date = CourseDate::find($request->get('course_date_id'));
        if (!$course_date) {
            return response()->json([
                'error' => 'This course does not exists'
            ], 404);
        }

        try {
            $course_opinion = $course_date->course_opinion()->getAll();
            return response()->json([
                'course_opinions' => CourseOpinionTransformer::transformCollection($course_opinion),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }

    }

    public function store(OpinionStoreRequest $request)
    {
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'Wrong input data'
            ], 422);
        }

        $course_date = CourseDate::find($request->get('course_date_id'));
        if (!User::auth() || !$course_date) {
            return response()->json([
                'error' => 'User or course does not exists'
            ], 404);
        }


        if(Opinion::where('user_id','=',User::auth()->getKey())->where('course_date_id','=',$course_date->getKey())->count()){
            return response()->json([
                'error'=>'You have already create opinion for this run'
            ],403);
        }

        DB::beginTransaction();
        try {
            $course_opinion = CourseOpinionTransformer::prepare($request->all(), ['user_id' => User::auth()->getKey()]);
            $course_opinion->save();

            if ($request->has('course_opinion_formats')) {
                CourseOpinionFormat::insert(array_map(function ($format) use ($course_opinion) {
                    return [
                        'format_id' => $format,
                        'course_opinion_id' => $course_opinion->getKey(),
                        'created_at' => Carbon::now()
                    ];
                },
                    explode(',', $request->get('course_opinion_formats'))));
            }

            if ($request->has('opinion_images')) {
                $course_opinion->images()
                    ->saveMany(OpinionImageTransformer::prepareCollection($request->get('opinion_images'), ['course_opinion_id' => $course_opinion->getKey()]));
            }

            DB::commit();
            return response()->json([
                'success' => 'Votre avis a cré avec succès',
                'data' => CourseOpinionTransformer::transform($course_opinion)
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
