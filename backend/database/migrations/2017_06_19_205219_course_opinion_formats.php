<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseOpinionFormats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('course_opinion_formats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_opinion_id')->unsigned();
            $table->integer('format_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('course_opinion_id')
                ->references('id')
                ->on('course_opinion')
                ->onUpdate('no action')
                ->onDelete('no action');

            $table->foreign('format_id')
                ->references('id')
                ->on('system_course_formats')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('course_opinion_formats');
    }
}
