<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class AviaBookingRequest extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search_id' => 'required|max:255',
            'term_url' => 'required|max:255'
        ];
    }
}
