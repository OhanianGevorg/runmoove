<?php

namespace App\Http\Controllers\Shared;

use App\Models\SystemConstants\SystemConstants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticFieldsController extends Controller
{
    //
    public function getAllFields()
    {
        return response()->json([
            'system_types' => SystemConstants::getAll()
        ], 200);
    }

    public function getCourseFields()
    {
        return response()->json([
            'system_types' => SystemConstants::getForCourse()
        ]);
    }

    public function getOpinionFields()
    {
        return response()->json([
            'system_types' => SystemConstants::getForOpinion()
        ], 200);
    }
}
