<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class APICallRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_date_id' => 'required|integer|exists:course_dates,id',
            'origin_iata' => 'required|string|max:255',
            'destination_iata' => 'required|string|max:255',
        ];
    }
}
