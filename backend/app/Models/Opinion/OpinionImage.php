<?php

namespace App\Models\Opinion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpinionImage extends Model
{
    //
    use SoftDeletes;

    protected $table = 'opinion_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_opinion_id',
        'img_url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course_opinion()
    {
        return $this->belongsTo(Opinion::class, 'course_opinion_id');
    }
}
