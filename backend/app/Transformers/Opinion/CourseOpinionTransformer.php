<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/20/2017
 * Time: 12:33 AM
 */

namespace App\Transformers\Opinion;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Opinion\Opinion;
use App\Traits\TransformCollection;
use App\Transformers\CourseTransformers\CourseDateTransformer;
use App\Transformers\CourseTransformers\CourseSmallTransformer;
use App\Transformers\Shared\StaticTransformer;
use App\Transformers\UserTransformer;
use Carbon\Carbon;

class CourseOpinionTransformer implements TransformerInterface, PrepareInterface
{

    use TransformCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Opinion([
            'user_id' => $injector['user_id'],
            'course_date_id' => array_get($raw, 'course_date_id'),
            'parcour_score' => array_get($raw, 'parcour_score'),
            'ambiance_score' => array_get($raw, 'ambiance_score'),
            'dotation_score' => array_get($raw, 'dotation_score'),
            'revitaillement_score' => array_get($raw, 'revitaillement_score'),
            'organisation_score' => array_get($raw, 'organisation_score'),
            'difficult_score' => array_get($raw, 'difficult_score'),
            'title' => array_get($raw, 'title'),
            'participation_purpose' => array_get($raw, 'participation_purpose'),
            'participate_again' => array_get($raw, 'participate_again'),
            'course_finish_time' => array_get($raw, 'course_finish_time') ? Carbon::parse(array_get($raw, 'course_finish_time'))->toTimeString() : array_get($raw, 'course_finish_time'),
            'body' => array_get($raw, 'body'),
            'best_memories' => array_get($raw, 'best_memories'),
            'place_recommendations' => array_get($raw, 'place_recommendations'),
            'suggestions' => array_get($raw, 'suggestions'),
        ]);
    }

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id'=>$model->getKey(),
            'user' => UserTransformer::smallTransformer($model->user),
            'user_id' => $model->user_id,
            'course_date_id' => CourseSmallTransformer::transform($model->course_date),
            'parcour_score' => $model->parcour_score,
            'ambiance_score' => $model->ambiance_score,
            'dotation_score' => $model->dotation_score,
            'revitaillement_score' => $model->revitaillement_score,
            'organisation_score' => $model->organisation_score,
            'difficult_score' => $model->difficult_score,
            'title' => $model->title,
            'participation_purpose' => $model->participation_purpose,
            'participate_again' => $model->participate_again,
            'course_finish_time' => $model->course_finish_time,
            'body' => $model->body,
            'best_memories' => $model->best_memories,
            'place_recommendations' => $model->place_recommendations,
            'suggestions' => $model->suggestions,
            'opinion_formats' => StaticTransformer::transformCollection($model->get_opinion_formats()),
            'images' => OpinionImageTransformer::transformCollection($model->images),
            'total' => $model->countScoreTotal(),
            'created_at'=>$model->created_at
        ];
    }
}