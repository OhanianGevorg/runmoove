'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PusherChatctrlCtrl
 * @description
 * # PusherChatctrlCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('ChatCtrl', function ($scope,Chat,$rootScope,ENV,Friends,$mdToast,$localStorage, $timeout, Notification) {

    $scope.sendingMsg = false;

    var msgIds = [];


    $timeout(function () {
      angular.forEach($rootScope.getActiveMessages,function (msg) {
        msgIds.push(msg.id);
      });

      if ($rootScope.getActiveMessages.length > 0) {
        Notification.setStatus({notification_ids:msgIds},function (res) {
          $rootScope.getActiveMessages = [];
        });
      }
    },2000);

    /**
     * get all rooms
     */
    Chat.get(function (res) {
      $scope.rooms = res.rooms;

      angular.forEach($scope.rooms,function (room) {
        angular.forEach(room.participants,function (user) {
          if (typeof user.user_id === 'object') {
            room.participate = user.user_id;
          }
        });
      });

      $timeout(function () {
        $scope.getRoomChat($scope.rooms[0].id);
      },400);
    });


    $scope.scrollChatListToBottom = function () {
      $('.message-list-box[rm-scroll]').mCustomScrollbar("update",{scrollInertia:0});
      $('.message-list-box[rm-scroll]').mCustomScrollbar("scrollTo", ".message:last-child");
    };



    $scope.getRoomChat = function (id) {
      var msgIds = [];
      $rootScope.messages = [];
      $('.message-list-box[rm-scroll]').mCustomScrollbar({scrollInertia:0,setHeight: '100%'});
      $('.message-list-box[rm-scroll]').mCustomScrollbar("update");


      $timeout(function () {
        Chat.roomChat({room_id:id},function (res) {
          $rootScope.messages = res.messages;

          angular.forEach($rootScope.messages, function (msg) {
            msgIds.push(msg.id);
          });

          angular.forEach(res.room.participants,function (user) {
            if (typeof user.user_id === 'object') {
              $scope.recipient = user.user_id;
            }
          });

          /**
           * set read status for room
           */
          Chat.setMessagesRead({message_ids:msgIds},function (res) {
            angular.forEach($scope.rooms,function (room) {
              if (room.id === id) {
                room.unread_messages = 0;
              }
            });
          });
          $timeout(function () { $scope.scrollChatListToBottom();},1500);
        });
      },1500);



    };



    $scope.data = {
      recipient_id:'',
      body:'',
    };

    $scope.sendFromRoom = function (recipient_id,e) {
      $scope.openSmileBox();

      if ($scope.data.body === '' ) {
        angular.element('.emojionearea').addClass('shake-error');
        setTimeout(function () {
          angular.element('.emojionearea').removeClass('shake-error');
        },800);
        return;
      }

      $scope.sendingMsg = true;
      $scope.data.recipient_id = recipient_id;

      Chat.save($scope.data,function (res) {
        $rootScope.messages.push(res.message);
        $scope.sendingMsg = false;
        $scope.data.body = '';
        angular.element('.emojionearea-editor').html('');
        $scope.scrollChatListToBottom();
        $rootScope.closeAllPopups();
      },function (err) {
        if (err.data.hasOwnProperty('error')) {
          toastr.error(err.data.error, 'Chat Error');
        }
      });
    };


    $scope.checkIsEmpty = function (e) {
      if ($scope.data.body !== '' ) {
        angular.element('.sendMsgBtn').removeAttr('disabled');
      }
    };

    $scope.openSmileBox = function () {
      $('.emojionearea-button').click();
    };



    $scope.blockUser = function (recipient_id) {
      Friends.block({user_id:recipient_id},function (res) {
        $mdToast.show(
          $mdToast.simple()
            .textContent('This User is Blocked!')
            .position('top right')
            .hideDelay(3000)
        );
      });
    };


    /*************************************************************************************
                             *  DROPZONE CODE SECTION *
    ***************************************************************************************/



    $scope.InProgress = false;


//  main slider



//Set options for dropzone
//Visit http://www.dropzonejs.com/#configuration-options for more options
$scope.dzOptions = {
  url: ENV.apiEndpoint + '/upload',
  paramName: 'photo',
  maxFilesize: '30',
  acceptedFiles: 'image/jpeg, images/jpg, image/png',
  addRemoveLinks: true,
  dictRemoveFile: "",
  dictCancelUpload: "",
  clickable: '#attachFileBtn',
  parallelUploads: 1,
  autoProcessQueue: true,
  maxFiles:1,
  uploadMultiple:false,

};

//Handle events for dropzone
//Visit http://www.dropzonejs.com/#events for more events
$scope.dzCallbacks = {

  addedfile: function (file) {

  },
  success: function (file, xhr) {
    if (xhr.hasOwnProperty('image')) {
      $scope.sendingMsg = true;

      var fileMsg = '<a data-fancybox class="msgImg" ng-href="'+ ENV.apiEndpoint +'/storage/' + xhr.image + '"><img src="'+ ENV.apiEndpoint +'/storage/' + xhr.image + '" alt=""></a>';

      $scope.data.body = fileMsg;

      $scope.sendFromRoom($scope.recipient.id);
    }

  },
  error: function (file) {
    $scope.newFile.length = 0;
  },

  removedfile: function (file) {
    var image = JSON.parse(file.xhr.response);
    var index;
    for (var i = 0;i<$scope.data.course_images.length;i++){
      if($scope.data.course_images[i].url==image.image){
        index = i;
        break;
      }
    }
    $scope.data.course_images.splice(index, 1);
  },
  uploadprogress: function (file, progress) {
    file.progress = progress;
    if(progress===100){
      $scope.InProgress = true;
    }
  },
  sending: function(file,xhr) {
    return xhr.setRequestHeader('X-TOKEN',$localStorage.api_token);
  }
};


  });
