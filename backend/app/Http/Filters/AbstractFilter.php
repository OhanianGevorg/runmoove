<?php
namespace App\Http\Filters;

class AbstractFilter {

	public function __construct($query)
	{
		$this->query = $query;
	}

	public function _callFilter($filter, $value = '')
	{
		if(in_array($filter, $this->filter)) {
			$this->{'filterBy' . ucfirst($filter)}($value);
		}
		
	}

	public function _callSorter($filter, $value = '')
	{
		if(in_array($filter, $this->order)) {
			$this->{'sortBy' . ucfirst($filter)}($value);
		}
		
	}
}