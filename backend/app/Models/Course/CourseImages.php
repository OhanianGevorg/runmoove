<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseImages extends Model
{
    use SoftDeletes;

    protected $table = 'course_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_id',
        'img_url',
        'type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }
}
