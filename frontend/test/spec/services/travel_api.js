'use strict';

describe('Service: travelApi', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var travelApi;
  beforeEach(inject(function (_travelApi_) {
    travelApi = _travelApi_;
  }));

  it('should do something', function () {
    expect(!!travelApi).toBe(true);
  });

});
