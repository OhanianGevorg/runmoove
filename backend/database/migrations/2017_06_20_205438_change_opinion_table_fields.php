<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOpinionTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('course_opinion', function (Blueprint $table) {
            $table->dropColumn('parcours_score');
            $table->dropColumn('ravitaillement_score');
        });
        Schema::table('course_opinion', function (Blueprint $table) {
            $table->tinyInteger('parcour_score');
            $table->tinyInteger('revitaillement_score');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_opinion', function (Blueprint $table) {
            $table->dropColumn('parcour_score');
            $table->dropColumn('revitaillement_score');
        });
        Schema::table('course_opinion', function (Blueprint $table) {
            $table->tinyInteger('parcours_score');
            $table->tinyInteger('ravitaillement_score');
        });
    }
}
