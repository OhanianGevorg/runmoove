<?php

namespace App\Http\Requests\Article;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth()) && User::auth()->canWriteArticle();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $custom_rules = [];
        if ($this->has('images')) {
            foreach ($this->get('images') as $index => $image) {
                $custom_rules['images.' . $index . '.file_name'] = 'file_exists:storage|required_with:images.'.($index);
            }
        }
        if ($this->has('videos')) {
            foreach ($this->get('videos') as $index => $video) {
                $custom_rules['videos.' . $index . '.url'] = array('required_with:videos.'.($index), 'max:255', 'regex:/^http(s)?:\/\/(www\.)?(youtube\.com|youtu\.be).+$/');
            }
        }
        return array_merge([
            'title' => 'required|string|max:255',
            'body' => 'required|string|max:65000',
            'category' => 'required|integer|exists:categories,id',
            'images' => 'array',
            'videos' => 'array'
        ],$custom_rules);
    }
}
