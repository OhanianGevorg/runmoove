<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/7/2017
 * Time: 12:23 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Course\CourseMarkers;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class CourseMarkerTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'course_id' => $model->course_id,
            'latitude' => $model->latitude,
            'longitude' => $model->longitude,
            'marker_type' => $model->marker_type,
            'coords' => htmlspecialchars_decode(htmlspecialchars_decode($model->coords, ENT_QUOTES), ENT_QUOTES),
            'gpx_url' => $model->gpx_url
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new CourseMarkers([
            'course_id' => $injector['course_id'],
            'latitude' => array_get($raw, 'latitude'),
            'longitude' => array_get($raw, 'longitude'),
            'marker_type' => array_get($raw, 'marker_type'),
            'coords' => array_get($raw, 'coords'),
            'gpx_url' => array_get($raw, 'gpx_url')
        ]);
    }
}