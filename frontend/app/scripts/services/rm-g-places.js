'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.rmGPlaces
 * @description
 * # rmGPlaces
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('rmGPlaces', function ($q) {

    var map;
    var service;
    var pyrmont;
    var test;

    return {
      getPlaces: function (coords, types, radius,callback) {

        var Lat = parseFloat(coords[0]);
        var Lng = parseFloat(coords[1]);

        pyrmont = new google.maps.LatLng(Lat, Lng);

        // console.log(new google.maps.LatLng(-33.8665433,151.1956316));
        map = new google.maps.Map(document.getElementById('fakeMap'), {
          center: pyrmont,
        });

        var request = {
          location: pyrmont,
          radius: radius,
          type: types
        };

        service = new google.maps.places.PlacesService(map);
          service.nearbySearch(request, function (results, status) {
            return  results;
        });

      }
    }
  });
