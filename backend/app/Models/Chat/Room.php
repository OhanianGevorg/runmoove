<?php

namespace App\Models\Chat;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    //
    use SoftDeletes;

    protected $table = 'rooms';
    protected $primaryKey = 'id';
    protected $fillable = [
        'creator_id'
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'participants', 'room_id', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'room_id');
    }

    public function participants()
    {
        return $this->hasMany(Participant::class, 'room_id');
    }
}
