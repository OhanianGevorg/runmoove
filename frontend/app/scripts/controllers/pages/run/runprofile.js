'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PagesRunRunprofileCtrl
 * @description
 * # PagesRunRunprofileCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
    .controller('RunProfileCtrl', function ($scope, $rootScope, $stateParams, Runs, $window, Opinions, courseParticipation, AuthService, QA, Answers, $http, travelApi,$timeout) {
        $scope.runID = $stateParams.id;
        $scope.is_participating = false;
        $scope.profileIsActive = false;
        $scope.msgBoxIsOpen = false;
        $scope.questionAndAnswers = [];
        $scope.currentQpage = 1;
        $scope.location = {
            'latitude': '',
            'longitude': ''
        };
        $scope.start = {};
        $scope.coords = '';
        $scope.questionAndAnswers = [];

        Runs.show({id: $scope.runID}, function (res) {
            $scope.run = res.course_date;
            $scope.profileIsActive = true;
            $scope.location['latitude'] = $scope.run.course.latitude;
            $scope.location['longitude'] = $scope.run.course.longitude;
            $scope.coords = $scope.run.course.course_markers[0] ? $scope.run.course.course_markers[0].coords : '';



            var token = '7d4bddfe4ce17c56ed7f1205a8117f3f';



            $http.get('scripts/data/cities_with_airname.json').then(function (res) {
              var cityCode = [];
              var userCityCode = [];

              angular.forEach(res.data.data,function (n) {
                if (n.coordinates !== null && n.coordinates !== undefined
                  && n.coordinates.lat.toString().substr(0,2) == $scope.run.course.latitude.toString().substr(0,2)
                  && n.coordinates.lon.toString().substr(0,2) == $scope.run.course.longitude.toString().substr(0,2)) {
                  cityCode.push(n);
                }
                if (n.coordinates !== null && n.coordinates !== undefined
                  && n.coordinates.lat.toString().substr(0, 2) == AuthService.currentLat().toString().substr(0, 2)
                  && n.coordinates.lon.toString().substr(0, 2) == AuthService.currentLng().toString().substr(0, 2)) {
                  userCityCode.push(n);
                }
              });



              Runs.travelAPI({course_date_id: $scope.run.id,origin_iata:userCityCode[0].code,destination_iata:cityCode[0].code}, function (res) {
                $scope.getTravelHotels();
                // $scope.getTravelAvia();
              });

              $scope.getTravelHotels = function () {
                travelApi.get({course_date_id:$scope.runID},function (res) {



                  if (res.data.length > 0) {
                    angular.forEach(res.data,function (item) {
                      if (item.type == 1) {
                        $scope.travelHotels = JSON.parse(item.body);
                      } else if (item.type == 2) {
                        $scope.travelAvia = JSON.parse(JSON.parse(item.body));
                      }
                    });
                    console.log($scope.travelHotels);
                    console.log($scope.travelAvia);
                    clearInterval($scope.hotelInterval);
                  } else {
                    $scope.getHotelsInterval();
                  }
                });
              };




              $scope.getAviaInfo = function (avia) {
                return avia[Object.keys(avia)[0]];
              };


              $scope.getAviaBookingUrl = function (avia) {
                if ($scope.getAviaInfo(avia['gates_info']).hasOwnProperty('terms')) {
                  return travelApi.getAvia({search_id:avia.search_id,terms_url:avia.terms.url},function (res) {
                    return res.data;
                  });
                }
              };



              $scope.getHotelsInterval = function () {
                $scope.hotelInterval = setInterval(function () {
                  $scope.getTravelHotels();
                },30000);
              };


              $scope.getAviaInterval = function () {
                $scope.aviaInterval = setInterval(function () {
                  $scope.getTravelAvia();
                },30000);
              };

              var aviaUrl = 'https://api.travelpayouts.com/v2/prices/latest?currency=eur&period_type=year&page=1&origin='+ userCityCode[0].code +'&destination=' + cityCode[0].code + '&limit=30&show_to_affiliates=true&sorting=price&trip_class=0&token=7d4bddfe4ce17c56ed7f1205a8117f3f';

              Runs.avia({url: aviaUrl}, function (res) {
                // console.log(JSON.parse(res.data).data);
                $scope.aviaTickets = JSON.parse(res.data).data;

                $http.get('scripts/data/airports.json').then(function (air) {
                  console.log(air);

                  angular.forEach($scope.aviaTickets,function (item) {
                    angular.forEach(air.data,function (n) {
                      if (n.city_code == item.destination) {
                       item.air_name = n.name;
                      }
                    })
                  })
                })



              });




              // TODO move to service
              $scope.getPlaces = function () {
                var pyrmont = new google.maps.LatLng(parseFloat($scope.run.course.latitude), parseFloat($scope.run.course.longitude));

                // console.log(new google.maps.LatLng(-33.8665433,151.1956316));
                var map = new google.maps.Map(document.getElementById('fakeMap'), {
                  center: pyrmont,
                });

                var request = {
                  location: pyrmont,
                  radius: '500',
                  type: ['lodging']
                };

                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, function (results, status) {
                  $scope.nearbyHotels = results;
                  console.log($scope.nearbyHotels);
                });
              }

              // console.log($scope.nearbyHotels);
            $scope.getPlaces();

            })

            angular.forEach($scope.run.course_participants, function (partis) {
                if (partis.id == AuthService.userID()) {
                    $scope.is_participating = true;
                }
            });
            $scope.start.init($scope.location, $scope.coords);
        });

        Opinions.query({course_date_id: $scope.runID}, function (res) {
            $scope.opinions = res.course_opinions;
        });

        $scope.data = {
            'user_id': AuthService.userID(),
            'course_date_id': $scope.runID,
        };



      $scope.addParticipation = function () {
            courseParticipation.save($scope.data, function (res) {
                if (res.hasOwnProperty('success')) {
                    $scope.is_participating = true;
                }
            });
        };


        $scope.removeParticipation = function () {
            courseParticipation.destroy($scope.data, function (res) {
                if (res.hasOwnProperty('success')) {
                    $scope.is_participating = false;
                }
            });
        };


        $scope.dateFilter = function (item) {
          if (moment($scope.run.created_at.date).isSame(item, 'year')) {
            return true;
          }
        };

        /************************
         * **** QUESTIONS ***** *
         ************************/

        $scope.qaData = {
            body: '',
            course_date_id: $scope.runID
        };


        /**
         * get course Questions and Answers
         */

        $scope.getQuestions = function () {
            QA.list({course_date_id: $scope.runID, page: $scope.currentQpage}, function (res) {
                $scope.questionAndAnswers = $scope.questionAndAnswers.concat(res.data);
            });
        };


        $scope.getQuestions();

        $scope.openMsgBox = function () {
            $scope.msgBoxIsOpen = true;
        };

        $scope.sendAnswer = function () {
            $scope.qaData.course_date_id = $scope.runID;
            $scope.errors = '';
            QA.save($scope.qaData, function (res) {
                res.data.user = {
                    name: $rootScope.AUTHUSER.name(),
                    avatar: $rootScope.AUTHUSER.avatar(),
                    user_level: {
                        name: $rootScope.AUTHUSER.levelName()
                    }
                };

                $scope.questionAndAnswers.push(res.data);
                $scope.msgBoxIsOpen = false;
                $scope.qaData.body = '';
            }, function (err) {
                $scope.errors = err.data;
            });
        };


        $scope.openAnswerMsgBox = function (q, idx) {
            swal({
                title: 'Your Answer',
                text: q.body,
                input: 'textarea',
                confirmButtonText: 'Send',
                width: '50%',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value) {
                            resolve();
                        } else {
                            reject('You need to write something!')
                        }
                    });
                },
                inputAttributes: {
                    'autocapitalize': 'on',
                    'autocorrect': 'on'
                }
            }).then(function (msg) {
                Answers.save({question_id: q.id, body: msg}, function (res) {
                    q.answer.push(res.data);
                }, function (err) {
                    swal(
                        'Oops...',
                        'Something went wrong!',
                        'error'
                    );
                });
            });
        };


        $scope.removeQuestion = function (q, idx) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                QA.delete({id: q.id}, function (res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    $scope.questionAndAnswers.splice(idx, 1);
                });
            })

        }


        $scope.removeAnswer = function (answer, idx) {

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                Answers.delete({id: answer.id}, function (res) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );
                    $scope.questionAndAnswers.splice(idx, 1);
                });
            })

        }

        $scope.getPlaceAvatar = function (key) {
            return 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' + key + '&key=AIzaSyAGwJ2ur16B-ZZtRPF7d3jFUtz_oXPhUrg'
        }


        $scope.loadMoreQuestion = function () {
            $scope.currentQpage++;
            $scope.getQuestions();
        };


    });
