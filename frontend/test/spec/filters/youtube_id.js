'use strict';

describe('Filter: youtubeId', function () {

  // load the filter's module
  beforeEach(module('runMoovApp'));

  // initialize a new instance of the filter before each test
  var youtubeId;
  beforeEach(inject(function ($filter) {
    youtubeId = $filter('youtubeId');
  }));

  it('should return the input prefixed with "youtubeId filter:"', function () {
    var text = 'angularjs';
    expect(youtubeId(text)).toBe('youtubeId filter: ' + text);
  });

});
