<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/10/17
 * Time: 6:13 PM
 */

namespace App\Transformers\Friendship;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Friendship\FriendList;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;

class FriendListTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new FriendList([
            'sender_id' => array_get($raw, 'sender_id'),
            'recipient_id' => array_get($raw, 'recipient_id'),
            'is_blocked' => array_get($raw, 'is_blocked')
        ]);
    }

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'friend' => FriendListTransformer::getOnlyFriends($model),
            'is_blocked' => $model->is_blocked
        ];
    }

    public static function getOnlyFriends($model)
    {
        if (!User::auth() || !$model->sender || !$model->recipient) {
            return [];
        }
        if ((int)User::auth()->getKey() === (int)$model->recipient->getKey()) {
            return UserForFriendsTransformer::transform($model->recipient);
        }
        return UserForFriendsTransformer::transform($model->sender);
    }
}