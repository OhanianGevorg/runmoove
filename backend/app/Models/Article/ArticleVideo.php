<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleVideo extends Model
{
    use SoftDeletes;
    protected $table = 'article_videos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'url',
        'article_id'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id');
    }
}
