<?php

namespace App\Transformers\NotificationTransformer;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\UserRelated\Notifications;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use Carbon\Carbon;

class NotificationTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'owner_id' => $model->owner_id,
            'sender' => UserForFriendsTransformer::transform($model->sender),
            'is_read' => $model->is_read,
            'event_id' => $model->event_id,
            'event_name' => $model->event_name,
            'created_at' => $model->created_at->toDateTimeString()
        ];
    }

    /**
     * @param array $raw
     * @param array $injector
     * @return Notifications
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new Notifications([
            'owner_id' => array_get($raw, 'owner_id'),
            'event_name' => array_get($raw, 'event_name'),
            'sender_id' => array_get($raw, 'sender_id'),
            'is_read' => array_get($raw, 'is_read'),
            'event_id' => array_get($raw, 'event_id'),
            'event_type' => array_get($raw, 'event_type')
        ]);
    }
}