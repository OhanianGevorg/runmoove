<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/18/2017
 * Time: 9:00 PM
 */

namespace App\Transformers\Shared;


use App\Interfaces\TransformerInterface;

class LevelTransformer implements TransformerInterface
{

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'name' => $model->name,
            'real_name' => $model->real_name,
            'up_score' => $model->up_score,
            'down_score' => $model->down_score,
            'img_name' => $model->img_name
        ];
    }
}