<?php

namespace App\Models\Course;

use App\Models\System\SystemPracticingType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseTypes extends Model
{
    //
    use SoftDeletes;
    protected $table = 'course_types';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_id',
        'type_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(SystemPracticingType::class,'type_id');
    }
}
