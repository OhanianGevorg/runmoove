'use strict';

describe('Controller: PagesRunArticlesProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesRunArticlesProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunArticlesProfileCtrl = $controller('PagesRunArticlesProfileCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunArticlesProfileCtrl.awesomeThings.length).toBe(3);
  });
});
