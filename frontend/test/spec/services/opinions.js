'use strict';

describe('Service: opinions', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var opinions;
  beforeEach(inject(function (_opinions_) {
    opinions = _opinions_;
  }));

  it('should do something', function () {
    expect(!!opinions).toBe(true);
  });

});
