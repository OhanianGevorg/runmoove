'use strict';

describe('Service: friends', function () {

  // load the service's module
  beforeEach(module('runMoovApp'));

  // instantiate service
  var friends;
  beforeEach(inject(function (_friends_) {
    friends = _friends_;
  }));

  it('should do something', function () {
    expect(!!friends).toBe(true);
  });

});
