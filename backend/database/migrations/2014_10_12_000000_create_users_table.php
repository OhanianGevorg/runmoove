<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('user_type');
            $table->integer('is_completed');
            $table->integer('completed_percent');
            $table->enum('opt_in',['yes','no']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('dob');
            $table->string('address');
            $table->string('avatar')->nullable();
            $table->string('fb_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->string('practice_since')->nullable();
            $table->string('running_frequency')->nullable();
            $table->string('transportation_mode')->nullable();
            $table->string('budget')->nullable();
            $table->string('practice_location')->nullable();
            $table->string('travel_mode')->nullable();
            $table->string('transport_preference')->nullable();
            $table->mediumText('presentation')->nullable();
            $table->string('api_token',60)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
