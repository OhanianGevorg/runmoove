'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:rmScroll
 * @description
 * # rmScroll
 */
angular.module('runMoovApp')
  .directive('rmScroll', function () {
    return {
      restrict: 'AEC',
      scope:{
        contentData:'=',
        scrollTo:'=',
      },
      link: function link(scope, element, attrs) {
        $(element).mCustomScrollbar();

      }
    };
  });
