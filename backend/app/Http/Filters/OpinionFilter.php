<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/21/2017
 * Time: 12:02 AM
 */

namespace App\Http\Filters;


class OpinionFilter extends AbstractFilter
{
    /**
     * CourseFilter constructor.
     * @param $query
     */
    public function __construct($query)
    {
        parent::__construct($query);
    }


    /**
     *  Allowed filter fields
     */
    protected $filter = [

    ];

    /**
     *  Allowed sorting fields
     */
    protected $order = [

    ];
}