'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:LayoutFooterCtrl
 * @description
 * # LayoutFooterCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
  .controller('FooterCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
