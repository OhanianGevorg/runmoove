'use strict';


angular.module('runMoovApp')
  .controller('RunOpinionsMainCtrl', function ($scope,$rootScope,Runs,$timeout) {




    $rootScope.searchRuns = function () {


      if (Object.keys($rootScope.filterField.address).length > 0) {
        $rootScope.searchRunCriteria.location = $rootScope.filterField.address.formatted_address;
        $rootScope.searchRunCriteria.latitude = $rootScope.filterField.address.geometry.location.lat();
        $rootScope.searchRunCriteria.longitude = $rootScope.filterField.address.geometry.location.lng();
      }


      $rootScope.searchRunCriteria.types = $rootScope.filterField.types;

      for (var n in $rootScope.searchRunCriteria) {

        if (Array.isArray( $rootScope.searchRunCriteria[n])) {
          $rootScope.searchRunCriteria[n] = $rootScope.searchRunCriteria[n].join();
        }
      }

      //temp


      $timeout(function () {
        Runs.query($rootScope.searchRunCriteria, function (res) {
          $scope.runs = res.data;
        });
      },300);

    };


    $rootScope.searchRuns();

  });
