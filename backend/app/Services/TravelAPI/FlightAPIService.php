<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 9/2/2017
 * Time: 12:02 AM
 */

namespace App\Services\TravelAPI;


class FlightAPIService extends AbstractGetAPIData
{
    protected $token = "7d4bddfe4ce17c56ed7f1205a8117f3f";
    protected $marker = "145897";
    private $params = [];
    private $segments = [];
    private $dates = [
        'departure' => ''
        , 'return' => ''
    ];
    private $data = [
        'host' => '',
        'user_ip' => '',
        'locale' => 'en',
        'trip_class' => '',
        'passengers' => [
            'adults' => 1,
            'children' => 0,
            'infants' => 0
        ],
        'segments' => [
            [
                'origin' => '',
                'destination' => '',
                'date' => ''
            ],
            [
                'origin' => '',
                'destination' => '',
                'date' => ''
            ]
        ],
        'currency' => 'usd'
    ];

    private $searchIDUrl = "http://api.travelpayouts.com/v1/flight_search";
    private $searchDataUrl = "http://api.travelpayouts.com/v1/flight_search_results?uuid=";

    public function __construct(array $params, $segments, $dates)
    {
        $this->params = $params;
        $this->segments = $segments;
        $this->dates = $dates;
        $this->data['marker'] = $this->marker;
    }

    /**
     * @param string $code
     * @return string
     */
    private function setTripClass($code = '')
    {
        $codeClass = 'C';
        if ($code === 'Y') {
            $codeClass = 'Y';
        }
        return $codeClass;
    }

    public function getAPIData(array $params = [])
    {
        try {
            $searchIDResponse = $this->fillSearchData($this->params, "flight")->getSearchID($this->data);
            if (!isset($searchIDResponse["search_id"]) && !$searchIDResponse["search_id"]) {
                throw new \Exception("Search Id not found", 404);
            }
            $this->searchDataUrl .= $searchIDResponse["search_id"];
            $flights = $this->initCurl("","GET",['Accept-Encoding:gzip,deflate,sdch'],$this->searchDataUrl);
            $result = [
                'content' => gzdecode($flights["content"]),
                'city_id' => $searchIDResponse["search_id"],
            ];
            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(),$e->getCode());
        }
    }

    /**
     * @param array $searchData
     * @param $type
     * @return $this
     */
    protected function fillSearchData(array $searchData, $type)
    {
        foreach ($searchData as $key => $datum) {
            if (isset($this->data[$key])) {
                $this->data[$key] = $datum;
            }
        }
        $this->data['trip_class'] = $this->setTripClass("Y");
        $this->data['segments'] = [
            [
                'origin' => array_get($this->segments, 'origin'),
                'destination' => array_get($this->segments, 'destination'),
                'date' => array_get($this->dates, 'departure')
            ],
            [
                'origin' => array_get($this->segments, 'destination'),
                'destination' => array_get($this->segments, 'origin'),
                'date' => array_get($this->dates, 'return')
            ]
        ];
        return $this;
    }

    private function buildToAPIRequest()
    {
        $signature = $this->makeSignature($this->data);
        if (!$signature) {
            throw new \Exception("Error when creating signature", 400);
        }
        $this->data['marker'] = $this->marker;
        $this->data['signature'] = $signature;
        return json_encode($this->data);
    }

    protected function initCurl($query = "", $method = "GET", array $headers = [], $url = "")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        if ($method === "POST") {
            curl_setopt($ch, CURLOPT_POST, true);
            if ($query) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
            }
        }
        if (count($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error) {
            throw new \Exception($error, 500);
        }
        return [
            'content' => $content,
            'response' => $response
        ];
    }

    protected function getSearchID(array  $searchData)
    {
        try {
            $requestJson = $this->buildToAPIRequest();
            $response = $this->initCurl($requestJson, "POST", ['Content-Type: application/json'], $this->searchIDUrl);
            if (!isset($response['content'])) {
                throw new \Exception("No Data", 404);
            }
            if (isset($response['content'])) {
                if (isset($response["response"]) && isset($response["response"]["http_code"]) && $response["response"]["http_code"] !== 200) {
                    throw new \Exception($response['content'], $response["response"]["http_code"]);
                }
                if (strpos($response['content'], "Bad") !== false) {
                    throw new \Exception("Bad Request", 500);
                }
            }
            $content = json_decode($response['content'], true);
            return $content;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
}