<?php

namespace App\Http\Requests\UploadRequests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class GPXUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gpx'=>'required|max:5120'
        ];
    }
}
