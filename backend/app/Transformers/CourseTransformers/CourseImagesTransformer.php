<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/7/2017
 * Time: 12:22 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Course\CourseImages;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use Illuminate\Support\Facades\Storage;

class CourseImagesTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'url' => Storage::disk('public')->exists($model->img_url) ? asset(Storage::url($model->img_url)) : '',
            'course_id' => $model->course_id
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new CourseImages([
            'course_id' => $injector['course_id'],
            'img_url' => array_get($raw, 'url')
        ]);
    }
}