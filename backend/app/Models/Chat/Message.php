<?php

namespace App\Models\Chat;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes, FilterTrait;

    const IS_READ = 1;
    const IS_NOT_READ = 0;
    //
    protected $table = 'messages';
    protected $primaryKey = 'id';
    protected $fillable = [
        'room_id',
        'user_id',
        'recipient_id',
        'body',
        'is_read',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function room()
    {
        $this->belongsTo(Room::class, 'room_id');
    }

    public function isRead()
    {
        return (int)$this->is_read === self::IS_READ;
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function scopeLast($query)
    {
        return $query->latest();
    }

    public static function getReceivedMessages($user_id, array $ids = [])
    {
        return static::whereIn('id', $ids)->where('recipient_id', '=', $user_id);
    }

    /**
     * @param User $user
     * @param Room|null $room
     * @return mixed
     * @desc static function to get unread message
     */
    public static function getUnreadMessages(User $user, Room $room = null)
    {
        /**
         * @var $query Model
         */
        $query = static::where('recipient_id', '=', $user->getKey());
        if ($room) {
            $query->where('room_id', '=', $room->getKey());
        }
        return $query->where('is_read', '=', self::IS_NOT_READ);
    }

    /**
     * @param $query
     * @param User|null $user
     * @return mixed
     * @desc scope for unread messages
     */
    public function scopeUnreadMessages($query, User $user = null)
    {
        if ($user) {
            $query->where('recipient_id', '=', $user->getKey());
        }
        return $query->where('is_read', '=', self::IS_NOT_READ);
    }
}
