<?php

namespace App\Http\Controllers\CourseControllers;


use App\Http\Requests\CourseRequests\APICallRequest;
use App\Http\Requests\CourseRequests\AviaAPIRequest;
use App\Http\Requests\CourseRequests\AviaBookingRequest;
use App\Http\Requests\CourseRequests\CourseReadRequests;
use App\Http\Requests\CourseRequests\CourseRenewRequest;
use App\Http\Requests\CourseRequests\CourseStoreRequest;
use App\Http\Requests\CourseRequests\CourseUpdateRequest;
use App\Http\Requests\CourseRequests\GetAPIDataRequest;
use App\Jobs\CallHotelsApi;
use App\Jobs\CallTravelsApi;
use App\Models\Course\Course;
use App\Models\Course\CourseDate;
use App\Models\Course\CourseFormats;
use App\Models\Course\CourseTypes;
use App\Services\GetContentsService;
use App\Transformers\CourseTransformers\CourseDateTransformer;
use App\Transformers\CourseTransformers\CourseImagesTransformer;
use App\Transformers\CourseTransformers\CourseMarkerTransformer;
use App\Transformers\CourseTransformers\CourseTransformer;
use App\Transformers\CourseTransformers\TravelAPITransformer;
use App\Transformers\UserTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class CourseController extends Controller
{
    public function __construct()
    {
//        $this->middleware('ownAuth', ['except' => ['index', 'show']]);
    }

    /**
     * @param CourseReadRequests $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CourseReadRequests $request)
    {
        /**
         * @var $course_dates CourseDate
         */
        try {
            $course_dates = CourseDate::getAll($request->all());
            return response()->json([
                'data' => CourseDateTransformer::transformCollection($course_dates)
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }

    }

    /**
     * @param CourseStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseStoreRequest $request)
    {

        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'You have not permissions to see this page'
            ], 404);
        }

        DB::beginTransaction();
        try {
            /**
             * @var $user User
             */
            $course = CourseTransformer::prepare($request->all(), ['user_id' => $user->getKey()]);
            $course->save();

            $injector = [
                'course_owner_id' => $user->getKey(),
                'course_id' => $course->getKey(),
                'official_organizer_id' => $user->is_orginizer() ? $user->getKey() : null
            ];
            $course_dates = CourseDateTransformer::prepare([
                'start_at' => $request->get('start_at'),
                'end_at' => $request->get('end_at')],
                $injector);

            $course_dates->save();

            if ($request->has('course_formats')) {
                CourseFormats::insert(array_map(function ($format) use ($course) {
                    return [
                        'format_id' => $format,
                        'course_id' => $course->getKey(),
                        'created_at' => Carbon::now()
                    ];
                },
                    explode(',', $request->get('course_formats'))));
            }

            if ($request->has('practicing_types')) {
                CourseTypes::insert(array_map(function ($type) use ($course) {

                    return [
                        'type_id' => $type,
                        'course_id' => $course->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, explode(',', $request->get('practicing_types'))));
            }


            if ($request->has('course_images')) {

                $course
                    ->course_images()
                    ->saveMany(CourseImagesTransformer::prepareCollection($request->get('course_images'), ['course_id' => $course->getKey()]));
            }

            if ($request->has('course_markers')) {
                $course
                    ->course_markers()
                    ->saveMany(CourseMarkerTransformer::prepareCollection($request->get('course_markers'), ['course_id' => $course->getKey()]));
            }
            if ($request->has('course_coords') || $request->has('course_coords_url')) {
                $data = [
                    'latitude' => null,
                    'longitude' => null,
                    'marker_type' => null,
                    'gpx_url' => (!$request->has('course_coords') && $request->has('course_coords_url')) ? $request->get('course_coords_url') : null,
                    'coords' => $request->has('course_coords') ? $request->get('course_coords') : null
                ];
                $course_coords = CourseMarkerTransformer::prepare($data, ['course_id' => $course->getKey()]);
                $course_coords->save();
            }

            DB::commit();
            return response()->json([
                'course_date' => CourseDateTransformer::transform($course_dates),
                'success' => 'Course has successfully inserted'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function show($id, Request $request)
    {
        try {
            $course_date = CourseDate::findOrFail($id);
            return response()->json([
                'course_date' => CourseDateTransformer::transform($course_date)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Course does not exist'
            ], 404);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'You have not permission'
            ], 500);
        }
        try {

            /**
             * @var $user User
             */
            $course_date = CourseDate::find($id)->where('course_owner_id', '=', $user->getKey());
            if (!$course_date) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 500);
            }
            return response()->json([
                'course_date' => CourseDateTransformer::transform($course_date)
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function update(CourseUpdateRequest $request)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, Request $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'You have not permission'
            ]);
        }
        try {

            /**
             * @var $user User
             */
            $date = CourseDate::findOrFail($id)->where('course_owner_id', '=', $user->getKey());
            if (!$date) {
                return response()->json([
                    'error' => 'You have not permission'
                ], 500);
            }
            $date->delete();

            return response()->json([
                "success" => "Course has successfully deleted!"
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "error" => "Course does not exist!"
            ], 404);
        }
    }

    /**
     * @param CourseRenewRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renewCourse(CourseRenewRequest $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                "error" => "You have not permission!"
            ], 404);
        }
        try {
            /**
             * @var $user User
             */
            $course_date = CourseDateTransformer::prepare($request->all(), ['user_id' => $user->getKey(), 'course_id' => $request->get('user_id')]);
            $course_date->save();
            return response()->json([
                "course_date" => $course_date,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "error" => "Something goes wrong"
            ], 500);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCourseParticipants(Request $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }

        try {
            /**
             * @var $courseDate CourseDate
             */
            $courseDate = CourseDate::findOrFail($request->get('course_date_id'));

            return response()->json([
                'users' => UserTransformer::transformCollection($courseDate->get_participants_query()->getAll())
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param APICallRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @return void
     */
    public function callAPI(APICallRequest $request)
    {
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'ID is required'
            ], 422);
        }
        try {
            $course_date = CourseDate::findOrFail($request->get('course_date_id'));
            $apiParams = [
                'origin' => $request->get('origin_iata'),
                'destination' => $request->get('destination_iata'),
                'user_ip' => $request->getClientIp()
            ];
            dispatch(new CallTravelsApi($course_date, $apiParams));
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
//            dd($e->getMessage());
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function getApiData(GetAPIDataRequest $request)
    {
        if (!$request->has('course_date_id')) {
            return response()->json([
                'error' => 'Course ID is required'
            ], 422);
        }
        /**
         * @var $course_date CourseDate
         */
        try {
            $course_date = CourseDate::findOrFail($request->get('course_date_id'));
            $api_data = TravelAPITransformer::transformCollection($course_date->getAPIResponses());
            return response()->json([
                'data' => $api_data
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param AviaAPIRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @desc this is test
     */
    public function avia(AviaAPIRequest $request)
    {
        if (!$request->get('url')) {
            return response()->json([
                'error' => 'Url is required'
            ], 422);
        }
        try {
            $data = GetContentsService::getContent($request->get('url'));
            return response()->json([
                'success' => 'Data successfully has gotten',
                'data' => $data
            ], 200);
        } catch (InvalidArgumentException $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 400);
        } catch (NotFoundHttpException $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * @param AviaBookingRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @desc aviaservices booking url
     */
    public function getAviaBookingUrl(AviaBookingRequest $request)
    {
        $url = 'http://api.travelpayouts.com/v1/flight_searches/' . $request->get('search_id') . '/clicks/' . $request->get("term_url") . '.json';

        try {
            $data = json_decode($this->curlInit($url)['content'], true);
            if (isset($data['error'])) {
                return response()->json([
                    'error' => 'params error'
                ], 400);
            }
            return response()->json([
                'url' => $data['url']
            ], 200);
        } catch (\Exception $e) {
            if (!$url) {
                return response()->json([
                    'error' => 'Invalid Url'
                ], 500);
            }
        }
        return response()->json([
            'url' => $url
        ], 200);
    }

    private function curlInit($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error) {
            throw new \Exception($error, 500);
        }
        return [
            'content' => $content,
            'response' => $response
        ];
    }
}
