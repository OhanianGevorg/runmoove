<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Session::has('api_token')) {
            Session::forget('api_token');
        }
        return $response;
    }
}