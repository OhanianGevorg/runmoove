<?php

namespace App\Http\Controllers\Shared;

use App\Http\Requests\UploadRequests\GPXUploadRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GPXDownloadController extends Controller
{
    //
    public function upload(GPXUploadRequest $request)
    {
        if (!$request->hasFile('gpx')) {
            return response()->json([
                'error' => 'Download valid GPX file'
            ], 422);
        }
        try {

            $file = $request->file('gpx');
            $extension = $file->getClientOriginalExtension() ? $file->getClientOriginalExtension() : '.gpx';
            $file_name = md5(strtotime('now')) . '.' . $extension;
            Storage::putFileAs('public/', $file, $file_name);

            return response()->json([
                'gpx' => $file_name
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function getFile($fileName)
    {
        if (!$fileName) {
            return response()->json([
                'error' => 'File name is missing'
            ], 422);
        }

        if (!Storage::disk('public')->exists($fileName)) {
            return response()->json([
                'error' => 'File Does not exist'
            ], 404);
        }
        try {
            $headers = [
              'Content-Type'=>'application/xml'
            ];
            return response()->file(storage_path("app/public/".$fileName));
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
