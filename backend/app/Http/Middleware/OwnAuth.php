<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Session;

class OwnAuth
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('X-TOKEN');
        $user = User::where('api_token', $token)->first();

        if (!$token||!$user) {

            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('errors.503');
            }
        } else {
            Session::put('api_token', $token);
        }

        return $next($request);
    }
}
