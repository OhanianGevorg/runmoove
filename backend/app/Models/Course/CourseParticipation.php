<?php

namespace App\Models\Course;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseParticipation extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'course_participation';
    protected $fillable = [
        'user_id',
        'course_date_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function course_date()
    {
        return $this->belongsTo(CourseDate::class,'course_date_id');
    }
}
