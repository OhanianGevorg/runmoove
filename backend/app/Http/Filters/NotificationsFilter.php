<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/13/17
 * Time: 1:01 PM
 */

namespace App\Http\Filters;


class NotificationsFilter extends AbstractFilter
{
    /**
     *  Allowed filter fields
     */
    protected $filter = [
        'isRead'
    ];

    /**
     *  Allowed sorting fields
     */
    protected $order = [
        'created'
    ];

    /**
     * @desc filter by is_read value
     * @param string $value
     * @return mixed
     */
    public function filterByIsRead($value = '')
    {
        return $this->query->where('is_read', $value);
    }

    /**
     * Sort by created_at
     *
     * @param string $dir , ASC or DESC
     */
    protected function sortByCreated($dir = '')
    {
        $this->query->orderBy('created_at', $dir);
    }
}