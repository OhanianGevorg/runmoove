<?php

namespace App\Models\QuestionAndAnswer;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes, FilterTrait;
    protected $table = 'answers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'question_id',
        'user_id',
        'body'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function answering()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
