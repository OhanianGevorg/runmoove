'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.auth/events
 * @description
 * # auth/events
 * Constant in the runMoovApp.
 */
angular.module('runMoovApp')
  .constant('AuthEvents', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
  })
  .constant('ErrorEvents', {
    notFound: 'page-not-found'
  });
