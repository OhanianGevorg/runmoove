'use strict';

angular.module('runMoovApp')
  .controller('ProfileUpdateCtrl', function ($scope,systemConstants,Users,AuthService,$state,$timeout,$mdToast) {

    $scope.initialValue = 0;

    $scope.data = {
      practice_since: '',
      running_frequency: '',
      transportation_mode: '',
      budget: '',
      practice_location: '',
      travel_mode: '',
      transport_preference: '',
      presentation: '',
      course_formats: [1,3],
      user_practicing: '',
      course_importance: '',
    };

    $scope.sliderTooltip = function(event, ui) {
      $scope.curValue = ui.value || $scope.initialValue;
      $scope.tooltip = '<div class="tooltips"><div class="tooltips-inner">' + $scope.curValue + ' %</div></div>';

      $('.ui-slider-handle').html($scope.tooltip);

    };



    angular.element( "#slider5" ).slider( "disable" );


    $scope.scrollToElement = function (element) {
      angular.element('body').duScrollToElement(angular.element('#' + element), 200, 1000);
    };


    Users.show({user_id:AuthService.userID(),id:AuthService.userID()},function (res) {
      $scope.data = res.user;

      for (var i in res.user) {
        if (Array.isArray(res.user[i])) {
          var data = [];
          for (var n in res.user[i]) {
            data.push(res.user[i][n].id);
          }
          $scope.data[i] = data;
        }
      }


      $scope.initialValue = $scope.data.completed_percent;

      angular.element("#slider5").slider({
        value: $scope.initialValue,
        min: 1,
        max: 100,
        step: 1,
        create: $scope.sliderTooltip,
        slide: $scope.sliderTooltip
      });
      $scope.slider.value = $scope.data.completed_percent;
      console.log($scope.data.completed_percent);
    });

//  main slider
    $scope.slider = {
      value:0,
      options: {
        floor: 0,
        ceil: 100,
        step: 1,
        disabled:true
      }
    };

    systemConstants.get(function (res) {
      $scope.systemTypes = res.system_types;

    });





    $scope.updateProfileData = function () {
      for (var n in $scope.data) {
        if(Array.isArray($scope.data[n])) {
          $scope.data[n]= $scope.data[n].join();
        }
      }
      Users.saveAdditional($scope.data,function (res) {
        $state.go('profile.view',{id:AuthService.userID()});
        $timeout(function () {
          $mdToast.show(
            $mdToast.simple()
              .textContent('Votre profil a été changé!')
              .position('top right')
              .hideDelay(3000)
          );
        },1500);
      });


    };

  });
