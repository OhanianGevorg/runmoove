<?php

namespace App\Http\Requests\UserStoreRequests;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends AbstractRequest
{

    public function authorize()
    {
        return !is_null(User::auth()) && (int)User::auth()->getKey() === (int)$this->route('user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
                    ->ignore($this->route('user'), 'id')
            ],
            'dob' => 'required|date',
            'address' => 'required|max:255',
            'is_subscribed' => 'string|max:255',
        ];
    }
}
