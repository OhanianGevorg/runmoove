'use strict';

describe('Controller: PagesRunRunOpinionsMainCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesRunRunOpinionsMainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunRunOpinionsMainCtrl = $controller('PagesRunRunOpinionsMainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunRunOpinionsMainCtrl.awesomeThings.length).toBe(3);
  });
});
