<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 5/29/17
 * Time: 4:09 PM
 */
namespace App\Http\Controllers\Auth;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserStoreRequests\LoginRequest;
use App\Transformers\UserTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait OwnLoginTrait
{
    use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            /**
             * @var $user User
             */
            $user = Auth::guard('web')->getLastAttempted(); // get hold of the user
            if (!$user->api_token) {
                $token = md5(Carbon::now());
                $user->api_token = $token;
            };

            if (!$user->user_level()->exists) {
                $user->score = 0;
                $user->setLevel()->save();
            }
            try {
                $user->save();
                return $this->sendLoginResponse($request, $user);
            } catch (\Exception $e) {
                return response()->json([
                    'error' => 'Something goes wrong'
                ], 422);
            }
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return response()->json([
            'error' => 'Wrong Credentials'
        ], 422);

    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|email|max:255', 'password' => 'required|max:255',
        ]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request, $user)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json([
            'user' => UserTransformer::transform($user),
            'api_token' => $user->api_token
        ], 200);
    }

    /**
     * @param UserLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            'error' => 'Wrong Credentials'
        ], 422);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = User::auth();
        if ($user) {
            $user->api_token = null;
            $user->save();
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
        }

        return response()->json([
            'success' => 'Logged Out!'
        ], 200);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
