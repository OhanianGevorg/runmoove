<?php
namespace App\Transformers\Article;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Article\Article;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;

class ArticleTransformer implements TransformerInterface, PrepareInterface
{

    use TransformCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Article([
            'title' => array_get($raw, 'title'),
            'body' => array_get($raw, 'body'),
            'category_id' => array_get($raw, 'category'),
            'user_id' => $injector['user_id']
        ]);
    }

    public static function transform($model)
    {
        /**
         * @var $model Article
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'title' => $model->title,
            'body' => html_entity_decode($model->body),
            'category' => CategoryTransformer::transform($model->category),
            'user' => UserForFriendsTransformer::transform($model->user),
            'article_images' => ArticleImageTransformer::transformCollection($model->images),
            'article_videos' => ArticleVideoTransformer::transformCollection($model->videos),
            'created_at'=>$model->created_at
        ];
    }

    public static function prepareUpdate(array $raw, array $injector = [])
    {
        return [
            'title' => array_get($raw, 'title'),
            'body' => array_get($raw, 'body'),
            'category_id' => array_get($raw, 'category'),
        ];
    }
}