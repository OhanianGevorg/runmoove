<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/9/2017
 * Time: 11:10 PM
 */

namespace App\Transformers\UserOtherTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Transformers\Shared\LevelTransformer;
use App\User;
use Illuminate\Support\Facades\Storage;

class UserXsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        /**
         * @var $model User
         */

        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'name' => $model->first_name . " " . $model->last_name,
            'avatar' => (!filter_var($model->avatar, FILTER_VALIDATE_URL) && Storage::disk('public')->exists($model->avatar)) ? asset(Storage::url($model->avatar)) : $model->avatar,
            'user_level' => LevelTransformer::transform($model->get_user_level())
        ];
    }
}