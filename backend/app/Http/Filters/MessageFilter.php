<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/18/2017
 * Time: 2:21 AM
 */

namespace App\Http\Filters;


class MessageFilter extends AbstractFilter
{
    public function __construct($query)
    {
        $this->query = $query;
    }

    /**
     *  Allowed filter fields
     */
    protected $filter = [

    ];

    /**
     *  Allowed sorting fields
     */
    protected $order = [
        'created'
    ];

    /**
     * Sort by created_at
     *
     * @param string $dir , ASC or DESC
     */
    public function sortByCreated($dir = '')
    {
        return $this->query->orderBy('created_at', $dir);
    }
}