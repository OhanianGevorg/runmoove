<?php

namespace App\Http\Requests\Chat;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class MessageStoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'recipient_id' => 'required|integer|exists:users,id',
            'body' => 'required|max:16777215'
        ];
        if ($this->has('room_id')) {
            $rule['room_id'] = 'required|integer|exists:rooms,id';
        }

        return $rule;
    }
}
