/**
 * @ngdoc service
 * @name runMoovApp.articles
 * @description
 * # articles
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('QA', function (ENV,$resource) {
    return $resource(ENV.apiEndpoint + '/question/:id', {}, {
      get: {
        method: 'GET',
      },
      query: {
        method: 'GET',
        isArray: false
      },
      delete: {
        method: 'DELETE',
      },
      list: {
        url: ENV.apiEndpoint + '/question/list',
        method: 'GET',
      },

    });
  });

