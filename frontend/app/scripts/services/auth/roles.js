'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.auth/roles
 * @description
 * # auth/roles
 * Constant in the runMoovApp.
 */
angular.module('runMoovApp')
  .constant('userRoles', {
    all: '*',
    user: 4,
    admin: 3,
  });
