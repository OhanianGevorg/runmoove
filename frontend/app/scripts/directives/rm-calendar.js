'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:rmCalendar
 * @description
 * # rmCalendar
 */
angular.module('runMoovApp')
  .directive('rmCalendar', function ($state) {
    return {

      scope: {
        events:'=',
        view:'='
      },
      restrict: 'AEC',
      link: function postLink(scope, element, attrs) {

        // $('#mycalendar').fullCalendar( 'gotoDate', 2011,04,7 );

        scope.$watch("events.events", function(newValue, oldValue) {
          if (newValue) {


            angular.forEach(newValue,function (elem) {
              console.log(elem.hasOwnProperty('end'));
              if (elem.hasOwnProperty('end') && scope.view == 'listMonth') {
                console.log(elem);
                delete elem.end;
              }
            });
            setTimeout(function () {

              $(element).fullCalendar({
                height: 850,
                editable: false,
                defaultView: scope.view ? scope.view : 'month',
                displayEventTime:false,
                header:{
                  left: 'title',
                  center: '',
                  right: 'today prev,next'
                },
                viewRender: function(view, element) {
                  console.log(element);

                  if ($('.fc-left img').length == 0) {
                    $('.fc-left').prepend('<img src="img/calendar-small.png">');
                  };
                },
                eventRender: function (event, element) {
               if (scope.view == 'listMonth') {
                 element.html(

                   '<div class="row pointer" ui-sref="run.profile({id:event.id})">'+
                   '<div class="col-md-2">'+
                   '<h1 class="day">'+ moment(event.start).lang('fr').format('dddd') + '</h1>'+
                   '<div class="date">'+ moment(event.start).format('DD') + '</div>'+
                   '</div>'+
                   '<div class="col-md-9">'+
                   '<p class="content-slider-text">'+ event.description+ '</p>'+
                   '</div>'+
                   '</div>');
               }


                  element.popover({
                    title:  event.title,
                    placement: 'top',
                    html:true,
                    content: ''
                  });
                },
                eventClick: function(calEvent, jsEvent, view) {

                  console.log(calEvent, jsEvent, view);
                  $state.go('run.profile',{id:calEvent.id});
                  // change the border color just for fun
                },
                events:scope.events,
              });

            },1000);
          }
        });
      }
    };
  });
