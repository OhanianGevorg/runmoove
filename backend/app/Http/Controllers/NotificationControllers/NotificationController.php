<?php

namespace App\Http\Controllers\NotificationControllers;

use App\Http\Requests\Notifications\NotificationIsReadRequest;
use App\Http\Requests\Notifications\NotificationReadRequest;
use App\Models\UserRelated\Notifications;
use App\Services\TransformerMapper;
use App\Transformers\NotificationTransformer\NotificationTransformer;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotificationController extends Controller
{
    //
    public function index(NotificationReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        try {
            $notifications = NotificationTransformer::transformCollection(User::auth()->notifications()->getAll($request->all()));
            return response()->json([
                'notifications' => $notifications
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something has gone wrong'
            ], 500);
        }
    }

    public function updateList(NotificationIsReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You hav not permission'
            ], 403);
        }
        if (!$request->has('notification_ids')) {
            return response()->json([
                'error' => 'Fields are required'
            ], 422);
        }
        try {
            Notifications::setStatus($request->get('notification_ids'), Notifications::IS_READ);
            return response()->json([
                'success' => 'Successfully updated'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function show($id)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        try {
            /**
             * @var $notification Notifications
             * @var $event Model
             */
            $notification = Notifications::findOrFail($id);
            $event = TransformerMapper::mapTransformer($notification->event);
            return response()->json([
                'notification' => NotificationTransformer::transform($notification),
                'event' => $event
            ], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Event not found'
            ], 404);

        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
