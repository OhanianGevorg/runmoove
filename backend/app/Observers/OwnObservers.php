<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/10/2017
 * Time: 12:06 AM
 */
namespace App\Observers;

use App\Libs\EventClassesDescriber;

class OwnObservers
{

    /**
     * @desc check if models exists in eventlist array and call event generator function
     * @param $model
     */
    public function created($model)
    {
        $event_array = $this->checkEvent($model);
        if (count($event_array) && array_key_exists('created', $event_array)) {
            $this->callEvents($event_array['created'], $model);
        }
        return;
    }

    /**
     * @desc check if model exists in eventlist array
     * @param $model
     * @return array|mixed
     */
    private function checkEvent($model)
    {
        return EventClassesDescriber::getEvents(get_class($model));
    }

    /**
     * @desc loop over eventlist and call event or broadcast methods
     * @param $eventArray
     * @param $model
     */
    private function callEvents($eventArray, $model)
    {
        $eventType = 'event';
        foreach ($eventArray as $event => $type) {

            if ($type == 'broadcast') {
                $eventType = 'broadcast';
            }
            try {
                if (class_exists($event)) {
                    if ($eventType == 'event') {
                        event(new $event($model));
                    } else {
                        broadcast(new $event($model));
                    }
                }
            } catch (\Exception $e) {
                continue;
            }
        }
    }
}