<?php

namespace App\Http\Requests\Friendship;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class FriendAcceptRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->has('event_id')) {
            return [
                'event_id' => 'required|integer|exists:notifications,id'
            ];
        }
        return [
            'request_id' => 'required|integer|exists:friend_requests,id'
        ];
    }
}
