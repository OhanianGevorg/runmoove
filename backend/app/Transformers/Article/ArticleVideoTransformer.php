<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/1/2017
 * Time: 1:11 AM
 */

namespace App\Transformers\Article;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Article\ArticleVideo;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class ArticleVideoTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function transform($model)
    {
        /**
         * @var $model ArticleVideo
         */
        if (!$model) {
            return [

            ];
        }

        return [
            'id' => $model->getKey(),
            'file_name' => $model->url,
            'article_id' => $model->article_id
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new ArticleVideo([
            'url' => array_get($raw, 'url'),
            'article_id' => array_get($injector, 'article_id')
        ]);
    }
}