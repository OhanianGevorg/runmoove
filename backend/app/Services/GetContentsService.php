<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/25/2017
 * Time: 11:35 PM
 * @desc Small service class for grab file contents from giving url
 */

namespace App\Services;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Exception\InvalidArgumentException;

class GetContentsService
{
    public static function getContent($url)
    {
        if (!$url) {
            throw new InvalidArgumentException("Url is required");
        }
        $data = file_get_contents($url);
        if (!$data) {
            throw new NotFoundHttpException("file get contents does not contain any files");
        }
        return $data;
    }
}