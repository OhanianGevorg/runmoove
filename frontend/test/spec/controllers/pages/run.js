'use strict';

describe('Controller: PagesRunCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesRunCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunCtrl = $controller('PagesRunCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunCtrl.awesomeThings.length).toBe(3);
  });
});
