<?php

namespace App\Http\Requests\UserStoreRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255|min:5'
        ];
    }
}
