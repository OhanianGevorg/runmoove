'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.articles
 * @description
 * # articles
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('Articles', function (ENV,$resource) {
    return $resource(ENV.apiEndpoint + '/article', {}, {

      query: {
        method: 'GET',
        isArray: false
      },
      delete: {
        method: 'DELETE',
      },
      show: {
        url: ENV.apiEndpoint + '/article/:id',
        method: 'GET',
      },
      categories: {
        url: ENV.apiEndpoint + '/categories',
          method: 'GET',
      },
      update: {
        url: ENV.apiEndpoint + '/article/:id',
        method: 'PUT',
      },
    });
  });
