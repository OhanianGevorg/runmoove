<?php

namespace App\Models\Article;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes,FilterTrait;
    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'body',
        'category_id',
        'user_id'
    ];

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }


    public function videos()
    {
        return $this->hasMany(ArticleVideo::class, 'article_id');
    }

    public function images()
    {
        return $this->hasMany(ArticleImage::class, 'article_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
