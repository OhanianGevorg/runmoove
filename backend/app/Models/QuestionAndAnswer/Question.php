<?php

namespace App\Models\QuestionAndAnswer;

use App\Models\Course\CourseDate;
use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes, FilterTrait;
    protected $table = 'questions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'course_date_id',
        'body'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questioning()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course_date()
    {
        return $this->belongsTo(CourseDate::class, 'course_date_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function answer()
    {
        return $this->hasMany(Answer::class, 'question_id');
    }
}
