'use strict';

describe('Controller: PagesUserProfileCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesUserProfileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesUserProfileCtrl = $controller('PagesUserProfileCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesUserProfileCtrl.awesomeThings.length).toBe(3);
  });
});
