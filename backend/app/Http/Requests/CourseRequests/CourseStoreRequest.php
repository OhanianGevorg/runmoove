<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Support\Facades\Storage;

class CourseStoreRequest extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $custom_rules = [];
        if ($this->has('course_markers')) {
            foreach ($this->get('course_markers') as $index => $value) {
                $custom_rules['course_markers.' . ($index) . '.marker_type'] = 'string|max:255';
                $custom_rules['course_markers.' . ($index) . '.latitude'] = 'numeric|required_with:course_markers.' . ($index) . '.marker_type';
                $custom_rules['course_markers.' . ($index) . '.longitude'] = 'numeric|required_with:course_markers.' . ($index) . '.marker_type';

            }
        }
        if ($this->has('course_coords')){
            $custom_rules['course_coords'] = 'json|max:65000';
        }
        if ($this->has('course_coords_url')){
            $custom_rules['course_coords_url'] = 'string|max:255';
        }
            if ($this->has('course_images')) {
                foreach ($this->get('course_images') as $index => $value) {
                    $custom_rules['course_images.' . ($index) . '.url'] = 'required_with:course_images|file_exists:storage';
                }
            }

        return array_merge([
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'distance' => 'required|numeric',
            'location' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'organizer_telephone' => 'string|max:255',
            'organizer_email' => 'required|email|max:255',
            'site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i'),
            'registration_fee' => 'numeric',
            'participants_number' => 'numeric',
            'description' => 'max:65000',
            'course_formats' => 'max:255',
            'practicing_types' => 'max:255',
            'start_at' => 'required|date',
            'end_at' => 'required|date|after_or_equal:start_at',
            'license_agreement' => 'required|max:255|in:true',
            'course_coords' => 'json',
        ], $custom_rules);
    }
}
