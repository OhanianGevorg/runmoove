<?php

namespace App\Http\Requests\CourseRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class AviaAPIRequest extends AbstractRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'=>'required|max:255|url'
        ];
    }
}
