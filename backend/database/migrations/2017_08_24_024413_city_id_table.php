<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CityIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("city_id_list", function (Blueprint $table) {
            $table->increments("id");
            $table->double("lng");
            $table->double("lat");
            $table->string("city_id");
            $table->string("country_short");
            $table->string("city");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIFExists("city_id_list");
    }
}
