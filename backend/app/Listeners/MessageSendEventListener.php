<?php

namespace App\Listeners;

use App\Events\MessageSendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageSendEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageSendEvent  $event
     * @return void
     */
    public function handle(MessageSendEvent $event)
    {
        //
    }
}
