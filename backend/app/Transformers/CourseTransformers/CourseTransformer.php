<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/7/2017
 * Time: 12:17 AM
 */
namespace App\Transformers\CourseTransformers;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Course\Course;
use App\Traits\TransformCollection;
use App\Transformers\Shared\StaticTransformer;
use Carbon\Carbon;

class CourseTransformer implements PrepareInterface, TransformerInterface
{
    use TransformCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new Course([
            'user_id' => $injector['user_id'],
            'latitude' => array_get($raw, 'latitude'),
            'longitude' => array_get($raw, 'longitude'),
            'distance' => (int)array_get($raw, 'distance'),
            'name' => array_get($raw, 'name'),
            'location' => array_get($raw, 'location'),
            'organizer_email' => array_get($raw, 'organizer_email'),
            'description' => array_get($raw, 'description'),
            'organizer_telephone' => array_get($raw, 'organizer_telephone'),
            'site' => array_get($raw, 'site'),
            'registration_fee' =>(int) array_get($raw, 'registration_fee'),
            'participants_number' => (int)array_get($raw, 'participants_number'),
        ]);
    }

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'user_id'=>$model->user_id,
            'latitude'=>$model->latitude,
            'longitude'=>$model->longitude,
            'distance'=>$model->distance,
            'name'=>$model->name,
            'location'=>$model->location,
            'organizer_email'=>$model->organizer_email,
            'description'=>$model->description,
            'organizer_telephone'=>$model->organizer_telephone,
            'site'=>$model->site,
            'registration_fee'=>$model->registration_fee,
            'participants_number'=>$model->participants_number,
            'course_images'=>CourseImagesTransformer::transformCollection($model->course_images),
            'course_formats'=>StaticTransformer::transformCollection($model->get_course_formats()),
            'course_types'=>StaticTransformer::transformCollection($model->get_course_types()),
            'course_markers'=>CourseMarkerTransformer::transformCollection($model->course_markers),
            'created_at' =>$model->created_at
        ];
    }

    public static function courseXsTransformer($model)
    {
        if(!$model){
            return [];
        }
        return [
            'id' => $model->getKey(),
            'user_id'=>$model->user_id,
            'name'=>$model->name,
            'latitude'=>$model->latitude,
            'longitude'=>$model->longitude,
            'registration_fee'=>$model->registration_fee,
            'participants_number'=>$model->participants_number,
            'course_images'=>CourseImagesTransformer::transformCollection($model->course_images),
            'created_at' =>$model->created_at
        ];
    }
}