<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('opinion_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_opinion_id')->unsigned();
            $table->string('img_url');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('course_opinion_id')
                ->references('id')
                ->on('course_opinion')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('opinion_images');
    }
}
