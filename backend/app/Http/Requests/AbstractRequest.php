<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 5/28/2017
 * Time: 11:38 PM
 */
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function expectsJson()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    public abstract function rules();
}