<?php

namespace App\Listeners;

use App\Events\FriendRequestSendEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FriendRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FriendRequestSendEvent  $event
     * @return void
     */
    public function handle(FriendRequestSendEvent $event)
    {
        //
    }
}
