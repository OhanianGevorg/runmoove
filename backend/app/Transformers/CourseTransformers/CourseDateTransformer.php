<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 6/7/2017
 * Time: 12:20 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Course\CourseDate;
use App\Traits\TransformCollection;
use App\Transformers\QuestionAndAnswer\QuestionTransformer;
use App\Transformers\UserOtherTransformers\UserForParticipantsTransformer;
use Carbon\Carbon;

class CourseDateTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param array $raw
     * @param array $injector
     * @return CourseDate
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new CourseDate([
            'course_owner_id' => array_get($injector, 'course_owner_id'),
            'course_id' => array_get($injector, 'course_id'),
            'start_at' => strtotime(array_get($raw, 'start_at')),
            'end_at' => strtotime(array_get($raw, 'end_at')),
            'official_organizer_id' => array_get($injector, 'official_organizer_id')
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    public static function transform($model)
    {
        /**
         * @var $model CourseDate
         */
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'user' => $model->user,
            'course_owner_id' => $model->course_owner_id,
            'course_id' => $model->course_id,
            'official_organizer_id' => $model->official_organizer_id,
            'start_at' => Carbon::createFromTimestamp((int)$model->start_at)->toRfc822String(),
            'end_at' => Carbon::createFromTimestamp((int)$model->end_at)->toRfc822String(),
            'course' => CourseTransformer::transform($model->course),
            'totals' => $model->getStarsCount(),
            'reviews_count' => $model->getReviewCount(),
            'participants_count' => $model->participants_count(),
            'course_participants' => UserForParticipantsTransformer::transformCollection($model->get_participants()),
            'questions' => QuestionTransformer::transformCollection($model->questions),
            'created_at' => $model->created_at
        ];
    }
}