<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;

use App\Transformers\UserTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SocialAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function fbAuth(Request $request)
    {
        $model = new User;
        $type = $request->input('type');
        $user = User::where(['fb_id' => $request->input('id')])->first();
        $token = md5(Carbon::now());
        if (!$user) {

            return response()->json([
                'auth_type' => 'sign_up',
            ], 200);

        } else {
            return response()->json([
                'auth_type' => 'log_in',
                'user'      => $user,
            ], 200);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function twAuth(Request $request)
    {


        $model = new User;
        $type = $request->input('type');
        $user = User::where(['twitter_id' => $request->input('id')])->first();

        $token = md5(Carbon::now());
        if (!$user) {

            return response()->json([
                'auth_type' => 'sign_up',
            ], 200);

        } else {
            return response()->json([
                'auth_type' => 'log_in',
                'user'      => $user,
            ], 200);
        }

    }

}
