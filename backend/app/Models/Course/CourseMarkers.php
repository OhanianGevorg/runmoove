<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseMarkers extends Model
{
    //
    use SoftDeletes;
    protected $table = 'course_markers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'course_id',
        'marker_type',
        'latitude',
        'longitude',
        'coords',
        'gpx_url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
