'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.auth/types
 * @description
 * # auth/types
 * Constant in the runMoovApp.
 */
angular.module('runMoovApp')
  .constant('userType', {
    all: '*',
    member: 2,
    organizer: 1,
  });
