'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesScheduleCtrl
 * @description
 * # PagesScheduleCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('ScheduleCtrl', function ($scope,uiCalendarConfig,$compile,Runs,orderByFilter) {

    $scope.events = [];
    $scope.eventsArray = [];
    $scope.scheduleView = 'calendar';



    Runs.get(function (res) {
      $scope.randomRuns = res.data;

      $scope.random = function() {
        return 0.9 - Math.random();
      };

      $scope.list = orderByFilter($scope.randomRuns,$scope.random);
    });




    Runs.userRuns(function (res) {

      $scope.runs = res.data;
      // $scope.events2.push({id:235,'title':'ARMAN AAA',start:new Date(y, m, 1), end:new Date(y, m, 4),allDay:false});

        angular.forEach($scope.runs,function (run) {
          $scope.eventsArray.push({
            id:run.id,
            title:run.course.name,
            description:run.course.description,
            start:new Date(run.start_at),
            end:new Date(run.end_at),
            img:run.course.course_images[0].url
          });

          $scope.events =  $scope.eventsArray;
          console.log($scope.events);
        });
    });




    $scope.changeView = function (view) {
      $scope.scheduleView = view;
    };

    // $scope.events = [
    //   {title: 'All Day Event',start: new Date(y, m, 1)},
    //   {title: 'Long Event',start: new Date(y, m, d - 155),end: new Date(y, m, d - 150)},
    //   {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
    //   {id:235,'title':'ARMAN AAA',start:new Date(y, m, 1), end:new Date(y, m, 4),allDay:false},
    //   {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
    //   {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
    //   {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
    // ];




  });
