'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:PagesRunRunOpinionsNewCtrl
 * @description
 * # PagesRunRunOpinionsNewCtrl
 * Controller of the frontendApp
 */
angular.module('runMoovApp')
  .controller('RunOpinionsNewCtrl', function ($scope,AuthService,systemConstants,ENV,$stateParams,$localStorage,Opinions,Runs) {

    $scope.activeStep = 'first';

    $scope.UserType = AuthService.type();


    $scope.chekedDatas = {
      course_formats:[],
      practicing_types:[]
    };

    $scope.data = {
      'user_id': AuthService.userID(),
      'course_date_id': $stateParams.id,
      'course_opinion_formats': [],
      'title': '',
      'body': '',
      'parcour_score': '',
      'ambiance_score': '',
      'dotation_score': '',
      'revitaillement_score': '',
      'organisation_score': '',
      'difficult_score': '',
      'participation_purpose': '',
      'participate_again': '',
      'course_finish_time': '',
      'best_memories': '',
      'place_recommendations': '',
      'suggestions': '',
      'opinion_images': [],
    };


    Runs.show({id:$stateParams.id},function (res) {
      $scope.run = res.course_date;
    });

    $scope.sendReview = function () {
      console.log($scope.data);
      $scope.data.course_opinion_formats = $scope.chekedDatas.course_formats;

      for (var n in $scope.data) {
        if (n == "opinion_images") {
          continue;
        }
        if (Array.isArray($scope.data[n])) {
          $scope.data[n] = $scope.data[n].join();
        }
      }

      Opinions.save($scope.data,function (res) {
        $scope.activeStep = 'finish';
      },function (err) {
        $scope.errors = err.data;
      });
    };


    $scope.nextStep = function (step) {
      $scope.activeStep = step;

      var body = angular.element("html, body");
      body.stop().animate({scrollTop: 0}, 500, 'swing', function () {
      });
    };

    $scope.previewMode = function (bool) {
      if (bool == true) {
        $scope.activeStep = '';
      } else {
        $scope.activeStep = 'third';
      }
      $scope.isPreviewMode = bool;
    };


    /**
     * get System Constants
     */
    systemConstants.get(function (res) {
      $scope.systemTypes = res.system_types;
    });

    systemConstants.opinionFields(function (res) {
      console.log(res);
      $scope.oponionFields = res.system_types;
    });


    //Set options for dropzone
    //Visit http://www.dropzonejs.com/#configuration-options for more options
    $scope.dzOptions = {
      url: ENV.apiEndpoint + '/upload',
      paramName: 'photo',
      maxFilesize: '30',
      acceptedFiles: 'image/jpeg, images/jpg, image/png',
      addRemoveLinks: true,
      dictRemoveFile: "",
      dictCancelUpload: "",
      clickable: '#selectRunPhotos',
      parallelUploads: 1,
      autoProcessQueue: true
    };


    $scope.InProgress = false;
    //Handle events for dropzone
    //Visit http://www.dropzonejs.com/#events for more events
    $scope.dzCallbacks = {
      addedfile: function (file) {
        if (!$scope.InProgress) {
          $scope.newFile = file;
        }
        $scope.InProgress = true;

      },
      success: function (file, xhr) {
        if (xhr.hasOwnProperty('image')) {

          $scope.data.opinion_images.push({url:xhr['image']});
          $scope.InProgress = false;
        }

        console.log($scope.data.course_images);
        angular.element('.dz-remove').addClass('fa fa-remove');
      },
      error: function (file) {
        $scope.newFile.length = 0;
      },

      removedfile: function (file) {
        var image = JSON.parse(file.xhr.response);
        var index;
        for (var i = 0;i<$scope.data.opinion_images.length;i++){
          if($scope.data.opinion_images[i].url==image.image){
            index = i;
            break;
          }
        }
        $scope.data.opinion_images.splice(index, 1);
        console.log($scope.data.course_images);
      },
      uploadprogress: function (file, progress) {
        file.progress = progress;
        if(progress===100){
          $scope.InProgress = true;
        }
      },
      sending: function(file,xhr) {
        return xhr.setRequestHeader('X-TOKEN', $localStorage.api_token);
      }
    };


    //Apply methods for dropzone
    //Visit http://www.dropzonejs.com/#dropzone-methods for more methods
    $scope.dzMethods = {};
    $scope.removeNewFile = function () {
      $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
    };


  });
