<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCourseDateStartAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_dates', function (Blueprint $table) {
            //
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
        });
        Schema::table('course_dates', function (Blueprint $table) {
            //
            $table->string('start_at');
            $table->string('end_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_date', function (Blueprint $table) {
            //
        });
    }
}
