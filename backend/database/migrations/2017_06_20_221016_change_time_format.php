<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTimeFormat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_opinion', function (Blueprint $table) {
            //
            $table->dropColumn('course_finish_time');
        });
        Schema::table('course_opinion', function (Blueprint $table) {
            //
            $table->string('course_finish_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_opinion', function (Blueprint $table) {
            //
            $table->dropColumn('course_finish_time');
        });
        Schema::table('course_opinion', function (Blueprint $table) {
            //
            $table->time('course_finish_time');
        });
    }
}
