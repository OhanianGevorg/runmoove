<?php

namespace App\Http\Controllers\Friendship;

use App\Http\Requests\Friendship\BlockFriendRequest;
use App\Http\Requests\Friendship\FriendAcceptRequest;
use App\Http\Requests\Friendship\FriendShipDeleteRequest;
use App\Http\Requests\Friendship\FriendshipStoreRequest;
use App\Mail\NotificationMessage;
use App\Models\Friendship\FriendList;
use App\Models\Friendship\FriendRequest;
use App\Models\UserRelated\Notifications;
use App\Transformers\Friendship\FriendListTransformer;
use App\Transformers\Friendship\FriendRequestTransformer;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FriendshipController extends Controller
{

    public function index()
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        return response()->json([
            'friend_requests' => FriendRequestTransformer::transformCollection(User::auth()->receivedFriendRequest)
        ]);
    }

    /**
     * @param FriendshipStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FriendshipStoreRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }
        /**
         * @var $recipient User
         */
        $recipient = User::find($request->get('recipient_id'));
        if (!$request->has('recipient_id') || is_null($recipient)) {
            return response()->json([
                'error' => 'This model does not exists'
            ], 404);
        }
        try {
            if (!User::auth()->canBeFriends($request->get('recipient_id'))) {
                return response()->json([
                    'error' => 'You have not permission to this action'
                ], 403);
            }
            $data = [
                'sender_id' => User::auth()->getKey(),
                'recipient_id' => $request->get('recipient_id'),
                'status' => FriendRequest::PENDING
            ];
            $friendRequest = FriendRequestTransformer::prepare($data);
            $friendRequest->save();
            return response()->json([
                'success' => 'Friendship request has sent'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param FriendAcceptRequest $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function acceptFriend(FriendAcceptRequest $request)
    {
        if (!User::auth()) {
            return $request->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }
        if ($request->has('event_id')) {
            $requestor = Notifications::find($request->get('event_id'))->event;
        } else {
            $requestor = FriendRequest::find($request->get('request_id'));
        }
        if (!$requestor) {
            return response()->json([
                'error' => 'Model has not found'
            ], 404);
        }
        if ((int)$requestor->recipient_id !== (int)User::auth()->getKey() || (int)$requestor->sender_id === (int)User::auth()->getKey()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }
        if (!FriendList::friendNotRecordExists($requestor)) {
            return response()->json([
                'You are already friends'
            ], 403);
        }
        DB::beginTransaction();
        try {
            $requestor->fill([
                'status' => FriendRequest::ACCEPTED
            ]);
            $requestor->save();
            $data = [
                'sender_id' => $requestor->sender_id,
                'recipient_id' => $requestor->recipient_id,
                'is_blocked' => FriendList::IS_NOT_BLOCKED
            ];
            $friendlist = FriendListTransformer::prepare($data);
            $friendlist->save();
            DB::commit();
            return response()->json([
                'success' => $friendlist
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param FriendShipDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(FriendShipDeleteRequest $request)
    {
        if (!$request->get('friend_request_id')) {
            return response()->json([
                'error' => 'Filed is required'
            ], 422);
        }
        try {
            /**
             * @var $friend_request FriendRequest
             */
            $friend_request = FriendRequest::where('id', '=', $request->get('friend_request_id'))
                ->where('sender_id', '=', (int)User::auth()->getKey())->firstOrFail();
            $friend_request->delete();

            return response()->json([
                'success' => 'Successfully deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param FriendShipDeleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function decline(FriendShipDeleteRequest $request)
    {
        try {
            /**
             * @var $friend_request FriendRequest
             */
            if ($request->has('event_id')) {
                $friend_request = Notifications::find($request->get('event_id'))
                    ->event()
                    ->where('recipient_id', '=', (int)User::auth()->getKey())
                    ->firstOrFail();
            } else {
                $friend_request = FriendRequest::where('id', '=', $request->get('friend_request_id'))
                    ->where('recipient_id', '=', (int)User::auth()->getKey())->firstOrFail();
            }
            $friend_request->status = FriendRequest::DECLINED;
            $friend_request->save();

            return response()->json([
                'success' => 'Successfully declined'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
