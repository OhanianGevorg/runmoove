<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class SystemCourseFormat extends Model
{

    protected $table = 'system_course_formats';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
}
