'use strict';

/**
 * @ngdoc service
 * @name runMoovApp.friends
 * @description
 * # friends
 * Service in the runMoovApp.
 */
angular.module('runMoovApp')
  .service('Friends', function (ENV, $resource) {

    return $resource(ENV.apiEndpoint + '/friends', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      delete: {
        method: 'DELETE',
      },
      block: {
        method: 'POST',
      },
      unblock: {
        method: 'POST',
      },
      show: {
        url:ENV.apiEndpoint + '/friends/:id',
        method: 'GET',
      },
      friendlist:{
        url:ENV.apiEndpoint + '/friendlist',
        method: 'GET'
      }


    });
  });
