<?php

namespace App\Http\Controllers\Shared;

use App\Http\Requests\UploadRequests\UploadRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * @param UploadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImages(UploadRequest $request)
    {
        if (!$request->hasFile('photo')) {
            return response()->json([
                'error' => 'You have not upload image'
            ], 422);
        }
        try {

            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension() ? $file->getClientOriginalExtension() : 'jpeg';
            $file_name = md5(strtotime('now')) . '.' . $extension;
            Storage::putFileAs('public/',$file,$file_name);

            return response()->json([
                'image' => $file_name
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
