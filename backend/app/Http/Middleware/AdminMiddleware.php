<?php

namespace App\Http\Middleware;
use App\User;
use Closure;
class AdminMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('X-TOKEN');
        $user = User::where('api_token', $token)->first();

        /**
         * @var $user User
         */
        if (!$user->is_admin()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('errors.503');
            }
        }
        return $next($request);
    }
}