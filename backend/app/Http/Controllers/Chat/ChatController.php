<?php

namespace App\Http\Controllers\Chat;

use App\Http\Requests\Chat\GetRoomRequest;
use App\Http\Requests\Chat\MessagesReadRequest;
use App\Http\Requests\Chat\MessageStoreRequest;
use App\Http\Requests\Chat\RoomChatReadRequest;
use App\Http\Requests\Chat\SetMessageReadRequest;
use App\Models\Chat\Message;
use App\Models\Chat\Participant;
use App\Models\Chat\Room;
use App\Transformers\Chat\MessageTransformer;
use App\Transformers\Chat\ParticipantsTransformer;
use App\Transformers\Chat\RoomTransformer;
use App\Transformers\Chat\RoomWithLastMessageTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        try {
            $rooms = User::auth()->rooms;
            return response()->json([
                'rooms' => RoomWithLastMessageTransformer::transformCollection($rooms)
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param RoomChatReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function roomChat(RoomChatReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        if (!$request->has('room_id')) {
            return response()->json([
                'error' => 'room_id field is required'
            ], 422);
        }
        /**
         * @var $room Room
         */
        try {
            $room = Room::findOrFail($request->get('room_id'));
            return response()->json([
                'messages' => MessageTransformer::transformCollection($room->messages()->getAll($request->all())),
                'room' => RoomTransformer::transform($room),
                'unread_messages' =>$room->messages()->unreadMessages(User::auth())->count()
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param MessageStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MessageStoreRequest $request)
    {
        if (!$request->has('recipient_id')) {
            return response()->json([
                'error' => 'recipient is required'
            ], 422);
        }

        if (!User::auth() || (int)User::auth()->getKey() === (int)$request->get('recipient_id')) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }

        /**
         * @var $recipient User
         */
        $recipient = User::find($request->get('recipient_id'));
        if (!$recipient) {
            return response()->json([
                'error' => 'User not found'
            ], 404);
        }

        if (!User::auth()->canWrite($recipient)) {
            return response()->json([
                'error' => 'You can\'t wright to this user'
            ], 403);
        }

        $rooms = User::auth()->checkRoomExists($recipient);
        $room_id = $request->has('room_id') ? $request->get('room_id') : $rooms > 0 ? $rooms : null;

        DB::beginTransaction();
        try {

            /**
             * @desc if room is created first time
             */
            if (!$room_id) {
                /**
                 * @desc save room for users
                 */
                $room = RoomTransformer::prepare(['user_id' => User::auth()->getKey()]);
                $room->save();
                $room_id = $room->getKey();
                /**
                 * @desc save room participants
                 */
                $participants = ParticipantsTransformer::prepareCollection([
                    [
                        'user_id' => User::auth()->getKey(),
                        'room_id' => $room->getKey()
                    ],
                    [
                        'user_id' => $request->get('recipient_id'),
                        'room_id' => $room->getKey()
                    ]
                ]);

                $room->participants()->saveMany($participants);
            }
            /**
             * @desc save message
             */
            $message = MessageTransformer::prepare([
                'room_id' => (int)$room_id,
                'user_id' => User::auth()->getKey(),
                'recipient_id' => $request->get('recipient_id'),
                'body' => json_encode($request->get('body')),
                'is_read' => Message::IS_NOT_READ
            ]);
            $message->save();
            DB::commit();
            return response()->json([
                'message' => MessageTransformer::transform($message),
                'room_id' => (int)$room_id
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param SetMessageReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setMessagesRead(SetMessageReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        /**
         * @var $message Message
         */
        try {
            Message::getReceivedMessages(User::auth()->getKey(), $request->get('message_ids'))->update([
                'is_read' => Message::IS_READ
            ]);

            return response()->json([
                'success' => 'Messages are read'
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {

            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        try {
            $message = Message::where('id', '=', $id)
                ->where('recipient_id', '=', User::auth()->getKey())
                ->orWhere('user_id', '=', User::auth()->getKey())
                ->firstOrFail();
            $message->delete();
            return response()->json([
                'success' => 'Message successfully deleted'
            ], 200);
        } catch (ModelNotFoundException $e) {

            return response()->json([
                'error' => 'Message not found'
            ], 404);
        } catch (\Exception $e) {

            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param GetRoomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoom(GetRoomRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                response()->json([
                    'error' => 'You have not permission'
                ], 403)
            ]);
        }

        try {
            $recipient = User::findOrFail($request->get('recipient_id'));

            return response()->json([
                'rooms' => User::auth()->checkRoomExists($recipient)
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => 'Data not found'
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }
}
