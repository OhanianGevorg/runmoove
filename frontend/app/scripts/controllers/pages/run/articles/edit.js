'use strict';

/**
 * @ngdoc function
 * @name runMoovApp.controller:PagesRunArticlesEditCtrl
 * @description
 * # PagesRunArticlesEditCtrl
 * Controller of the runMoovApp
 */
angular.module('runMoovApp')
  .controller('ArticleEditCtrl', function (Articles,$stateParams,$scope,ENV,$localStorage,$state) {

    $scope.updatedData = {};
    $scope.images = [];
    $scope.videos = [];
    $scope.category_id = '';




    Articles.show({id:$stateParams.id},function (res) {
      $scope.data = res.data;

      $scope.category_id = res.data.category.id;
      var myDropzone = angular.element('#ArticleDropZone')[0].dropzone;

      $scope.addMoreVideo = function () {
        $scope.data.article_videos.push({url: ''});
      };


      $scope.removeVideo = function (idx) {
        $scope.data.article_videos.splice(idx,1);
      };



      var mockFile = {};

      var i = 0;
      angular.forEach($scope.data.article_images,function (img) {

        mockFile = {
          accepted:true,

          name: 'Image' + i,
          size: 123456,
          type: 'image/jpeg',
          status: Dropzone.SUCCESS,
          url: img.file_name,
        };
        i++;
        // Call the default addedfile event handler

        myDropzone.emit("addedfile", mockFile);

        // And optionally show the thumbnail of the file:
        myDropzone.emit("thumbnail", mockFile, img.file_name);

        myDropzone.files.push(mockFile);
      });







      $scope.InProgress = false;

    });

    Articles.categories(function (res) {
      $scope.categories = res.categories;
    });






    //Set options for dropzone
    //Visit http://www.dropzonejs.com/#configuration-options for more options
    $scope.dzOptions = {
      url: ENV.apiEndpoint + '/upload',
      paramName: 'photo',
      maxFilesize: '30',
      acceptedFiles: 'image/jpeg, images/jpg, image/png',
      addRemoveLinks: true,
      dictRemoveFile: "",
      dictCancelUpload: "",
      clickable: '#addPhotoBtn',
      parallelUploads: 1,
      autoProcessQueue: true,
      maxFiles:40,
      uploadMultiple:false,

    };

    //Handle events for dropzone
    //Visit http://www.dropzonejs.com/#events for more events
    $scope.dzCallbacks = {

      addedfile: function (file) {
        angular.element('.dz-remove').addClass('fa fa-remove');
        console.log(file);

        if (!$scope.InProgress) {
          $scope.newFile = file;
        }
        $scope.InProgress = true;

      },
      success: function (file, xhr) {
        if (xhr.hasOwnProperty('image')) {
          $scope.InProgress = false;
        }

        $scope.data.article_images.push({file_name:xhr.image})

        angular.element('.dz-remove').addClass('fa fa-remove');
      },
      error: function (file) {
        $scope.newFile.length = 0;
      },

      removedfile: function (file) {
        var image = JSON.parse(file.xhr.response);
        var index;
        for (var i = 0;i<$scope.data.article_images.length;i++){
          if($scope.data.article_images[i].url==image.image){
            index = i;
            break;
          }
        }
        $scope.data.article_images.splice(index, 1);
      },
      uploadprogress: function (file, progress) {
        file.progress = progress;
        if(progress===100){
          $scope.InProgress = true;
        }
      },
      sending: function(file,xhr) {
        return xhr.setRequestHeader('X-TOKEN', $localStorage.api_token);
      }
    };


    //Apply methods for dropzone
    //Visit http://www.dropzonejs.com/#dropzone-methods for more methods
    $scope.dzMethods = {};
    $scope.removeNewFile = function () {
      $scope.dzMethods.removeFile($scope.newFile); //We got $scope.newFile from 'addedfile' event callback
    };




    $scope.updateArticle = function () {

      angular.forEach($scope.data.article_images,function (img) {
        $scope.images.push({file_name:img.file_name.replace(/^.*[\\\/]/, '')})
      });

      angular.forEach($scope.data.article_videos,function (video) {
        $scope.videos.push({url:video.file_name})
      });


      $scope.updatedData.images = $scope.images;
      $scope.updatedData.videos = $scope.videos;

      $scope.updatedData = {
        category:$scope.category_id,
        title:$scope.data.title,
        body:$scope.data.body,
        date:$scope.data.date,
        images:$scope.images,
        videos:$scope.videos,
      };

        Articles.update({id:$scope.data.id},$scope.updatedData,function (res) {
          toastr.success('Article is updated', 'Success message');
          $state.go('run.home');
        },function (err) {
          $scope.errors = err.data;
          toastr.error('Something goes wrong', 'Error message');
        });

    };



  });
