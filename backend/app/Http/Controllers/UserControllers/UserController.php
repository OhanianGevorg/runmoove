<?php

namespace App\Http\Controllers\UserControllers;

use App\Http\Requests\UserStoreRequests\ChangeAvatarRequest;
use App\Http\Requests\UserStoreRequests\ChangePassword;
use App\Http\Requests\UserStoreRequests\ChangePasswordRequest;
use App\Http\Requests\UserStoreRequests\OwnParticipationRequest;
use App\Http\Requests\UserStoreRequests\UserAdditionalRequest;
use App\Http\Requests\UserStoreRequests\UserMainStoreRequest;
use App\Http\Requests\UserStoreRequests\UserParticipationReadRequest;
use App\Http\Requests\UserStoreRequests\UserReadRequest;
use App\Http\Requests\UserStoreRequests\UserUpdateRequest;
use App\Libs\Calculations;
use App\Models\Course\CourseDate;
use App\Models\SystemRelated\UserCourseFormat;
use App\Models\SystemRelated\UserCourseImportance;
use App\Models\SystemRelated\UserPracticingType;
use App\Transformers\CourseTransformers\CourseDateTransformer;
use App\Transformers\Friendship\FriendListTransformer;
use App\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function __construct()
    {
//        $this->middleware('ownAuth', ['except' => ['index', 'show', 'storeMain']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $user = User::getAll($request->all());
            return response()->json([
                'user' => UserTransformer::transformCollection($user),
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param UserMainStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMain(UserMainStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $user = UserTransformer::prepare($request->all());
            $user->save();
            Calculations::calculateFillPercent($user)->save();
            $user->setLevel()->save();
            DB::commit();
            return response()->json([
                'success' => 'Data has inserted'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => $e->getMessage() . " " . "\n" . $e->getLine()
            ], 422);
        }
    }

    /**
     * @param UserAdditionalRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeAdditional(UserAdditionalRequest $request)
    {
        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'User not found'
            ], 404);
        }

        DB::beginTransaction();
        try {

            /**-
             * @var $user User
             */
            $user->update(UserTransformer::prepareAdditional($request->all()));

            if ($request->has('course_formats')) {

                if ($user->user_course_formats) {
                    $user->user_course_formats()->delete();
                }

                UserCourseFormat::insert(array_map(function ($format) use ($user) {
                    return [
                        'format_id' => $format,
                        'user_id' => $user->getKey(),
                        'created_at' => Carbon::now()
                    ];
                },
                    explode(',', $request->get('course_formats'))));
            }

            if ($request->has('course_importance')) {
                if ($user->user_course_importances) {
                    $user->user_course_importances()->delete();
                }
                UserCourseImportance::insert(array_map(function ($importance) use ($user) {
                    return [
                        'importance_id' => $importance,
                        'user_id' => $user->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, explode(',', $request->get('course_importance'))));
            }

            if ($request->has('user_practicing')) {

                if ($user->user_practicing_types) {
                    $user->user_practicing_types()->delete();
                }

                UserPracticingType::insert(array_map(function ($type) use ($user) {

                    return [
                        'type_id' => $type,
                        'user_id' => $user->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, explode(',', $request->get('user_practicing'))));
            }

            Calculations::calculateFillPercent($user)->save();

            DB::commit();
            return response()->json([
                'user' => UserTransformer::transform($user)
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => $e->getMessage() . " " . "\n" . $e->getFile() . " " . $e->getLine()
            ]);
        }
    }

    public function edit(Request $request, $id)
    {
        /**
         * @var $user User
         */
        $user = User::auth();
        if (!$user || (int)$user->getKey() != (int)$id) {
            return response()->json([
                'error' => 'You have not permission'
            ], 500);
        }
        return response()->json([
            'user' => UserTransformer::transform($user)
        ], 200);
    }

    public function show($id, UserReadRequest $request)
    {
        try {
            $user = User::findOrFail($id);
            return response()->json([
                'user' => UserTransformer::transform($user)
            ]);
        } catch (NotFoundHttpException $e) {
            return response()->json([
                'error' => 'User does not exist'
            ], 404);
        }
    }

    public function update(UserUpdateRequest $request, $id)
    {
        /**
         * @var $user User
         */
        $user = User::auth();
        if (is_null($user) || (int)$user->getKey() !== (int)$id) {
            return response()->json([
                'error' => 'You have not permission.'
            ], 403);
        }

        try {
            $user->update(UserTransformer::updatePrepare($request->all()));
            $user->save();
            return response()->json([
                'user' => UserTransformer::transform($user),
                'success' => 'Data has successfully updated'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => "Something goes wrong"
            ], 500);
        }
    }

    public function changeAvatar(ChangeAvatarRequest $request)
    {
        /**
         * @var $user User
         */
        $user = User::auth();
        if (is_null($user)) {
            return response()->json([
                'error' => 'You have not permission.'
            ], 403);
        }
        try {
            $user->avatar = $request->get('avatar');
            $user->save();
            return response()->json([
                'file' => asset(Storage::url($user->avatar)),
                'success' => 'Avatar has successfully changed'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        /**
         * @var $user User
         */
        $user = User::auth();
        if (is_null($user)) {
            return response()->json([
                'error' => 'You have not permission.'
            ], 403);
        }
        if (!Hash::check($request->get('old_password'), $user->password)) {
            return response()->json([
                'error' => 'Password is incorrect'
            ], 422);
        }
        try {
            $user->password = bcrypt($request->get('password'));
            $user->save();
            return response()->json([
                'success' => 'Password has successfully changed'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    /**
     * @param UserParticipationReadRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getParticipationCourses(UserParticipationReadRequest $request)
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }

        /**
         * @var $user User
         */
        $user = User::auth();
        try {
            $course_date = $user->get_user_course_query()->getAll($request->all());
            return response()->json([
                'user_courses' => CourseDateTransformer::transformCollection($course_date)
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 500);
        }
    }

    public function getEventsList()
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission for this action'
            ], 403);
        }
        return response()->json([
            'events' => User::auth()->getEventsList()
        ], 200);
    }

    public function getFriends()
    {
        if (!User::auth()) {
            return response()->json([
                'error' => 'You have not permission'
            ], 403);
        }
        return response()->json([
            'friends' => FriendListTransformer::transformCollection(User::auth()->friends()->get())
        ], 200);
    }

    public function ownParticipations(OwnParticipationRequest $request)
    {
        if(!User::auth()){
            return response()->json([
                'error'=>'You haven not permission'
            ],403);
        }
        try{
            /**
             * @var $participations CourseDate
             */
            $participations = User::auth()->course_participation_to_many()->getAll($request->all());
            return response()->json([
                'data'=>CourseDateTransformer::transformCollection($participations)
            ],200);

        }catch (\Exception $e){
            return response()->json([
                'error'=>'Something goes wrong'
            ],500);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pusherAuth(Request $request)
    {

        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'Not found'
            ], 404);
        }
        $pusher = new \Pusher(env('PUSHER_KEY'), env('PUSHER_SECRET'), env('PUSHER_APP_ID'));
        $connection_auth = $pusher->socket_auth($request->get('channel_name'), $request->get('socket_id'));
        echo $connection_auth;
    }
}
