<?php

namespace App\Http\Requests\Notifications;

use App\Http\Requests\AbstractRequest;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class NotificationReadRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
