'use strict';

angular.module('runMoovApp')
  .controller('RunMainCtrl', function ($scope,$rootScope, Runs, $state,$timeout,Articles,AuthService) {

    $scope.runs = [];
    $scope.articles = [];
    $scope.data = [];
    $scope.dataIsLoading = false;
    $rootScope.searchRunCriteria.page = 1;
    $scope.article_page = 1;

    $scope.isAdmin = AuthService.isAdmin();

    /**
     * Search and get Runs
     */
    $rootScope.searchRuns = function () {
      $scope.dataIsLoading = true;

      if (Object.keys($rootScope.filterField.address).length > 0) {
        $rootScope.searchRunCriteria.location = $rootScope.filterField.address.formatted_address;
        $rootScope.searchRunCriteria.latitude = $rootScope.filterField.address.geometry.location.lat();
        $rootScope.searchRunCriteria.longitude = $rootScope.filterField.address.geometry.location.lng();
      }

      if ($rootScope.filterField.address == '') {
        $rootScope.searchRunCriteria.location = '';
        $rootScope.searchRunCriteria.latitude = '';
        $rootScope.searchRunCriteria.longitude = '';
      }


      $rootScope.searchRunCriteria.types = $rootScope.filterField.types;


      for (var n in $rootScope.searchRunCriteria) {

        if (Array.isArray( $rootScope.searchRunCriteria[n])) {
          $rootScope.searchRunCriteria[n] = $rootScope.searchRunCriteria[n].join();
        }
      }

      $timeout(function () {
        Runs.query($rootScope.searchRunCriteria, function (res) {
          $scope.runs =  $scope.runs.concat(res.data);
          $scope.dataIsLoading = false;
        });
      },300);
    };


    /**
     * get Articles
     */
    $scope.loadArticles = function () {
      Articles.query({page:$scope.article_page},function (res) {
        $scope.articles = $scope.articles.concat(res.data);
      });
    };


    /**
     * Load more Runs method
     */
    $scope.loadMoreRuns = function () {
      $rootScope.searchRunCriteria.page++;
      $rootScope.searchRuns();
    };


    /**
     * Load more articles method
     */
    $scope.loadMoreArticles = function () {
      $scope.article_page++;
      $scope.loadArticles();
    };


    /**
     * call general methods
     */
    $rootScope.searchRuns();
    $scope.loadArticles();
  });
