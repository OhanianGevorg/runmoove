'use strict';

/**
 * @ngdoc directive
 * @name runMoovApp.directive:RmEmojiBox
 * @description
 * # RmEmojiBox
 */
angular.module('runMoovApp')
  .directive('rmEmojiBox', function () {
    return {
      restrict: 'AE',
      link: function link(scope, element, attrs) {

        $(element).emojioneArea(
          {
            pickerPosition: "top",
            filtersPosition: "bottom",
            tones: false,
            autocomplete: true,
            inline: true,
            hidePickerOnBlur: false
          }
        );
      }
    };
  });
