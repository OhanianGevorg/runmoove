<?php

namespace App\Http\Requests\UserStoreRequests;

use App\Http\Requests\AbstractRequest;

class UserMainStoreRequest extends AbstractRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:255|confirmed',
            'dob' => 'required|date',
            'address' => 'required|max:255',
            'longitude' => 'max:255',
            'latitude' => 'max:255',
            'is_subscribed' => 'string|max:255',
            'twitter_id' => 'min:2|max:255',
        ];
    }
}
