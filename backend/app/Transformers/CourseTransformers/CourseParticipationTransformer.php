<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 6/24/17
 * Time: 11:40 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Course\CourseParticipation;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class CourseParticipationTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new CourseParticipation([
            'user_id' => array_get($raw, 'user_id'),
            'course_date_id' => array_get($raw, 'course_date_id')
        ]);
    }

    public static function transform($model)
    {
        // TODO: Implement transform() method.
    }
}