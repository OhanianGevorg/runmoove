'use strict';

describe('Controller: LayoutHeaderCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var LayoutHeaderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LayoutHeaderCtrl = $controller('LayoutHeaderCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LayoutHeaderCtrl.awesomeThings.length).toBe(3);
  });
});
