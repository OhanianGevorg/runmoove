<?php

namespace App\Models\UserRelated;

use App\Traits\FilterTrait;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    //
    use FilterTrait;

    const IS_READ = 1;
    const IS_NOT_READ = 2;
    protected $table = 'notifications';
    protected $primaryKey = 'id';
    protected $fillable = [
        'owner_id',
        'event_name',
        'sender_id',
        'is_read',
        'event_id',
        'event_type',
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function getSender()
    {
        return ($this->sender_id) ? User::find($this->sender_id) : null;
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public static function getNotifications($ids = [])
    {
        if (!count($ids)) {
            return [];
        }
        if (count($ids) === 1 || gettype($ids) !== 1) {
            return static::where('id', '=', $ids);
        }
        return static::whereIn('id', $ids);
    }

    public static function setStatus($ids, $status)
    {
        $status = $status !== self::IS_NOT_READ ? self::IS_READ : self::IS_NOT_READ;
        static::getNotifications($ids)->update([
            'is_read' => $status
        ]);
    }

    //morph relation
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function event()
    {
        return $this->morphTo();
    }
}
