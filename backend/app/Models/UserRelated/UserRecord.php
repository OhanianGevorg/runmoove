<?php

namespace App\Models\UserRelated;

use App\Models\Course\CourseDate;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRecord extends Model
{
    //
    use SoftDeletes;
    protected $table = 'user_records';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'course_date_id',
        'time',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function course_date()
    {
        return $this->belongsTo(CourseDate::class, 'course_date_id');
    }
}
