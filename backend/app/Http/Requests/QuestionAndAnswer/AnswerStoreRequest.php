<?php

namespace App\Http\Requests\QuestionAndAnswer;

use App\Http\Requests\AbstractRequest;
use App\Models\QuestionAndAnswer\Question;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class AnswerStoreRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !is_null(User::auth())&& User::auth()->canAnswer(Question::find($this->get('question_id')));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_id'=>'required|integer|exists:questions,id',
            'body'=>'required|max:65000'
        ];
    }
}
