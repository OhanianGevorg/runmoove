<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class SystemCourseImportance extends Model
{
    //
    protected $table = 'system_course_importance';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name'
    ];
}
