<?php

/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/10/2017
 * Time: 1:39 AM
 */
namespace App\Transformers\Friendship;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Friendship\FriendRequest;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserForFriendsTransformer;
use App\User;

class FriendRequestTransformer implements TransformerInterface, PrepareInterface
{

    use TransformCollection, PrepareCollection;

    public static function prepare(array $raw, array $injector = [])
    {
        return new FriendRequest([
            'sender_id' => array_get($raw, 'sender_id'),
            'recipient_id' => array_get($raw, 'recipient_id'),
            'status' => array_get($raw, 'status')
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     * @desc function also check if sender or recipient is auth user
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'sender' => FriendRequestTransformer::returnModel($model->sender_id, $model->sender),
            'recipient' => FriendRequestTransformer::returnModel($model->recipient_id, $model->recipient),
            'status' => $model->status
        ];
    }

    /**
     * @param $user_id
     * @param $model
     * @return array
     * @desc return not auth user formatted data
     */
    private static function returnModel($user_id, $model)
    {
        if (!User::auth()) {
            return $user_id;
        }
        if ((int)$user_id !== (int)User::auth()->getKey()) {
            return UserForFriendsTransformer::transform($model);
        }
        return $user_id;
    }
}