'use strict';

describe('Controller: PagesRunMainCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesRunMainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunMainCtrl = $controller('PagesRunMainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunMainCtrl.awesomeThings.length).toBe(3);
  });
});
