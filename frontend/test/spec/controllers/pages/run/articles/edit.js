'use strict';

describe('Controller: PagesRunArticlesEditCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PagesRunArticlesEditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunArticlesEditCtrl = $controller('PagesRunArticlesEditCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunArticlesEditCtrl.awesomeThings.length).toBe(3);
  });
});
