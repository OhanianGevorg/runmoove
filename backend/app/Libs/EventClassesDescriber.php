<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 7/9/2017
 * Time: 10:28 PM
 */

namespace App\Libs;


use Illuminate\Support\Facades\App;

class EventClassesDescriber
{
    private static $eventClasses = [
        \App\Models\Friendship\FriendRequest::class => [
            'created' => [
                \App\Events\FriendRequestSendEvent::class => 'broadcast'
            ]
        ],
        \App\Models\Friendship\FriendList::class => [
            'created' => [
                \App\Events\FriendEvent::class => 'broadcast'
            ]
        ],
        \App\Models\Chat\Message::class => [
            'created' => [
                \App\Events\MessageSendEvent::class => 'broadcast'
            ]
        ],
    ];

    public static function getEvents($className)
    {
        if (array_key_exists($className, static::$eventClasses)) {
            return static::$eventClasses[$className];
        }
        return [];
    }

    public static function getEventClasses()
    {
        return static::$eventClasses;
    }
}