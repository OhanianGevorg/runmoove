<?php
namespace App\Http\Filters;

class CourseDateFilter extends AbstractFilter
{
    /**
     * CourseFilter constructor.
     * @param $query
     */
    public function __construct($query)
    {
        parent::__construct(
            $query
                ->select('course_dates.*')
                ->join('courses', 'courses.id', '=', 'course_dates.course_id'));
    }

    /**
     *  Allowed filter fields
     */
    protected $filter = [
        'location',
        'longitude',
        'latitude',
        'start_at',
        'end_at',
        'name',
        'distance',
        'types'
    ];

    /**
     *  Allowed sorting fields
     */
    protected $order = [
        'name',
        'created'
    ];

    /**
     * Filter by "name" field
     *
     * @param string $value
     */
    protected function filterByName($value = '')
    {
        $this->query->where('courses.name', 'like', '%' . $value . '%');
    }

    /**
     * Filter by "tag" field
     *
     * @param string $value
     */
    protected function filterByStart_at($value = '')
    {
        $this->query->where('course_dates.start_at', '=', strtotime($value));
    }

    /**
     * Filter by "domain_id" field
     *
     * @param string $value
     */
    protected function filterByEnd_at($value = '')
    {
        $this->query->where('course_dates.end_at', '=', strtotime($value));
    }

    /**
     * Filter by "domain_id" field
     *
     * @param string $value
     */
    protected function filterByLocation($value = '')
    {
        $this->query->where('courses.location', $value);
    }

    /**
     * filter by "longitude"
     * @param string $value
     */
    protected function filterByLongitude($value = '')
    {
        $this->query->where('courses.longitude', (double)$value);
    }

    /**
     * filter by "latitude"
     * @param string $value
     */
    protected function filterByLatitude($value = '')
    {
        $this->query->where('courses.latitude', (double)$value);
    }

    /**
     * @param string $value
     */
    protected function filterByDistance($value = '')
    {
        $this->query->where('courses.distance', $value);
    }


    /**
     * @param string $value
     */
    protected function filterByTypes($value = '')
    {
        $this->query->join('course_types', 'courses.id', '=', 'course_types.course_id')
            ->whereIn('course_types.type_id', explode(',', $value));
    }

    /**
     * Sort by name
     *
     * @param string $dir , ASC or DESC
     */
    protected function sortByName($dir = '')
    {
        $this->query->orderBy('name', $dir);
    }

    /**
     * Sort by created_at
     *
     * @param string $dir , ASC or DESC
     */
    protected function sortByCreated($dir = '')
    {
        $this->query->orderBy('created_at', $dir);
    }
}