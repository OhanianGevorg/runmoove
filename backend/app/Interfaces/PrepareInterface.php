<?php
namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface PrepareInterface
{
    /**
     * @param array $raw
     * @param array $injector
     *
     * @return Model
     */
    public static function prepare(array $raw, array $injector = []);
}