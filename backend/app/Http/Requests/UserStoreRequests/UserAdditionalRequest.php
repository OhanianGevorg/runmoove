<?php

namespace App\Http\Requests\UserStoreRequests;

use App\Http\Requests\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class UserAdditionalRequest extends AbstractRequest
{
    public function rules()
    {
        return [
            'practice_since' =>'max:255',
            'running_frequency' => 'max:255',
            'transportation_mode' => 'max:255',
            'budget' => 'max:255',
            'practice_location' => 'max:255',
            'travel_mode' => 'max:255',
            'transport_preference' => 'max:65000',
            'presentation' => 'max:255',
            'course_formats'=>'max:255',
            'user_practicing'=>'max:255',
            'course_importance'=>'max:255'
        ];
    }
}
