<?php
namespace App\Transformers\QuestionAndAnswer;

use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\QuestionAndAnswer\Question;
use App\Traits\TransformCollection;
use App\Transformers\UserOtherTransformers\UserXsTransformer;

class QuestionTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        /**
         * @var $model Question
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'user_id' => $model->user_id,
            'user' => UserXsTransformer::transform($model->questioning),
            'course_date_id' => $model->course_date_id,
            'body' => html_entity_decode($model->body),
            'answer' => AnswerTransformer::transformCollection($model->answer),
            'created_at'=>$model->created_at
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new Question([
            'user_id' => array_get($injector, 'user_id'),
            'course_date_id' => array_get($raw, 'course_date_id'),
            'body' => array_get($raw, 'body')
        ]);
    }

    public static function prepareUpdate(array $raw, array $injector = [])
    {
        return [
            'course_date_id' => array_get($raw, 'course_date_id'),
            'body' => array_get($raw, 'body')
        ];
    }
}