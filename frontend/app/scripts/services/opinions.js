'use strict';

/**
 * @ngdoc service
 * @name frontendApp.opinions
 * @description
 * # opinions
 * Service in the frontendApp.
 */
angular.module('runMoovApp')
  .service('Opinions', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + '/opinion', {}, {
      query: {
        method: 'GET',
        isArray: false
      },
      update: {
        method: 'PUT'
      },
      save:{
        method:'POST'
      },
      show:{
        method:'GET',
        url:ENV.apiEndpoint + '/opinion/:id'
      },
    });
  });
