<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 8/31/2017
 * Time: 8:13 AM
 */

namespace App\Transformers\CourseTransformers;


use App\Interfaces\TransformerInterface;
use App\Models\Course\TravelApi;
use App\Traits\TransformCollection;

class TravelAPITransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        /**
         * @var $model TravelApi
         */
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'type' => $model->type,
            'body' => $model->body,
            'course_date_id' => $model->course_date_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}