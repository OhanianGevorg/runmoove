'use strict';

describe('Controller: PusherChatctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('runMoovApp'));

  var PusherChatctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PusherChatctrlCtrl = $controller('PusherChatctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PusherChatctrlCtrl.awesomeThings.length).toBe(3);
  });
});
