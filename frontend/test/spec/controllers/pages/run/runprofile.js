'use strict';

describe('Controller: PagesRunRunprofileCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendApp'));

  var PagesRunRunprofileCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PagesRunRunprofileCtrl = $controller('PagesRunRunprofileCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PagesRunRunprofileCtrl.awesomeThings.length).toBe(3);
  });
});
